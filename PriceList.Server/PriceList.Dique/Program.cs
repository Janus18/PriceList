using PriceList.Dique.Controllers.Filters;
using PriceList.Dique.Models;
using PriceList.Dique.Tools;

var builder = WebApplication.CreateBuilder(args);

builder.Configuration
    .AddJsonFile("appsettings.json", false)
    .AddUserSecrets<Program>()
    .AddEnvironmentVariables();

builder.Services.Configure<Configuration>(builder.Configuration.GetSection(nameof(Configuration)));
builder.Services.Configure<Credentials>(builder.Configuration.GetSection(nameof(Credentials)));

var externalUrl = builder.Configuration.GetValue<string>("ExternalUrl")
    ?? throw new NullReferenceException("ExternalUrl must be included in the settings");
builder.Services.AddHttpClient(
    HttpClientWrapper.Name,
    c => c.BaseAddress = new Uri(externalUrl));

builder.Services.AddSingleton<HttpClientWrapper>();

builder.Services.AddMediatR(c => c.RegisterServicesFromAssemblyContaining(typeof(Program)));

builder.Services.AddControllers(
    c => c.Filters.Add(new HttpResponseExceptionFilter())
);
builder.Services.AddResponseCaching();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors(b => b
    .AllowAnyMethod()
    .AllowAnyHeader()
    .AllowAnyOrigin());

app.UseHttpsRedirection();

app.UseResponseCaching();

app.UseAuthorization();

app.MapControllers();

app.Run();