using System.Net;
using Microsoft.Extensions.Options;
using PriceList.Dique.Models;
using PriceList.Dique.Tools.Exceptions;

namespace PriceList.Dique.Tools;

public class HttpClientWrapper
{
    public const string Name = "ExternalClient";

    private const string CookieHeaderName = "Set-Cookie";
    
    private readonly IHttpClientFactory _httpClientFactory;
    private readonly Credentials _credentials;
    private readonly Configuration _configuration;

    private SessionCookie _sessionCookie = new InvalidSessionCookie();

    private readonly SemaphoreSlim _semaphore = new(1, 1);
    
    public HttpClientWrapper(
        IHttpClientFactory httpClientFactory,
        IOptions<Credentials> credentialsOptions,
        IOptions<Configuration> configurationOptions)
    {
        _httpClientFactory = httpClientFactory;
        _credentials = credentialsOptions.Value;
        _configuration = configurationOptions.Value;
    }

    public async Task<HttpClient> Get()
    {
        var sessionCookie = await GetSessionCookie();
        return GetClientWithCookie(sessionCookie.Cookie);
    }

    private bool CookieIsValid => _sessionCookie.IsValid(_configuration.CookieRetention); 
    
    private async Task<SessionCookie> GetSessionCookie()
    {
        if (CookieIsValid)
            return _sessionCookie;
        var taken = await _semaphore.WaitAsync(new TimeSpan(0, 0, 15));
        if (!taken)
            throw new BaseAppException(ErrorCodes.Timeout, "Timeout waiting to enter semaphore");
        try
        {
            if (!CookieIsValid)
                _sessionCookie = new SessionCookie(await DoGetSessionCookie(), DateTime.UtcNow);
            return _sessionCookie;
        }
        finally
        {
            _semaphore.Release();
        }
    }

    private async Task<string> DoGetSessionCookie()
    {
        var client = _httpClientFactory.CreateClient(Name);
        var response = await client.GetAsync("/");
        var cookie = response.Headers.Any(h => h.Key == CookieHeaderName)
            ? response.Headers.First(h => h.Key == CookieHeaderName).Value.FirstOrDefault()
            : null;
        if (string.IsNullOrEmpty(cookie))
            throw new BaseAppException(ErrorCodes.NoCookie, "Could not get session cookie");
        await Login(cookie);
        return cookie;
    }
    
    private async Task Login(string cookie)
    {
        var client = GetClientWithCookie(cookie);
        var content = new FormUrlEncodedContent(new Dictionary<string, string>
        {
            { "user", _credentials.Username },
            { "pass", _credentials.Password }
        });
        var response = await client.PostAsync(_configuration.LoginPath, content);
        if (response.StatusCode != HttpStatusCode.Found)
            throw new BaseAppException(
                ErrorCodes.Login,
                $"Login was not successful. Response: {System.Text.Json.JsonSerializer.Serialize(response)}"
            );
    }

    private HttpClient GetClientWithCookie(string cookie)
    {
        var client = _httpClientFactory.CreateClient(Name);
        client.DefaultRequestHeaders.Add("Cookie", cookie);
        return client;
    }
}