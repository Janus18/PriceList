namespace PriceList.Dique.Tools.Exceptions;

public class BaseAppException : ApplicationException
{
    public ErrorCodes Code { get; }

    public BaseAppException(ErrorCodes code, string message) : base(message)
    {
        Code = code;
    }
}