namespace PriceList.Dique.Tools.Exceptions;

public enum ErrorCodes
{
    ProductNotFound = 1,
    PriceNotFound = 2,
    NoCookie = 3,
    Login = 4,
    Timeout = 5,
    Unknown = 6,
    InvalidFormat = 7
}