using System.Text.Json;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using PriceList.Dique.Models;
using PriceList.Dique.Tools.Exceptions;

namespace PriceList.Dique.Controllers.Filters;

public class HttpResponseExceptionFilter : IActionFilter, IOrderedFilter
{
    public void OnActionExecuting(ActionExecutingContext context) { }

    public void OnActionExecuted(ActionExecutedContext context)
    {
        if (context.Exception is not BaseAppException baseAppException) 
            return;

        context.Result = new ContentResult
        {
            StatusCode = 500,
            Content = JsonSerializer.Serialize(
                StandardErrorContent.FromBaseException(baseAppException),
                new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase}
            ),
            ContentType = "application/json"
        };
        context.ExceptionHandled = true;
    }

    public int Order => int.MaxValue - 100;
}