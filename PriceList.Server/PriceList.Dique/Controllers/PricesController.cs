using MediatR;
using Microsoft.AspNetCore.Mvc;
using PriceList.Dique.DTOs;
using PriceList.Dique.Handlers.GetPrice;
using PriceList.Dique.Handlers.GetPrices;

namespace PriceList.Dique.Controllers;

[ApiController]
[Route("[controller]")]
public class PricesController : ControllerBase
{
    private readonly IMediator _mediator;

    public PricesController(
        IMediator mediator
    )
    {
        _mediator = mediator;
    }

    [HttpGet]
    [ResponseCache(Duration = 600, VaryByQueryKeys = new[] { "reference" })]
    public async Task<IActionResult> Get([FromQuery] string reference)
    {
        var result = await _mediator.Send(new GetPriceRequest(reference));
        return Ok(result);
    }

    [HttpPost]
    public async Task<IActionResult> Get([FromBody] GetPricesRequestDto request)
    {
        var result = await _mediator.Send(new GetPricesRequest(request.References));
        return Ok(result);
    }
}