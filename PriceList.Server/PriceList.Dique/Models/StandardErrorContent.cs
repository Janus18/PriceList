using System.Runtime.CompilerServices;
using PriceList.Dique.Tools.Exceptions;

namespace PriceList.Dique.Models;

public record StandardErrorContent(string Code, string Message)
{
    public static StandardErrorContent FromBaseException(BaseAppException exception) =>
        new(exception.Code.ToString(), exception.Message);
}
