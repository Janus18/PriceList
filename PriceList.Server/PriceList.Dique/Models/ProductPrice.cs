namespace PriceList.Dique.Models;

public record ProductPrice(string Reference, decimal Price)
{
    public string Currency => "ARS";
}