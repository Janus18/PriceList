namespace PriceList.Dique.Models;

public record SessionCookie(string Cookie, DateTime CreatedAt)
{
    public virtual bool IsValid(TimeSpan retentionTime) => CreatedAt.Add(retentionTime) >= DateTime.UtcNow;
}

public record InvalidSessionCookie() : SessionCookie("", DateTime.MinValue)
{
    public override bool IsValid(TimeSpan retentionTime) => false;
}
