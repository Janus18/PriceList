namespace PriceList.Dique.Models;

public record MaybeProductPrice(ProductPrice? ProductPrice, StandardErrorContent? Error)
{
    public static MaybeProductPrice FromPrice(ProductPrice productPrice) => new(productPrice, null);

    public static MaybeProductPrice FromError(StandardErrorContent error) => new(null, error);
}
