namespace PriceList.Dique.Models;

public class Configuration
{
    public string LoginPath { get; init; } = "";
    public string PriceHtmlClass { get; init; } = "";
    public TimeSpan CookieRetention { get; init; }
}
