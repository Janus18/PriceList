namespace PriceList.Dique.DTOs;

public record GetPricesRequestDto(IList<string> References);
