using MediatR;
using PriceList.Dique.Models;

namespace PriceList.Dique.Handlers.GetPrices;

public class GetPricesRequest : IRequest<IList<MaybeProductPrice>>
{
    public IList<string> References { get; }

    public GetPricesRequest(IList<string> references)
    {
        References = references;
    }
}