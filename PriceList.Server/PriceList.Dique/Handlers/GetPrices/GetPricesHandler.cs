using MediatR;
using PriceList.Dique.Handlers.GetPrice;
using PriceList.Dique.Models;
using PriceList.Dique.Tools.Exceptions;

namespace PriceList.Dique.Handlers.GetPrices;

public class GetPricesHandler : IRequestHandler<GetPricesRequest, IList<MaybeProductPrice>>
{
    private readonly IMediator _mediator;
    private readonly ILogger<GetPricesHandler> _logger;

    public GetPricesHandler(
        IMediator mediator,
        ILogger<GetPricesHandler> logger
    )
    {
        _mediator = mediator;
        _logger = logger;
    }
    
    public async Task<IList<MaybeProductPrice>> Handle(GetPricesRequest request, CancellationToken cancellationToken)
    {
        var prices = await Task.WhenAll(request.References.Select(TryGetPrice));
        return prices.ToList();
    }

    private async Task<MaybeProductPrice> TryGetPrice(string reference)
    {
        try
        {
            var productPrice = await _mediator.Send(new GetPriceRequest(reference));
            return MaybeProductPrice.FromPrice(productPrice);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Error obtaining price for {reference}", reference);
            var error = ex is BaseAppException baseAppException
                ? StandardErrorContent.FromBaseException(baseAppException)
                : new StandardErrorContent(ErrorCodes.Unknown.ToString(), "Unknown error");
            return MaybeProductPrice.FromError(error);
        }
    }
}