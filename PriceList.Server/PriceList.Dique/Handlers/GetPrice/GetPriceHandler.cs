using System.Globalization;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using MediatR;
using Microsoft.Extensions.Options;
using PriceList.Dique.Models;
using PriceList.Dique.Tools;
using PriceList.Dique.Tools.Exceptions;

namespace PriceList.Dique.Handlers.GetPrice;

public class GetPriceHandler : IRequestHandler<GetPriceRequest, ProductPrice>
{
    private readonly HttpClientWrapper _httpClientWrapper;
    private readonly Configuration _configuration;

    public GetPriceHandler(
        HttpClientWrapper httpClientWrapper,
        IOptions<Configuration> configurationOptions)
    {
        _httpClientWrapper = httpClientWrapper;
        _configuration = configurationOptions.Value;
    }
    
    public async Task<ProductPrice> Handle(GetPriceRequest request, CancellationToken cancellationToken)
    {
        var client = await _httpClientWrapper.Get();
        var response = await client.GetAsync(request.Reference, cancellationToken);
        if (!response.IsSuccessStatusCode)
            throw new BaseAppException(
                ErrorCodes.ProductNotFound,
                $"Request was not successful for product {request.Reference}. Status {response.StatusCode}");
        var content = await response.Content.ReadAsStringAsync(cancellationToken);
        var price = GetPrice(content)
            ?? throw new BaseAppException(
                ErrorCodes.PriceNotFound,
                $"Price was not found for product {request.Reference}");
        return new ProductPrice(request.Reference, price);
    }

    private decimal? GetPrice(string responseContent)
    {
        var document = new HtmlDocument();
        document.LoadHtml(responseContent);
        var priceText = document.DocumentNode
            .Descendants()
            .FirstOrDefault(x => x.HasClass(_configuration.PriceHtmlClass))?
            .InnerText;
        if (string.IsNullOrEmpty(priceText))
            return null;
        var match = new Regex(@"\$\d{1,3}(\.\d{3})*(,\d+)?").Match(priceText);
        if (!match.Success)
            throw new BaseAppException(ErrorCodes.InvalidFormat, $"Price was not in a valid format. Value: {priceText}");
        return decimal.Parse(
            match.Value.Replace("$", ""),
            NumberStyles.Currency,
            CultureInfo.GetCultureInfo("es-AR"));
    }
}