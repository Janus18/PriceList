using MediatR;
using PriceList.Dique.Models;

namespace PriceList.Dique.Handlers.GetPrice;

public class GetPriceRequest : IRequest<ProductPrice>
{
    public string Reference { get; }

    public GetPriceRequest(string reference)
    {
        Reference = reference;
    }
}