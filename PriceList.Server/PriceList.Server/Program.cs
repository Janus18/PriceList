using Hangfire;
using Hangfire.PostgreSql;
using Microsoft.EntityFrameworkCore;
using PriceList.Server.Controllers.Filters;
using PriceList.Server.DataAccess;
using PriceList.Server.DataAccess.Queries;
using PriceList.Server.Jobs;
using PriceList.Server.Tools;

var builder = WebApplication.CreateBuilder(args);

var config = builder.Configuration
    .AddUserSecrets(typeof(Program).Assembly)
    .AddEnvironmentVariables()
    .Build();

builder.Services.AddDbContext<AppDbContext>(options =>
    options.UseNpgsql(
        config.GetConnectionString("PGSQL")
    )
);

builder.Services.AddHttpClient();
builder.Services.AddControllers(options => options.Filters.Add(new HttpResponseExceptionFilter()));

builder.Services.AddMediatR(c => c.RegisterServicesFromAssemblyContaining(typeof(Program)));
builder.Services.AddQueries();

builder.Services.AddScoped<HangfireJobs>();
builder.Services.AddScoped<SynchronizationJob>();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddHangfire(c =>
    c.SetDataCompatibilityLevel(CompatibilityLevel.Version_180)
        .UseSimpleAssemblyNameTypeSerializer()
        .UseRecommendedSerializerSettings()
        .UsePostgreSqlStorage(opts => opts.UseNpgsqlConnection(config.GetConnectionString("PGSQL"))));
builder.Services.AddHangfireServer();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseCors(b => b
    .AllowAnyMethod()
    .AllowAnyHeader()
    .SetIsOriginAllowed(o => o == config.GetValue<string>("CORS_ORIGIN")));

app.UseAuthorization();

app.MapControllers();

app.UseHangfireDashboard(options: new DashboardOptions { Authorization = new [] { new HangfireAuthorization() }});

using (var scope = app.Services.CreateScope())
{
    await scope.ServiceProvider.GetService<AppDbContext>()!.Database.MigrateAsync();
    await scope.ServiceProvider.GetService<HangfireJobs>()!.AddOrUpdateSynchronization();
}

app.Run();
