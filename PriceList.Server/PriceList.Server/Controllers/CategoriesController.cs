using MediatR;
using Microsoft.AspNetCore.Mvc;
using PriceList.Server.DTOs.AddCategory;
using PriceList.Server.DTOs.UpdateCategory;
using PriceList.Server.Handlers.AddCategory;
using PriceList.Server.Handlers.GetCategory;
using PriceList.Server.Handlers.ListCategories;
using PriceList.Server.Handlers.UpdateCategory;

namespace PriceList.Server.Controllers;

[ApiController]
[Route("[controller]")]
public class CategoriesController : ControllerBase
{
    private readonly IMediator _mediator;

    public CategoriesController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpPost]
    public async Task<IActionResult> Add([FromBody] AddCategoryRequestDto input)
    {
        var result = await _mediator.Send(new AddCategoryRequest(input));
        return CreatedAtAction(nameof(Get), new { categoryId = result.Id }, result);
    }

    [HttpPut]
    public async Task<IActionResult> Update([FromBody] UpdateCategoryRequestDto category)
    {
        await _mediator.Send(new UpdateCategoryRequest(category));
        return NoContent();
    }

    [HttpGet("{categoryId:int}")]
    public async Task<IActionResult> Get([FromRoute] int categoryId)
    {
        var result = await _mediator.Send(new GetCategoryRequest(categoryId));
        return result is null ? NotFound() : Ok(result);
    }

    [HttpGet]
    public async Task<IActionResult> Get()
    {
        var result = await _mediator.Send(new ListCategoriesRequest());
        return Ok(result);
    }
}