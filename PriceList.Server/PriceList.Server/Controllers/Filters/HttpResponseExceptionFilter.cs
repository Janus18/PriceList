using System.Text.Json;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using PriceList.Server.Tools.Exceptions;

namespace PriceList.Server.Controllers.Filters;

public class HttpResponseExceptionFilter : IActionFilter, IOrderedFilter
{
    public void OnActionExecuting(ActionExecutingContext context) { }

    public void OnActionExecuted(ActionExecutedContext context)
    {
        if (context.Exception is not ValidationException validationException) 
            return;

        var content = new StandardErrorContent(
            validationException.Code.ToString(),
            validationException.Description);
        context.Result = new ContentResult()
        {
            StatusCode = 400,
            Content = JsonSerializer.Serialize(
                content,
                new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase}
            ),
            ContentType = "application/json"
        };
        context.ExceptionHandled = true;
    }

    public int Order => int.MaxValue - 100;
}