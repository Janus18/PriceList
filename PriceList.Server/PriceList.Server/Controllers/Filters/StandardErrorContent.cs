using PriceList.Server.Tools.Exceptions;

namespace PriceList.Server.Controllers.Filters;

public record StandardErrorContent(string Code, string Message);
