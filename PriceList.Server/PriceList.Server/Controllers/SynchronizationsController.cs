using Hangfire;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using PriceList.Server.DTOs.UpdateSynchronizationConfiguration;
using PriceList.Server.Handlers.GetSynchronizationConfiguration;
using PriceList.Server.Handlers.ListSynchronizations;
using PriceList.Server.Handlers.UpdateSynchronizationConfiguration;
using PriceList.Server.Jobs;

namespace PriceList.Server.Controllers;

[ApiController]
[Route("[controller]")]
public class SynchronizationsController(
    ISender mediator,
    IBackgroundJobClient backgroundJobClient
) : ControllerBase
{
    [HttpGet]
    public async Task<IActionResult> GetAll(
        [FromQuery] DateTime until,
        [FromQuery] ushort maxSize
    )
    {
        until = DateTime.SpecifyKind(until, DateTimeKind.Utc);
        var request = new ListSynchronizationsRequest(until, maxSize);
        var result = await mediator.Send(request);
        return Ok(result);
    }

    [HttpPost]
    public IActionResult Trigger()
    {
        var token = Guid.NewGuid();
        backgroundJobClient.Enqueue<SynchronizationJob>(
            sj => sj.SynchronizeProducts(token, CancellationToken.None));
        return NoContent();
    }

    [HttpGet("configuration")]
    public async Task<IActionResult> GetConfiguration()
    {
        var result = await mediator.Send(new GetSynchronizationConfigurationRequest());
        return Ok(result);
    }
    
    [HttpPut("configuration")]
    public async Task<IActionResult> UpdateConfiguration(UpdateSynchronizationConfigurationRequestDto request)
    {
        await mediator.Send(new UpdateSynchronizationConfigurationRequest(request));
        return NoContent();
    }
}