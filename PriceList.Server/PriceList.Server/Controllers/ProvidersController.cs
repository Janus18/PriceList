using MediatR;
using Microsoft.AspNetCore.Mvc;
using PriceList.Server.DTOs.AddProvider;
using PriceList.Server.DTOs.UpdateProvider;
using PriceList.Server.Handlers.AddProvider;
using PriceList.Server.Handlers.GetProvider;
using PriceList.Server.Handlers.ListProviders;
using PriceList.Server.Handlers.UpdateProvider;

namespace PriceList.Server.Controllers;

[ApiController]
[Route("[controller]")]
public class ProvidersController : ControllerBase
{
    private readonly IMediator _mediator;

    public ProvidersController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpPost]
    public async Task<IActionResult> Add([FromBody] AddProviderRequestDto provider)
    {
        var result = await _mediator.Send(new AddProviderRequest(provider));
        return CreatedAtAction(nameof(Get), new { providerId = result.Id }, result);
    }

    [HttpPut]
    public async Task<IActionResult> Update([FromBody] UpdateProviderRequestDto provider)
    {
        await _mediator.Send(new UpdateProviderRequest(provider));
        return NoContent();
    }

    [HttpGet("{providerId:int}")]
    public async Task<IActionResult> Get([FromRoute] int providerId)
    {
        var result = await _mediator.Send(new GetProviderRequest(providerId));
        return result is null ? NotFound() : Ok(result);
    }

    [HttpGet]
    public async Task<IActionResult> Get()
    {
        var result = await _mediator.Send(new ListProvidersRequest());
        return Ok(result);
    }
}