using MediatR;
using Microsoft.AspNetCore.Mvc;
using PriceList.Server.DTOs.AddProduct;
using PriceList.Server.DTOs.UpdateProduct;
using PriceList.Server.DTOs.UpdateProductsBatch;
using PriceList.Server.Handlers.AddProduct;
using PriceList.Server.Handlers.UpdateProduct;
using PriceList.Server.Handlers.GetProductByCode;
using PriceList.Server.Handlers.GetProductById;
using PriceList.Server.Handlers.ListProductHistory;
using PriceList.Server.Handlers.ListProducts;
using PriceList.Server.Handlers.UpdateProductEnabled;
using PriceList.Server.Handlers.UpdateProductsBatch;

namespace PriceList.Server.Controllers;

[ApiController]
[Route("[controller]")]
public class ProductsController(ISender mediator) : ControllerBase
{
    [HttpPost]
    public async Task<IActionResult> Add([FromBody] AddProductRequestDto product)
    {
        var result = await mediator.Send(new AddProductRequest(product));
        return CreatedAtAction(nameof(Get), new { id = result.Id }, result);
    }

    [HttpPut]
    public async Task<IActionResult> Update([FromBody] UpdateProductRequestDto product)
    {
        await mediator.Send(new UpdateProductRequest(product));
        return NoContent();
    }

    [HttpGet("{id:int}")]
    public async Task<IActionResult> Get([FromRoute] int id)
    {
        var result = await mediator.Send(new GetProductByIdRequest(id));
        return result is not null ? Ok(result) : NotFound();
    }

    [HttpGet]
    public async Task<IActionResult> Get([FromQuery] string code)
    {
        var result = await mediator.Send(new GetProductByCodeRequest(code));
        return result is not null ? Ok(result) : NotFound();
    }

    [HttpGet("all")]
    public async Task<IActionResult> Get(
        [FromQuery] ushort size,
        [FromQuery] DateTime updatedAt,
        [FromQuery] string? description = null,
        [FromQuery] int? categoryId = null,
        [FromQuery] string? code = null,
        [FromQuery] bool frequent = false,
        [FromQuery] int? providerId = null,
        [FromQuery] int? synchronizationId = null
    )
    {
        var request = new ListProductsRequest(
            DateTime.SpecifyKind(updatedAt, DateTimeKind.Utc),
            size,
            categoryId,
            description,
            code,
            providerId,
            synchronizationId,
            frequent);
        var result = await mediator.Send(request);
        return Ok(result);
    }

    [HttpPut("{productId:int}/enable")]
    public async Task<IActionResult> EnableDisable([FromRoute] int productId, [FromQuery] bool value)
    {
        await mediator.Send(new UpdateProductEnabledRequest(productId, value));
        return NoContent();
    }

    [HttpPut("all")]
    public async Task<IActionResult> UpdateBatch([FromBody] UpdateProductsBatchDto dto)
    {
        await mediator.Send(new UpdateProductsBatchRequest(dto));
        return NoContent();
    }

    [HttpGet("{id:int}/history")]
    public async Task<IActionResult> GetHistory([FromRoute] int id, ushort maxSize, DateTime? maxDate = null)
    {
        var request = new ListProductHistoryRequest(id, maxSize, maxDate);
        var result = await mediator.Send(request);
        return Ok(result);
    }
}
