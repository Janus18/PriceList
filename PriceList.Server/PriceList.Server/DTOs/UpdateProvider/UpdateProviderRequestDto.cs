using PriceList.Server.Domain;

namespace PriceList.Server.DTOs.UpdateProvider;

public record UpdateProviderRequestDto(
    int Id,
    string Name,
    string? PricesHost,
    decimal? ProfitMargin,
    decimal? Taxes
)
{
    public void UpdateProvider(Provider provider)
    {
        var update = new Domain.ParameterObjects.UpdateProvider(provider)
        {
            Name = Name,
            PricesHost = PricesHost,
            ProfitMargin = ProfitMargin.HasValue ? new Percentage(ProfitMargin.Value) : null,
            Taxes = Taxes.HasValue ? new Percentage(Taxes.Value) : null
        };
        provider.Update(update);
    }
}
