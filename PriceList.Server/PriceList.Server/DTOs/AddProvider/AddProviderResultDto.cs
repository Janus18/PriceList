namespace PriceList.Server.DTOs.AddProvider;

public record AddProviderResultDto(int Id);