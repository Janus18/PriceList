using PriceList.Server.Domain;

namespace PriceList.Server.DTOs.AddProvider;

public record AddProviderRequestDto
{
    public string Name { get; init; } = "";
    public string? PricesHost { get; init; } = "";
    public decimal? ProfitMargin { get; init; }
    public decimal? Taxes { get; init; }

    public Provider GetProvider()
    {
        return new Provider(
            Name,
            PricesHost,
            ProfitMargin.HasValue ? new Percentage(ProfitMargin.Value) : null,
            Taxes.HasValue ? new Percentage(Taxes.Value) : null
        );
    }
}