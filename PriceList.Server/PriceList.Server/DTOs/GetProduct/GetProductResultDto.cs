using PriceList.Server.Domain;

namespace PriceList.Server.DTOs.GetProduct;

public class GetProductResultDto
{
    public int Id { get; }
    public GetProductResultMoneyDto? BasePrice { get; }
    public GetProductResultCategoryDto? Category { get; }
    public string Code { get; }
    public DateTime CreatedAt { get; }
    public bool Disabled { get; }
    public string Description { get; }
    public GetProductResultProviderDto? Provider { get; }
    public string? ProviderReference { get; }
    public GetProductResultMoneyDto? SellingPrice { get; }
    public GetProductResultMoneyDto? TaxedPrice { get; }
    public GetProductResultMoneyDto? RecommendedSellingPrice { get; }
    public DateTime? UpdatedAt { get; }
    public ushort? Units { get; }
    public bool UpdatedBySystem { get; }

    public GetProductResultDto(Product product)
    {
        Id = product.Id;
        BasePrice = GetProductResultMoneyDto.Create(product.BasePrice, product.Units);
        Category = product.Category is not null ? new GetProductResultCategoryDto(product.Category) : null;
        Code = product.Code;
        CreatedAt = product.CreatedAt;
        Description = product.Description;
        Disabled = product.Disabled;
        Provider = product.Provider is not null ? new GetProductResultProviderDto(product.Provider) : null;
        ProviderReference = product.ProviderReference;
        SellingPrice = GetProductResultMoneyDto.Create(product.SellingPrice, product.Units);
        TaxedPrice = GetProductResultMoneyDto.Create(product.TaxedPrice, product.Units);
        RecommendedSellingPrice = GetProductResultMoneyDto.Create(product.RecommendedSellingPrice, product.Units);
        UpdatedAt = product.UpdatedAt;
        Units = product.Units;
        UpdatedBySystem = product.UpdatedBySystem;
    }

    public GetProductResultDto(ProductHistory product)
    {
        Id = product.Id;
        BasePrice = GetProductResultMoneyDto.Create(product.BasePrice, product.Units);
        Category = product.Category is not null ? new GetProductResultCategoryDto(product.Category) : null;
        Code = product.Code;
        CreatedAt = product.CreatedAt;
        Description = product.Description;
        Provider = product.Provider is not null ? new GetProductResultProviderDto(product.Provider) : null;
        ProviderReference = product.ProviderReference;
        SellingPrice = GetProductResultMoneyDto.Create(product.SellingPrice, product.Units);
        TaxedPrice = GetProductResultMoneyDto.Create(product.TaxedPrice, product.Units);
        RecommendedSellingPrice = GetProductResultMoneyDto.Create(product.RecommendedSellingPrice, product.Units);
        Units = product.Units;
        UpdatedBySystem = product.UpdatedBySystem;
    }
}

public record GetProductResultMoneyDto(string Currency, decimal Amount, decimal UnitAmount)
{
    public GetProductResultMoneyDto(Money money, ushort? units)
        : this(money.Currency.ToString(), money.Amount, money.Amount / (units ?? 1))
    {
    }

    public static GetProductResultMoneyDto? Create(Money? money, ushort? units) =>
        money is null ? null : new GetProductResultMoneyDto(money, units);
}

public record GetProductResultCategoryDto(int Id, string Description)
{
    public GetProductResultCategoryDto(Category category) : this(category.Id, category.Description)
    {
    }
}

public record GetProductResultProviderDto(int Id, string Name, string? PricesHost)
{
    public GetProductResultProviderDto(Provider provider) : this(provider.Id, provider.Name, provider.PricesHost)
    {
    }
}
