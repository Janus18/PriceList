namespace PriceList.Server.DTOs.UpdateSynchronizationConfiguration;

public record UpdateSynchronizationConfigurationRequestDto(
    bool Enabled,
    ushort[] Days,
    ushort[] Hours,
    string Timezone);