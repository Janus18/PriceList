using PriceList.Server.Domain;

namespace PriceList.Server.DTOs.UpdateCategory;

public class UpdateCategoryRequestDto
{
    public int Id { get; init; }
    public string Description { get; init; } = "";

    public void UpdateCategory(Category category)
    {
        var update = new Domain.ParameterObjects.UpdateCategory(category)
        {
            Description = Description
        };
        category.Update(update);
    }
}