using PriceList.Server.Domain.Configurations;

namespace PriceList.Server.DTOs.GetSynchronizationConfiguration;

public record GetSynchronizationConfigurationResponseDto(
    bool Enabled,
    IList<ushort> Days,
    IList<ushort> Hours,
    string Timezone)
{
    public GetSynchronizationConfigurationResponseDto(SynchronizationConfiguration configuration)
        : this(configuration.Enabled, configuration.Days, configuration.Hours, configuration.TimeZoneName)
    {
    }
}
