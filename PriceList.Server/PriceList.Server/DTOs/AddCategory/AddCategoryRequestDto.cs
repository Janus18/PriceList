using PriceList.Server.Domain;

namespace PriceList.Server.DTOs.AddCategory;

public record AddCategoryRequestDto
{
    public string Description { get; init; } = "";
    
    public Category ToCategory()
    {
        return new Category(Description);
    }
}