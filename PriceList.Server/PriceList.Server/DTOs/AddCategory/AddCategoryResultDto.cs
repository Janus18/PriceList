namespace PriceList.Server.DTOs.AddCategory;

public record AddCategoryResultDto(int Id);
