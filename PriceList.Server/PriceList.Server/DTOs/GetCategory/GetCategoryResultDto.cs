using PriceList.Server.Domain;

namespace PriceList.Server.DTOs.GetCategory;

public record GetCategoryResultDto(
    int Id,
    string Description,
    DateTime CreatedAt,
    DateTime? UpdatedAt
)
{
    public GetCategoryResultDto(Category category) : this(
        category.Id,
        category.Description,
        category.CreatedAt,
        category.UpdatedAt
    ) { }
}
