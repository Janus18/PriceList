using PriceList.Server.Domain;

namespace PriceList.Server.DTOs.GetProvider;

public record GetProviderResultDto(
    DateTime CreatedAt,
    int Id,
    string Name,
    string? PricesHost,
    decimal? ProfitMargin,
    decimal? Taxes,
    DateTime? UpdatedAt)
{
    public GetProviderResultDto(Provider provider) : this(
        provider.CreatedAt,
        provider.Id,
        provider.Name,
        provider.PricesHost,
        provider.ProfitMargin?.Value,
        provider.Taxes?.Value,
        provider.UpdatedAt)
    {
    }
}