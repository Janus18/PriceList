using PriceList.Server.Domain.Synchronizations;

namespace PriceList.Server.DTOs.ListSynchronizations;

public record ListSynchronizationsResultDto
{
    public int Id { get; }
    public DateTime CreatedAt { get; }
    public DateTime? FinishedAt { get; }
    public int Total { get; }
    public int Updated { get; }
    public int Failed { get; }
    public ListSynchronizationsResultDtoMessage[] Errors { get; }

    public ListSynchronizationsResultDto(Synchronization synchronization)
    {
        Id = synchronization.Id;
        CreatedAt = synchronization.CreatedAt;
        FinishedAt = synchronization.FinishedAt;
        Total = synchronization.Total;
        Updated = synchronization.Updated;
        Failed = synchronization.Failed;
        Errors = synchronization.Errors?
            .Errors.Select(x => new ListSynchronizationsResultDtoMessage(x)).ToArray() 
                 ?? Array.Empty<ListSynchronizationsResultDtoMessage>();
    }
}

public record ListSynchronizationsResultDtoMessage(int ProductId, string Message)
{
    public ListSynchronizationsResultDtoMessage(SyncMessage message) : this(message.ProductId, message.Message)
    {
    }
}
