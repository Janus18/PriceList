using PriceList.Server.DataAccess.Queries.FindCategoryById;
using PriceList.Server.DataAccess.Queries.FindProviderById;
using PriceList.Server.Domain;
using PriceList.Server.Tools.Exceptions;

namespace PriceList.Server.DTOs.UpdateProduct;

public class UpdateProductRequestDto
{
    public int Id { get; init; }
    public string Code { get; init; } = "";
    public string Description { get; init; } = "";
    public UpdateProductRequestMoneyDto? BasePrice { get; init; }
    public UpdateProductRequestMoneyDto? SellingPrice { get; init; }
    public UpdateProductRequestMoneyDto? TaxedPrice { get; init; }
    public int? CategoryId { get; init; }
    public string? ProviderReference { get; init; }
    public int? ProviderId { get; init; }
    public ushort? Units { get; init; }

    public async Task UpdateProduct(
        Product product,
        IFindCategoryById findCategoryById,
        IFindProviderById findProviderById,
        CancellationToken cancellationToken
    )
    {
        var updateProductPo = new Domain.ParameterObjects.UpdateProduct(product)
        {
            BasePrice = BasePrice?.GetMoney(),
            Code = Code,
            Description = Description,
            Category = CategoryId == product.Category?.Id
                ? product.Category
                : await GetCategory(findCategoryById, cancellationToken),
            SellingPrice = SellingPrice?.GetMoney(),
            TaxedPrice = TaxedPrice?.GetMoney(),
            ProviderReference = ProviderReference,
            Provider = ProviderId == product.Provider?.Id
                ? product.Provider
                : await GetProvider(findProviderById, cancellationToken),
            Units = Units
        };
        product.Update(updateProductPo);
    }
    
    private async Task<Category?> GetCategory(IFindCategoryById findCategoryById, CancellationToken cancellationToken)
    {
        if (!CategoryId.HasValue)
        {
            return null;
        }
        return await findCategoryById.Execute(CategoryId.Value, cancellationToken)
               ?? throw new ValidationException(
                   ErrorCodes.CategoryNotFound,
                   nameof(CategoryId),
                   $"No category found with ID {CategoryId.Value}");
    }
    
    private async Task<Provider?> GetProvider(IFindProviderById findProviderById, CancellationToken cancellationToken)
    {
        if (!ProviderId.HasValue)
        {
            return null;
        }
        return await findProviderById.Find(ProviderId.Value, cancellationToken)
               ?? throw new ValidationException(
                   ErrorCodes.ProviderNotFound,
                   nameof(ProviderId),
                   $"No provider found with ID {ProviderId.Value}");
    }
}

public class UpdateProductRequestMoneyDto
{
    public string Currency { get; init; } = "";
    public decimal Amount { get; init; }

    public Money GetMoney()
    {
        if (!Enum.TryParse<Currency>(Currency, out var currency))
            throw new ValidationException(
                ErrorCodes.InvalidCurrency,
                nameof(Currency),
                $"{Currency} is not a valid currency");
        return new Money(currency, Amount);
    }
}
