namespace PriceList.Server.DTOs.AddProduct;

public record AddProductResultDto(int Id);
