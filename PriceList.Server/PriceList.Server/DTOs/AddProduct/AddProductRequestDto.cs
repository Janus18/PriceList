using PriceList.Server.DataAccess.Queries.FindCategoryById;
using PriceList.Server.DataAccess.Queries.FindProviderById;
using PriceList.Server.Domain;
using PriceList.Server.Tools.Exceptions;

namespace PriceList.Server.DTOs.AddProduct;

public class AddProductRequestDto
{
    public AddProductRequestMoneyDto? BasePrice { get; init; }
    public int? CategoryId { get; init; }   
    public string? Code { get; init; }
    public string Description { get; init; } = "";
    public int? ProviderId { get; init; }
    public string? ProviderReference { get; init; }
    public AddProductRequestMoneyDto? SellingPrice { get; init; }
    public AddProductRequestMoneyDto? TaxedPrice { get; init; }
    public ushort? Units { get; init; }

    public async Task<Product> GetProduct(
        IFindCategoryById findCategoryById,
        IFindProviderById findProviderById,
        CancellationToken cancellationToken
    )
    {
        return new Product(
            string.IsNullOrEmpty(Code) ? Guid.NewGuid().ToString("N") : Code,
            Description,
            BasePrice?.GetMoney(),
            SellingPrice?.GetMoney(),
            TaxedPrice?.GetMoney(),
            await GetCategory(findCategoryById, cancellationToken),
            ProviderReference,
            await GetProvider(findProviderById, cancellationToken),
            Units);
    }

    private async Task<Category?> GetCategory(IFindCategoryById findCategoryById, CancellationToken cancellationToken)
    {
        if (!CategoryId.HasValue)
            return null;
        return await findCategoryById.Execute(CategoryId.Value, cancellationToken)
           ?? throw new ValidationException(
               ErrorCodes.CategoryNotFound,
               nameof(CategoryId),
               $"No category found with ID {CategoryId.Value}");
    }
    
    private async Task<Provider?> GetProvider(IFindProviderById findProviderById, CancellationToken cancellationToken)
    {
        if (!ProviderId.HasValue)
            return null;
        return await findProviderById.Find(ProviderId.Value, cancellationToken)
               ?? throw new ValidationException(
                   ErrorCodes.ProviderNotFound,
                   nameof(ProviderId),
                   $"No provider found with ID {ProviderId.Value}");
    }
}

public class AddProductRequestMoneyDto
{
    public string Currency { get; init; } = "";
    public decimal Amount { get; init; }

    public Money GetMoney()
    {
        if (!Enum.TryParse<Currency>(Currency, out var currency))
            throw new ValidationException(
                ErrorCodes.InvalidCurrency,
                nameof(Currency),
                $"{Currency} is not a valid currency");
        return new Money(currency, Amount);
    }
}
