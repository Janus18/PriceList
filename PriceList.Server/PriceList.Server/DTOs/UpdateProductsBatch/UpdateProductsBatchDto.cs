using PriceList.Server.Handlers.UpdateProductsBatch;

namespace PriceList.Server.DTOs.UpdateProductsBatch;

public class UpdateProductsBatchDto
{
    public IList<int> ProductsIds { get; init; } = new List<int>();
    public decimal? BasePriceIncrement { get; init; }
    public decimal? TaxedPriceIncrement { get; init; }
    public decimal? SellingPriceIncrement { get; init; }
    public bool? Enabled { get; init; }
    public int? CategoryId { get; init; }
    public int? ProviderId { get; init; }
}