using PriceList.Server.Domain.ParameterObjects;

namespace PriceList.Server.Domain;

public class Provider
{
    public int Id { get; private set; }
    public string Name { get; private set; } = "";
    public string? PricesHost { get; private set; }
    public Percentage? Taxes { get; private set; }
    public Percentage? ProfitMargin { get; private set; }
    public DateTime CreatedAt { get; private set; }
    public DateTime? UpdatedAt { get; private set; }
    
    private Provider() {}

    public Provider(
        string name,
        string? pricesHost,
        Percentage? profitMargin,
        Percentage? taxes
    )
    {
        CreatedAt = DateTime.UtcNow;
        Name = name;
        PricesHost = pricesHost;
        ProfitMargin = profitMargin;
        Taxes = taxes;
        UpdatedAt = null;
    }

    public void Update(UpdateProvider update)
    {
        Name = update.Name;
        PricesHost = update.PricesHost;
        ProfitMargin = update.ProfitMargin;
        Taxes = update.Taxes;
        UpdatedAt = DateTime.UtcNow;
    }
}