namespace PriceList.Server.Domain.ParameterObjects;

public record UpdateProvider(
    string Name,
    string? PricesHost,
    Percentage? ProfitMargin,
    Percentage? Taxes
)
{
    public UpdateProvider(Provider provider) : this(
        provider.Name,
        provider.PricesHost,
        provider.ProfitMargin,
        provider.Taxes
    ) { }
}