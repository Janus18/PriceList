namespace PriceList.Server.Domain.ParameterObjects;

public struct UpdateCategory
{
    public string Description { get; init; }

    public UpdateCategory(Category category)
    {
        Description = category.Description;
    }
}