namespace PriceList.Server.Domain.ParameterObjects;

public record UpdateProduct
{
    public Money? BasePrice { get; init; }
    public Category? Category { get; init; }
    public string Code { get; init; }
    public string Description { get; init; }
    public Provider? Provider { get; init; }
    public string? ProviderReference { get; init; }
    public Money? SellingPrice { get; init; }
    public Money? TaxedPrice { get; init; }
    public ushort? Units { get; init; }

    public UpdateProduct(Product product)
    {
        BasePrice = product.BasePrice;
        Category = product.Category;
        Code = product.Code;
        Description = product.Description;
        Provider = product.Provider;
        ProviderReference = product.ProviderReference;
        SellingPrice = product.SellingPrice;
        TaxedPrice = product.TaxedPrice;
        Units = product.Units;
    }
}