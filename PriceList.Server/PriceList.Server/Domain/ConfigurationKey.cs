namespace PriceList.Server.Domain;

public enum ConfigurationKey
{
    RecurringJobsCron = 1,
    RecurringJobsEnabled = 2,
    RecurringJobsTimeOffset = 3,
    RecurringJobsTimeZone = 4,
}