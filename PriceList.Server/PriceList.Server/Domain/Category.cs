using PriceList.Server.Domain.ParameterObjects;

namespace PriceList.Server.Domain;

public class Category
{
    public int Id { get; private set; }
    public string Description { get; private set; } = "";
    public DateTime CreatedAt { get; private set; }
    public DateTime? UpdatedAt { get; private set; }

    private Category() {}
    
    public Category(string description)
    {
        Description = description;
        CreatedAt = DateTime.UtcNow;
        UpdatedAt = null;
    }

    public void Update(UpdateCategory update)
    {
        Description = update.Description;
        UpdatedAt = DateTime.UtcNow;
    }
}