using System.Text.RegularExpressions;
using PriceList.Server.Tools.Exceptions;

namespace PriceList.Server.Domain.Configurations;

/// <summary>
/// <para>Helps with the configuration of the automatic synchronizations.</para>
/// <para>Mainly, it takes care of validation and conversion between cron expressions and Days and Hours,
/// which is an abstraction for the users.</para>
/// </summary>
public record SynchronizationConfiguration
{
    public const ConfigurationKey CronKey = ConfigurationKey.RecurringJobsCron;
    public const ConfigurationKey EnabledKey = ConfigurationKey.RecurringJobsEnabled;
    public const ConfigurationKey TimeZoneKey = ConfigurationKey.RecurringJobsTimeZone;
    
    private const ushort DaysMax = 6;
    private const ushort HoursMax = 23;

    public Configuration CronConfiguration { get; }
    public Configuration EnabledConfiguration { get; }
    public Configuration TimeZoneConfiguration { get; }

    public ushort[] Days { get; private set; }
    public ushort[] Hours { get; private set; }
    public bool Enabled { get; private set; }
    public string TimeZoneName { get; private set; }

    public string Cron
    {
        get
        {
            var hours = Hours.Length == HoursMax + 1 ? "*" : string.Join(',', Hours);
            var days = Days.Length == DaysMax + 1 ? "*" : string.Join(',', Days);
            return $"0 {hours} * * {days}"; 
        }
    }

    public TimeZoneInfo TimeZone => 
        TimeZoneInfo.TryFindSystemTimeZoneById(TimeZoneName, out var tz)
        ? tz : TimeZoneInfo.Utc;
    
    /// <summary>
    /// <para>Receives a list of possibly existing configurations. If any of the configurations do not exist yet,
    /// those will be created here with default values.</para>
    /// <para>If a cron configuration is passed, it should have a valid cron expression as value.</para>
    /// <para>Similarly, if a enabled configuration is passed, it should have a valid boolean as value.</para>
    /// </summary>
    /// <param name="configurations"></param>
    /// <exception cref="ValidationException">Some of the configurations is not valid</exception>
    public SynchronizationConfiguration(IList<Configuration> configurations)
    {
        CronConfiguration = InitCronConfiguration(configurations);
        EnabledConfiguration = InitEnabledConfiguration(configurations);
        TimeZoneConfiguration = InitTimeZoneConfiguration(configurations);
        var groups = CronConfiguration.Value.Split(' ');
        Hours = ParseCronGroup(groups[1], HoursMax);
        Days = ParseCronGroup(groups[4], DaysMax);
        Enabled = bool.Parse(EnabledConfiguration.Value);
        TimeZoneName = TimeZoneConfiguration.Value;
    }

    private static Configuration InitTimeZoneConfiguration(IEnumerable<Configuration> configurations)
        => configurations.FirstOrDefault(c => c.Key == TimeZoneKey) ?? new Configuration(TimeZoneKey, "");
    
    private static Configuration InitEnabledConfiguration(IEnumerable<Configuration> configurations)
    {
        var enabledConfiguration = configurations.FirstOrDefault(c => c.Key == EnabledKey);
        if (enabledConfiguration is null)
            return new Configuration(EnabledKey, true.ToString());
        if (!IsValidBool(enabledConfiguration.Value))
            InvalidEnabledExpression(enabledConfiguration);
        return enabledConfiguration;
    }

    private static Configuration InitCronConfiguration(IEnumerable<Configuration> configurations)
    {
        var cronConfiguration = configurations.FirstOrDefault(c => c.Key == CronKey);
        if (cronConfiguration is null)
            return new Configuration(CronKey, "0 11 * * *");
        if (!IsValidCron(cronConfiguration.Value))
            InvalidCronExpression(cronConfiguration);
        return cronConfiguration;
    }

    public void UpdateCron(IEnumerable<ushort> days, IEnumerable<ushort> hours)
    {
        var nonRepeatingDays = days.ToHashSet().ToArray();
        var nonRepeatingHours = hours.ToHashSet().ToArray();
        if (nonRepeatingDays.Length < 1 || !nonRepeatingDays.All(d => d <= DaysMax))
            InvalidDays();
        if (nonRepeatingHours.Length < 1 || !nonRepeatingHours.All(h => h <= HoursMax))
            InvalidHours();
        Days = nonRepeatingDays;
        Hours = nonRepeatingHours;
        CronConfiguration.Update(Cron);
    }

    public void SetStatus(bool enabled)
    {
        EnabledConfiguration.Update(enabled.ToString());
        Enabled = enabled;
    }

    public void SetTimeZone(string timeZoneName)
    {
        TimeZoneName = timeZoneName;
        TimeZoneConfiguration.Update(timeZoneName);
    }

    private static bool IsValidCron(string cron) => Regex.IsMatch(cron, @"^(((\d+,)*\d+|\*)\s){4}((\d+,)*\d+|\*)$");

    private static bool IsValidBool(string value) => bool.TryParse(value, out _);
    
    private static ushort[] ParseCronGroup(string group, ushort maxExpected)
    {
        if (group == "*")
            return Enumerable.Range(0, maxExpected + 1).Select(x => (ushort)x).ToArray();
        var groups = group.Split(',');
        return groups
            .Select(g => ushort.TryParse(g, out var parsed) ? (ushort?)parsed : null)
            .Where(g => g.HasValue)
            .Select(g => g!.Value)
            .Where(g => g <= maxExpected)
            .ToArray();
    }

    private static void InvalidCronExpression(Configuration configuration)
    {
        throw new ValidationException(
            ErrorCodes.CronInvalidExpression,
            $"The cron expression \"{configuration.Value}\" is not valid for this application.");
    }

    private static void InvalidEnabledExpression(Configuration enabledConfiguration)
    {
        throw new ValidationException(
            ErrorCodes.EnabledInvalidExpression,
            $"The enabled expression \"{enabledConfiguration.Value}\" is not valid. It must be a boolean value");
    }
    
    private static void InvalidHours()
    {
        throw new ValidationException(
            ErrorCodes.CronInvalidHours,
            $"Hours should be between 0 and {HoursMax}, and must include at least one");
    }

    private static void InvalidDays()
    {
        throw new ValidationException(
            ErrorCodes.CronInvalidDays,
            $"Days should be between 0 and {DaysMax}, and must include at least one");
    }
}