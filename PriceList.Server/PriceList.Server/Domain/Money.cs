namespace PriceList.Server.Domain;

public record Money(Currency Currency, decimal Amount)
{
    public Money ApplyPercentage(Percentage percentage) => this with { Amount = percentage.Apply(Amount) };
    
    public override string ToString() => $"{Currency} {Amount}";
}
