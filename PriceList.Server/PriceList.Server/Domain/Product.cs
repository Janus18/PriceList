using PriceList.Server.Domain.ParameterObjects;

namespace PriceList.Server.Domain;

public class Product
{
    public int Id { get; private set; }
    public Money? BasePrice { get; private set; }
    public string Code { get; private set; } = "";
    public DateTime CreatedAt { get; private set; }
    public bool Disabled { get; private set; }
    public string Description { get; private set; } = "";
    public string? ProviderReference { get; private set; }
    public Money? SellingPrice { get; private set; }
    public DateTime UpdatedAt { get; private set; }
    public Money? TaxedPrice { get; private set; }
    public Money? RecommendedSellingPrice { get; private set; }
    public ushort? Units { get; private set; }
    public bool UpdatedBySystem { get; private set; }
    
    public int? CategoryId { get; private set; }
    public Category? Category { get; private set; }
    
    public int? ProviderId { get; private set; }
    public Provider? Provider { get; private set; }

    private Product() {}
    
    public Product(
        string code,
        string description,
        Money? basePrice,
        Money? sellingPrice,
        Money? taxedPrice,
        Category? category,
        string? providerReference,
        Provider? provider,
        ushort? units
    )
    {
        BasePrice = basePrice;
        Category = category;
        Code = code;
        CreatedAt = DateTime.UtcNow;
        Description = description;
        Disabled = false;
        Provider = provider;
        ProviderReference = providerReference;
        SellingPrice = sellingPrice;
        TaxedPrice = taxedPrice;
        Units = units;
        UpdatedAt = CreatedAt;
    }
    
    public void Update(UpdateProduct updateProduct)
    {
        BasePrice = updateProduct.BasePrice;
        Category = updateProduct.Category;
        Code = updateProduct.Code;
        Description = updateProduct.Description;
        Provider = updateProduct.Provider;
        ProviderReference = updateProduct.ProviderReference;
        SellingPrice = updateProduct.SellingPrice;
        UpdatedAt = DateTime.UtcNow;
        UpdatedBySystem = false;
        TaxedPrice = updateProduct.TaxedPrice;
        Units = updateProduct.Units;
    }

    public bool HasPricesUri => !string.IsNullOrEmpty(Provider?.PricesHost) && !string.IsNullOrEmpty(ProviderReference);
    
    public Uri? PricesUri
    {
        get
        {
            if (!HasPricesUri)
                return null;
            var builder = new UriBuilder(Provider!.PricesHost!)
            {
                Path = "prices",
                Query = $"reference={Uri.EscapeDataString(ProviderReference!)}"
            };
            return builder.Uri;
        }
    }

    public bool SyncPrices(Money basePrice)
    {
        if (BasePrice == basePrice)
            return false;
        BasePrice = basePrice;
        TaxedPrice = Provider?.Taxes != null ? basePrice.ApplyPercentage(Provider.Taxes) : TaxedPrice;
        RecommendedSellingPrice = Provider is { Taxes: not null, ProfitMargin: not null }
            ? TaxedPrice!.ApplyPercentage(Provider.ProfitMargin)
            : RecommendedSellingPrice;
        UpdatedAt = DateTime.UtcNow;
        UpdatedBySystem = true;
        return true;
    }

    public void Disable() => Disabled = true;
    public void Enable() => Disabled = false;
}