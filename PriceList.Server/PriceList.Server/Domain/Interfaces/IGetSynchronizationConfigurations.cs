using PriceList.Server.Domain.Configurations;

namespace PriceList.Server.Domain.Interfaces;

public interface IGetSynchronizationConfigurations
{
    Task<SynchronizationConfiguration> Get(CancellationToken cancellationToken);
    Task<IList<Configuration>> GetConfigurations(CancellationToken cancellationToken);
}