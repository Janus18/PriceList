namespace PriceList.Server.Domain;

public record Percentage(decimal Value)
{
    public decimal Apply(decimal x) => x * (1 + Value / 100);
}