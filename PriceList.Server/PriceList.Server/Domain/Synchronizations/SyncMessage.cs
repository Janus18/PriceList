namespace PriceList.Server.Domain.Synchronizations;

public record SyncMessage(int ProductId, string Message);
