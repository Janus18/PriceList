namespace PriceList.Server.Domain.Synchronizations;

public enum SyncStatus
{
    Updated,
    NotUpdated,
    Failed,
}
