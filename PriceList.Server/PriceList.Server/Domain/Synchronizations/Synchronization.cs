namespace PriceList.Server.Domain.Synchronizations;

public class Synchronization
{
    /// <summary>
    /// Maximum numbers of errors which can be stored in each instance of this class.
    /// For more info, you'll have to check the logs.
    /// </summary>
    private const int MaxErrors = 10;

    public int Id { get; private set; }
    public Guid Token { get; private set; }
    public DateTime CreatedAt { get; private set; } = DateTime.UtcNow;
    public DateTime? FinishedAt { get; private set; }
    public int Updated { get; private set; }
    public int Failed { get; private set; }

    /// <summary>
    /// Total number of products synchronized. It includes products updated, not updated, and failed. 
    /// </summary>
    public int Total { get; private set; }

    public SyncMessages? Errors { get; private set; }

    private Synchronization()
    {
    }

    public Synchronization(Guid token)
    {
        Token = token;
    }

    public void Finish()
    {
        if (FinishedAt.HasValue)
            return;
        FinishedAt = DateTime.UtcNow;
    }

    /// <summary>
    /// <para>Adds one to the Total count of products synchronized. If the product was updated, or it failed
    /// to sync, the method also adds one to the corresponding counter.</para>
    /// <para>Nothing changes if the synchronization is finished.</para> 
    /// </summary>
    /// <param name="status">Product synchronization status</param>
    /// <param name="productId">ID of the product synchronized</param>
    /// <param name="message">Optional message to include in the record</param>
    public void Add(SyncStatus status, int productId, string? message = null)
    {
        if (FinishedAt.HasValue)
            return;
        switch (status)
        {
            case SyncStatus.Updated:
                Updated++;
                break;
            case SyncStatus.NotUpdated:
                break;
            case SyncStatus.Failed:
                Failed++;
                AddError(productId, message);
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(status), status, null);
        }

        Total++;
    }

    private void AddError(int productId, string? message)
    {
        if (string.IsNullOrEmpty(message))
            return;
        if (Errors is { Length: >= MaxErrors })
            return;
        if (Errors is null)
            Errors = new SyncMessages();
        Errors.Add(new SyncMessage(productId, message));
    }
}
