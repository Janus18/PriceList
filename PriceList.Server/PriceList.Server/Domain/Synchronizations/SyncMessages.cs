using System.Collections.Immutable;
using System.Text.Json;
using PriceList.Server.Tools;

namespace PriceList.Server.Domain.Synchronizations;

public record SyncMessages
{
    private string? JsonErrors { get; set; }

    public IImmutableList<SyncMessage> Errors => JsonSerializerExtensions.DeserializeOrDefault(JsonErrors ?? "[]", ImmutableList<SyncMessage>.Empty);

    public SyncMessages() { }
    
    public SyncMessages(string jsonErrors)
    {
        JsonErrors = jsonErrors;
    }
    
    public void Add(SyncMessage message)
    {
        JsonErrors = JsonSerializer.Serialize(Errors.ToImmutableList().Add(message));
    }

    public int Length => Errors.Count;

    public override string ToString() => JsonErrors ?? "[]";
}