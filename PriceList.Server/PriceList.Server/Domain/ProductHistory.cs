namespace PriceList.Server.Domain;

public class ProductHistory
{
    public int Id { get; private set; }
    public Money? BasePrice { get; private set; }
    public Category? Category { get; private set; }
    public string Code { get; private set; } = "";
    public DateTime CreatedAt { get; private set; }
    public string Description { get; private set; } = "";
    public Provider? Provider { get; private set; }
    public string? ProviderReference { get; private set; }
    public Money? SellingPrice { get; private set; }
    public Money? TaxedPrice { get; private set; }
    public Money? RecommendedSellingPrice { get; private set; }
    public ushort? Units { get; private set; }
    public bool UpdatedBySystem { get; private set; }
    public int ProductId { get; private set; }
    public Product Product { get; private set; } = null!;
    
    protected ProductHistory() { }

    public ProductHistory(Product product)
    {
        ProductId = product.Id;
        Product = product;
    }
}