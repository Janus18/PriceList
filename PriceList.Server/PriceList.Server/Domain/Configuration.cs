namespace PriceList.Server.Domain;

public class Configuration
{
    public ConfigurationKey Key { get; private set; }
    public string Value { get; private set; } = "";

    public bool IsNew { get; } = false;
    
    protected Configuration()
    {
    }
    
    public Configuration(ConfigurationKey key, string value)
    {
        Key = key;
        Value = value;
        IsNew = true;
    }

    public void Update(string value)
    {
        Value = value;
    }
}