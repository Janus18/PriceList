namespace PriceList.Server.Tools;

public static class Assert
{
    public static void GreaterThanOrEqualTo(int a, int b)
    {
        if (a >= b)
            return;
        throw new Exception($"Assertion failed: {a} is less than {b}");
    }
}