using System.Text.Json;

namespace PriceList.Server.Tools;

public static class JsonSerializerExtensions
{
    public static T DeserializeOrDefault<T>(string s, T @default)
    {
        try
        {
            return JsonSerializer.Deserialize<T>(s) ?? @default;
        }
        catch (Exception)
        {
            return @default;
        }
    }
}