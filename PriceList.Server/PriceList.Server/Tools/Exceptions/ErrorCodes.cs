namespace PriceList.Server.Tools.Exceptions;

public enum ErrorCodes
{
    InvalidCurrency = 1,
    CategoryNotFound = 2,
    ProductCodeExists = 3,
    ProductNotFound = 4,
    ProviderNotFound = 5,
    CronInvalidDays = 7,
    CronInvalidHours = 8,
    CronInvalidExpression = 9,
    EnabledInvalidExpression = 12,
}