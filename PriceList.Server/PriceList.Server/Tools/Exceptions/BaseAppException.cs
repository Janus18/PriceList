namespace PriceList.Server.Tools.Exceptions;

public abstract class BaseAppException : ApplicationException
{
    public ErrorCodes Code { get; }

    public BaseAppException(ErrorCodes code, string message) : base(message)
    {
        Code = code;
    }
}