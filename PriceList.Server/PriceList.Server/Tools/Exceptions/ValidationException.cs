namespace PriceList.Server.Tools.Exceptions;

public class ValidationException : BaseAppException
{
    private string? Field { get; }

    public ValidationException(ErrorCodes errorCode, string message) : base(errorCode, message)
    {
    }
    
    public ValidationException(ErrorCodes errorCode, string field, string message) : this(errorCode, message)
    {
        Field = field;
    }

    public string Description => !string.IsNullOrEmpty(Field) ? $"{Field}: {Message}" : Message;
}