using Hangfire.Dashboard;

namespace PriceList.Server.Tools;

public class HangfireAuthorization : IDashboardAuthorizationFilter
{
    public bool Authorize(DashboardContext context) => true;
}
