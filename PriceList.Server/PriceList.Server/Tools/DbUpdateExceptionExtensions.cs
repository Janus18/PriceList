using Microsoft.EntityFrameworkCore;
using Npgsql;

namespace PriceList.Server.Tools;

public static class DbUpdateExceptionExtensions
{
    public static bool IsUniqueConstraintViolation(this DbUpdateException dbUpdateException)
        => dbUpdateException.InnerException is PostgresException { SqlState: "23505" };
}