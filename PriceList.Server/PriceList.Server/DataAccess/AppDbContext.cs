using Microsoft.EntityFrameworkCore;
using PriceList.Server.DataAccess.Configurations;
using PriceList.Server.Domain;

namespace PriceList.Server.DataAccess;

public class AppDbContext(DbContextOptions<AppDbContext> options) : DbContext(options)
{
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        modelBuilder.ApplyConfiguration(new ProductConfiguration());
        modelBuilder.ApplyConfiguration(new CategoryConfiguration());
        modelBuilder.ApplyConfiguration(new ProviderConfiguration());
        modelBuilder.ApplyConfiguration(new ProductHistoryConfiguration());
        modelBuilder.ApplyConfiguration(new SynchronizationConfiguration());
        modelBuilder.ApplyConfiguration(new ConfigurationConfiguration());
        modelBuilder.UseIdentityColumns();
    }
}