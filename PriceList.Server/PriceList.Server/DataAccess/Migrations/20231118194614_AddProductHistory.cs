﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace PriceList.Server.DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class AddProductHistory : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ProductHistory",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityAlwaysColumn),
                    BasePrice_Currency = table.Column<string>(type: "text", nullable: true),
                    BasePrice_Amount = table.Column<decimal>(type: "numeric(19,5)", nullable: true),
                    CategoryId = table.Column<int>(type: "integer", nullable: true),
                    Code = table.Column<string>(type: "character varying(500)", maxLength: 500, nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: false),
                    ProviderId = table.Column<int>(type: "integer", nullable: true),
                    ProviderReference = table.Column<string>(type: "text", nullable: true),
                    SellingPrice_Currency = table.Column<string>(type: "text", nullable: false),
                    SellingPrice_Amount = table.Column<decimal>(type: "numeric(19,5)", nullable: false),
                    TaxedPrice_Currency = table.Column<string>(type: "text", nullable: true),
                    TaxedPrice_Amount = table.Column<decimal>(type: "numeric(19,5)", nullable: true),
                    ProductId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductHistory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductHistory_Category_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Category",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProductHistory_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductHistory_Provider_ProviderId",
                        column: x => x.ProviderId,
                        principalTable: "Provider",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProductHistory_CategoryId",
                table: "ProductHistory",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductHistory_ProductId",
                table: "ProductHistory",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductHistory_ProviderId",
                table: "ProductHistory",
                column: "ProviderId");

            migrationBuilder.Sql(
                "CREATE OR REPLACE FUNCTION on_product_change() RETURNS trigger as $product_stamp$" +
                "BEGIN" +
                "    INSERT INTO \"ProductHistory\" (" +
                "        \"BasePrice_Currency\"," +
                "        \"BasePrice_Amount\"," +
                "        \"CategoryId\"," +
                "        \"Code\"," +
                "        \"CreatedAt\"," +
                "        \"Description\"," +
                "        \"ProviderId\"," +
                "        \"ProviderReference\"," +
                "        \"SellingPrice_Currency\"," +
                "        \"SellingPrice_Amount\","+
                "        \"TaxedPrice_Currency\"," +
                "        \"TaxedPrice_Amount\"," +
                "        \"ProductId\"" +
                "    ) VALUES (" +
                "        OLD.\"BasePrice_Currency\"," +
                "        OLD.\"BasePrice_Amount\"," +
                "        OLD.\"CategoryId\"," +
                "        OLD.\"Code\"," +
                "        NOW()," +
                "        OLD.\"Description\"," +
                "        OLD.\"ProviderId\"," +
                "        OLD.\"ProviderReference\"," +
                "        OLD.\"SellingPrice_Currency\"," +
                "        OLD.\"SellingPrice_Amount\"," +
                "        OLD.\"TaxedPrice_Currency\"," +
                "        OLD.\"TaxedPrice_Amount\"," +
                "        OLD.\"Id\");" +
                "    RETURN NEW;" +
                "END;" +
                "$product_stamp$ LANGUAGE plpgsql;" +
                "CREATE TRIGGER product_stamp BEFORE UPDATE ON \"Product\" " +
                "FOR EACH ROW EXECUTE PROCEDURE on_product_change();");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProductHistory");
            migrationBuilder.Sql("DROP FUNCTION on_product_change;");
        }
    }
}
