﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace PriceList.Server.DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class AddProvider : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ProviderId",
                table: "Product",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Provider",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: false),
                    PricesHost = table.Column<string>(type: "text", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Provider", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Product_ProviderId",
                table: "Product",
                column: "ProviderId");

            migrationBuilder.AddForeignKey(
                name: "FK_Product_Provider_ProviderId",
                table: "Product",
                column: "ProviderId",
                principalTable: "Provider",
                principalColumn: "Id");

            migrationBuilder.Sql(
                "INSERT INTO \"Provider\" (\"Id\", \"Name\", \"PricesHost\", \"CreatedAt\", \"UpdatedAt\")" +
                "SELECT \"Id\", \"Description\", \"PricesHost\", \"CreatedAt\", \"UpdatedAt\" " +
                "FROM \"Category\";");

            migrationBuilder.Sql(
                "SELECT setval(pg_get_serial_sequence('\"Provider\"', 'Id'), (SELECT max(\"Id\") FROM \"Provider\"));");

            migrationBuilder.Sql(
                "UPDATE \"Product\" " +
                "SET \"ProviderId\" = \"CategoryId\", \"CategoryId\" = NULL;");
            
            migrationBuilder.DropColumn(
                name: "PricesHost",
                table: "Category");

            migrationBuilder.Sql("DELETE FROM \"Category\";");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Product_Provider_ProviderId",
                table: "Product");

            migrationBuilder.DropTable(
                name: "Provider");

            migrationBuilder.DropIndex(
                name: "IX_Product_ProviderId",
                table: "Product");

            migrationBuilder.DropColumn(
                name: "ProviderId",
                table: "Product");

            migrationBuilder.AddColumn<string>(
                name: "PricesHost",
                table: "Category",
                type: "text",
                nullable: true);
        }
    }
}
