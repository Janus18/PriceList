﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PriceList.Server.DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class AddSynchronizationSeen : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Seen",
                table: "Synchronization",
                type: "boolean",
                nullable: false,
                defaultValue: false);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Seen",
                table: "Synchronization");
        }
    }
}
