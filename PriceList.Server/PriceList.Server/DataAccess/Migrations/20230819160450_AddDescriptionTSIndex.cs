﻿using Microsoft.EntityFrameworkCore.Migrations;
using PriceList.Server.Tools;

#nullable disable

namespace PriceList.Server.DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class AddDescriptionTSIndex : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Product_Description",
                table: "Product",
                column: "Description")
                .Annotation("Npgsql:IndexMethod", "GIN")
                .Annotation("Npgsql:TsVectorConfig", DbConfiguration.TextSearchConfiguration);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Product_Description",
                table: "Product");
        }
    }
}
