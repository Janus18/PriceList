﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PriceList.Server.DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class UpdateSynchronization : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SynchronizationProductLine");

            migrationBuilder.DropColumn(
                name: "Seen",
                table: "Synchronization");

            migrationBuilder.RenameColumn(
                name: "Succeeded",
                table: "Synchronization",
                newName: "Updated");

            migrationBuilder.AddColumn<string>(
                name: "Errors",
                table: "Synchronization",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Total",
                table: "Synchronization",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "UpdatedBySystem",
                table: "ProductHistory",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "UpdatedBySystem",
                table: "Product",
                type: "boolean",
                nullable: false,
                defaultValue: false);
            
            migrationBuilder.Sql("UPDATE \"Synchronization\" SET \"Total\" = \"Updated\", \"Updated\" = 0;");
            
            migrationBuilder.Sql(
                "CREATE OR REPLACE FUNCTION on_product_change() RETURNS trigger as $product_stamp$" +
                "BEGIN" +
                "    INSERT INTO \"ProductHistory\" (" +
                "        \"BasePrice_Currency\"," +
                "        \"BasePrice_Amount\"," +
                "        \"CategoryId\"," +
                "        \"Code\"," +
                "        \"CreatedAt\"," +
                "        \"Description\"," +
                "        \"ProviderId\"," +
                "        \"ProviderReference\"," +
                "        \"SellingPrice_Currency\"," +
                "        \"SellingPrice_Amount\"," +
                "        \"TaxedPrice_Currency\"," +
                "        \"TaxedPrice_Amount\"," +
                "        \"RecommendedSellingPrice_Currency\"," +
                "        \"RecommendedSellingPrice_Amount\"," +
                "        \"UpdatedBySystem\"," +
                "        \"ProductId\"" +
                "    ) VALUES (" +
                "        OLD.\"BasePrice_Currency\"," +
                "        OLD.\"BasePrice_Amount\"," +
                "        OLD.\"CategoryId\"," +
                "        OLD.\"Code\"," +
                "        NOW()," +
                "        OLD.\"Description\"," +
                "        OLD.\"ProviderId\"," +
                "        OLD.\"ProviderReference\"," +
                "        OLD.\"SellingPrice_Currency\"," +
                "        OLD.\"SellingPrice_Amount\"," +
                "        OLD.\"TaxedPrice_Currency\"," +
                "        OLD.\"TaxedPrice_Amount\"," +
                "        OLD.\"RecommendedSellingPrice_Currency\"," +
                "        OLD.\"RecommendedSellingPrice_Amount\"," +
                "        NEW.\"UpdatedBySystem\"," +
                "        OLD.\"Id\");" +
                "    RETURN NEW;" +
                "END;" +
                "$product_stamp$ LANGUAGE plpgsql;"
            );
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
                "CREATE OR REPLACE FUNCTION on_product_change() RETURNS trigger as $product_stamp$" +
                "BEGIN" +
                "    INSERT INTO \"ProductHistory\" (" +
                "        \"BasePrice_Currency\"," +
                "        \"BasePrice_Amount\"," +
                "        \"CategoryId\"," +
                "        \"Code\"," +
                "        \"CreatedAt\"," +
                "        \"Description\"," +
                "        \"ProviderId\"," +
                "        \"ProviderReference\"," +
                "        \"SellingPrice_Currency\"," +
                "        \"SellingPrice_Amount\"," +
                "        \"TaxedPrice_Currency\"," +
                "        \"TaxedPrice_Amount\"," +
                "        \"RecommendedSellingPrice_Currency\"," +
                "        \"RecommendedSellingPrice_Amount\"," +
                "        \"ProductId\"" +
                "    ) VALUES (" +
                "        OLD.\"BasePrice_Currency\"," +
                "        OLD.\"BasePrice_Amount\"," +
                "        OLD.\"CategoryId\"," +
                "        OLD.\"Code\"," +
                "        NOW()," +
                "        OLD.\"Description\"," +
                "        OLD.\"ProviderId\"," +
                "        OLD.\"ProviderReference\"," +
                "        OLD.\"SellingPrice_Currency\"," +
                "        OLD.\"SellingPrice_Amount\"," +
                "        OLD.\"TaxedPrice_Currency\"," +
                "        OLD.\"TaxedPrice_Amount\"," +
                "        OLD.\"RecommendedSellingPrice_Currency\"," +
                "        OLD.\"RecommendedSellingPrice_Amount\"," +
                "        OLD.\"Id\");" +
                "    RETURN NEW;" +
                "END;" +
                "$product_stamp$ LANGUAGE plpgsql;"
            );
            
            migrationBuilder.Sql("UPDATE \"Synchronization\" SET \"Updated\" = \"Total\";");
            
            migrationBuilder.DropColumn(
                name: "Errors",
                table: "Synchronization");

            migrationBuilder.DropColumn(
                name: "Total",
                table: "Synchronization");

            migrationBuilder.DropColumn(
                name: "UpdatedBySystem",
                table: "ProductHistory");

            migrationBuilder.DropColumn(
                name: "UpdatedBySystem",
                table: "Product");

            migrationBuilder.RenameColumn(
                name: "Updated",
                table: "Synchronization",
                newName: "Succeeded");

            migrationBuilder.AddColumn<bool>(
                name: "Seen",
                table: "Synchronization",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "SynchronizationProductLine",
                columns: table => new
                {
                    SynchronizationId = table.Column<int>(type: "integer", nullable: false),
                    ProductId = table.Column<int>(type: "integer", nullable: false),
                    Success = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SynchronizationProductLine", x => new { x.SynchronizationId, x.ProductId });
                    table.ForeignKey(
                        name: "FK_SynchronizationProductLine_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SynchronizationProductLine_Synchronization_SynchronizationId",
                        column: x => x.SynchronizationId,
                        principalTable: "Synchronization",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SynchronizationProductLine_ProductId",
                table: "SynchronizationProductLine",
                column: "ProductId");
        }
    }
}
