﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PriceList.Server.DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class UpdateMoneyAmount : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "TaxedPrice_Amount",
                table: "Product",
                type: "numeric(19,5)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "SellingPrice_Amount",
                table: "Product",
                type: "numeric(19,5)",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<decimal>(
                name: "BasePrice_Amount",
                table: "Product",
                type: "numeric(19,5)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "TaxedPrice_Amount",
                table: "Product",
                type: "integer",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "numeric(19,5)",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "SellingPrice_Amount",
                table: "Product",
                type: "integer",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "numeric(19,5)");

            migrationBuilder.AlterColumn<int>(
                name: "BasePrice_Amount",
                table: "Product",
                type: "integer",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "numeric(19,5)",
                oldNullable: true);
        }
    }
}
