﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PriceList.Server.DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class AddProductRecommendedSellingPrice : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "RecommendedSellingPrice_Amount",
                table: "ProductHistory",
                type: "numeric(19,5)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RecommendedSellingPrice_Currency",
                table: "ProductHistory",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "RecommendedSellingPrice_Amount",
                table: "Product",
                type: "numeric(19,5)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RecommendedSellingPrice_Currency",
                table: "Product",
                type: "text",
                nullable: true);

            migrationBuilder.Sql(
                "CREATE OR REPLACE FUNCTION on_product_change() RETURNS trigger as $product_stamp$" +
                "BEGIN" +
                "    INSERT INTO \"ProductHistory\" (" +
                "        \"BasePrice_Currency\"," +
                "        \"BasePrice_Amount\"," +
                "        \"CategoryId\"," +
                "        \"Code\"," +
                "        \"CreatedAt\"," +
                "        \"Description\"," +
                "        \"ProviderId\"," +
                "        \"ProviderReference\"," +
                "        \"SellingPrice_Currency\"," +
                "        \"SellingPrice_Amount\"," +
                "        \"TaxedPrice_Currency\"," +
                "        \"TaxedPrice_Amount\"," +
                "        \"RecommendedSellingPrice_Currency\"," +
                "        \"RecommendedSellingPrice_Amount\"," +
                "        \"ProductId\"" +
                "    ) VALUES (" +
                "        OLD.\"BasePrice_Currency\"," +
                "        OLD.\"BasePrice_Amount\"," +
                "        OLD.\"CategoryId\"," +
                "        OLD.\"Code\"," +
                "        NOW()," +
                "        OLD.\"Description\"," +
                "        OLD.\"ProviderId\"," +
                "        OLD.\"ProviderReference\"," +
                "        OLD.\"SellingPrice_Currency\"," +
                "        OLD.\"SellingPrice_Amount\"," +
                "        OLD.\"TaxedPrice_Currency\"," +
                "        OLD.\"TaxedPrice_Amount\"," +
                "        OLD.\"RecommendedSellingPrice_Currency\"," +
                "        OLD.\"RecommendedSellingPrice_Amount\"," +
                "        OLD.\"Id\");" +
                "    RETURN NEW;" +
                "END;" +
                "$product_stamp$ LANGUAGE plpgsql;"
            );
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
                "CREATE OR REPLACE FUNCTION on_product_change() RETURNS trigger as $product_stamp$" +
                "BEGIN" +
                "    INSERT INTO \"ProductHistory\" (" +
                "        \"BasePrice_Currency\"," +
                "        \"BasePrice_Amount\"," +
                "        \"CategoryId\"," +
                "        \"Code\"," +
                "        \"CreatedAt\"," +
                "        \"Description\"," +
                "        \"ProviderId\"," +
                "        \"ProviderReference\"," +
                "        \"SellingPrice_Currency\"," +
                "        \"SellingPrice_Amount\"," +
                "        \"TaxedPrice_Currency\"," +
                "        \"TaxedPrice_Amount\"," +
                "        \"ProductId\"" +
                "    ) VALUES (" +
                "        OLD.\"BasePrice_Currency\"," +
                "        OLD.\"BasePrice_Amount\"," +
                "        OLD.\"CategoryId\"," +
                "        OLD.\"Code\"," +
                "        NOW()," +
                "        OLD.\"Description\"," +
                "        OLD.\"ProviderId\"," +
                "        OLD.\"ProviderReference\"," +
                "        OLD.\"SellingPrice_Currency\"," +
                "        OLD.\"SellingPrice_Amount\"," +
                "        OLD.\"TaxedPrice_Currency\"," +
                "        OLD.\"TaxedPrice_Amount\"," +
                "        OLD.\"Id\");" +
                "    RETURN NEW;" +
                "END;" +
                "$product_stamp$ LANGUAGE plpgsql;"
            );
            
            migrationBuilder.DropColumn(
                name: "RecommendedSellingPrice_Amount",
                table: "ProductHistory");

            migrationBuilder.DropColumn(
                name: "RecommendedSellingPrice_Currency",
                table: "ProductHistory");

            migrationBuilder.DropColumn(
                name: "RecommendedSellingPrice_Amount",
                table: "Product");

            migrationBuilder.DropColumn(
                name: "RecommendedSellingPrice_Currency",
                table: "Product");
        }
    }
}
