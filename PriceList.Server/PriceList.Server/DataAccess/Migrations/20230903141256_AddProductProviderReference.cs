﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PriceList.Server.DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class AddProductProviderReference : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ProviderReference",
                table: "Product",
                type: "text",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ProviderReference",
                table: "Product");
        }
    }
}
