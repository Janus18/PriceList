using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using PriceList.Server.Domain;

namespace PriceList.Server.DataAccess.Configurations;

public class ConfigurationConfiguration : IEntityTypeConfiguration<Configuration>
{
    public void Configure(EntityTypeBuilder<Configuration> builder)
    {
        builder.HasKey(c => c.Key);
        builder.Property(c => c.Key).HasConversion<EnumToStringConverter<ConfigurationKey>>();
        builder.Property(c => c.Value).HasMaxLength(500);
    }
}