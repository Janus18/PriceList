using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PriceList.Server.Domain;
using PriceList.Server.Tools;

namespace PriceList.Server.DataAccess.Configurations;

public class ProductConfiguration : IEntityTypeConfiguration<Product>
{
    public void Configure(EntityTypeBuilder<Product> builder)
    {
        builder.HasKey(p => p.Id);
        builder.Property(p => p.Id).UseIdentityAlwaysColumn();
        builder.Property(p => p.Code).IsRequired().HasMaxLength(100);
        builder.Property(p => p.Description).IsRequired().HasMaxLength(500);
        builder.Property(p => p.ProviderReference).HasMaxLength(1000);
        builder.Property(p => p.Disabled).IsRequired();
        builder.OwnsOne(
            p => p.BasePrice,
            MoneyConfiguration.Get<Product>());
        builder.OwnsOne(
            p => p.SellingPrice,
            MoneyConfiguration.Get<Product>());
        builder.OwnsOne(
            p => p.TaxedPrice,
            MoneyConfiguration.Get<Product>());
        builder.OwnsOne(
            p => p.RecommendedSellingPrice,
            MoneyConfiguration.Get<Product>());
        builder.HasOne(p => p.Category).WithMany().OnDelete(DeleteBehavior.Restrict);
        builder.HasIndex(p => p.Code).IsUnique();
        builder.HasIndex(p => p.Description)
            .HasMethod("GIN")
            .IsTsVectorExpressionIndex(DbConfiguration.TextSearchConfiguration);
        builder.HasIndex(p => p.UpdatedAt).IsDescending();
    }

    
}