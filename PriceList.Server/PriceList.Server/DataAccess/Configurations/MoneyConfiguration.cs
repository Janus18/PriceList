using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using PriceList.Server.Domain;

namespace PriceList.Server.DataAccess.Configurations;

public static class MoneyConfiguration
{
    public static Action<OwnedNavigationBuilder<T, Money>> Get<T>() where T : class
    {
        return x =>
        {
            x.Property(y => y.Currency).HasConversion(new EnumToStringConverter<Currency>());
            x.Property(y => y.Amount).IsRequired().HasColumnType("numeric(19,5)");
        };
    }
}