using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PriceList.Server.Domain;

namespace PriceList.Server.DataAccess.Configurations;

public class ProductHistoryConfiguration : IEntityTypeConfiguration<ProductHistory>
{
    public void Configure(EntityTypeBuilder<ProductHistory> builder)
    {
        builder.HasKey(p => p.Id);
        builder.Property(p => p.Id).UseIdentityAlwaysColumn();
        builder.Property(p => p.Code).IsRequired().HasMaxLength(500);
        builder.Property(p => p.Description).IsRequired();
        builder.OwnsOne(
            p => p.BasePrice,
            MoneyConfiguration.Get<ProductHistory>());
        builder.OwnsOne(
            p => p.SellingPrice,
            MoneyConfiguration.Get<ProductHistory>());
        builder.OwnsOne(
            p => p.TaxedPrice,
            MoneyConfiguration.Get<ProductHistory>());
        builder.OwnsOne(
            p => p.RecommendedSellingPrice,
            MoneyConfiguration.Get<ProductHistory>());
        builder.HasOne(p => p.Category).WithMany().OnDelete(DeleteBehavior.Restrict);
        builder.HasOne(p => p.Product)
            .WithMany()
            .HasForeignKey(p => p.ProductId)
            .IsRequired()
            .OnDelete(DeleteBehavior.Cascade);
    }
}