using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PriceList.Server.Domain;

namespace PriceList.Server.DataAccess.Configurations;

public class ProviderConfiguration : IEntityTypeConfiguration<Provider>
{
    public void Configure(EntityTypeBuilder<Provider> builder)
    {
        builder.HasKey(p => p.Id);
        builder.Property(p => p.ProfitMargin)
            .HasConversion(p => p == null ? (decimal?)null : p.Value, p => p.HasValue ? new Percentage(p.Value) : null);
        builder.Property(p => p.Taxes)
            .HasConversion(p => p == null ? (decimal?)null : p.Value, p => p.HasValue ? new Percentage(p.Value) : null);
    }
}