using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PriceList.Server.Domain.Synchronizations;

namespace PriceList.Server.DataAccess.Configurations;

public class SynchronizationConfiguration : IEntityTypeConfiguration<Synchronization>
{
    public void Configure(EntityTypeBuilder<Synchronization> builder)
    {
        builder.HasKey(s => s.Id);
        builder.HasIndex(s => s.CreatedAt);
        builder.Property(s => s.Errors)
            .IsRequired(false)
            .HasConversion<string?>(
                xs => xs.ToString(),
                s => new SyncMessages(s ?? ""));
    }
}