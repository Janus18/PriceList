using Microsoft.EntityFrameworkCore;
using PriceList.Server.Domain;

namespace PriceList.Server.DataAccess.Queries.FindConfigurationByKey;

public class FindConfigurationByKey(AppDbContext appDbContext) : IFindConfigurationByKey
{
    public Task<Configuration?> Find(ConfigurationKey key, CancellationToken cancellationToken)
    {
        return appDbContext.Set<Configuration>()
            .Where(c => c.Key == key)
            .FirstOrDefaultAsync(cancellationToken);
    }
}