using PriceList.Server.Domain;

namespace PriceList.Server.DataAccess.Queries.FindConfigurationByKey;

public interface IFindConfigurationByKey
{
    Task<Configuration?> Find(ConfigurationKey key, CancellationToken cancellationToken);
}