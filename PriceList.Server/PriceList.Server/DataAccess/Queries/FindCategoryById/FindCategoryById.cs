using Microsoft.EntityFrameworkCore;
using PriceList.Server.Domain;

namespace PriceList.Server.DataAccess.Queries.FindCategoryById;

public class FindCategoryById : IFindCategoryById
{
    private readonly AppDbContext _context;

    public FindCategoryById(AppDbContext context)
    {
        _context = context;
    }
    
    public async Task<Category?> Execute(int id, CancellationToken cancellationToken)
    {
        return await _context.Set<Category>().AsQueryable()
            .FirstOrDefaultAsync(c => c.Id == id, cancellationToken);
    }
}