using PriceList.Server.Domain;

namespace PriceList.Server.DataAccess.Queries.FindCategoryById;

public interface IFindCategoryById
{
    Task<Category?> Execute(int id, CancellationToken cancellationToken);
}