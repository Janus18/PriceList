using Microsoft.EntityFrameworkCore;
using PriceList.Server.Domain;
using PriceList.Server.Domain.Configurations;
using PriceList.Server.Domain.Interfaces;

namespace PriceList.Server.DataAccess.Queries.GetSynchronizationConfigurations;

public class GetSynchronizationConfigurations(AppDbContext dbContext) : IGetSynchronizationConfigurations
{
    public async Task<SynchronizationConfiguration> Get(CancellationToken cancellationToken)
    {
        var configs = await GetConfigurations(cancellationToken);
        return new SynchronizationConfiguration(configs);
    }

    public async Task<IList<Configuration>> GetConfigurations(CancellationToken cancellationToken)
    {
        IQueryable<Configuration> queryable = dbContext.Set<Configuration>();
        var configurations = await queryable
            .Where(c => 
                c.Key == SynchronizationConfiguration.CronKey
                || c.Key == SynchronizationConfiguration.EnabledKey
                || c.Key == SynchronizationConfiguration.TimeZoneKey)
            .ToListAsync(cancellationToken);
        return configurations;
    }
}