using Microsoft.EntityFrameworkCore;
using PriceList.Server.Domain;

namespace PriceList.Server.DataAccess.Queries.FindProviderById;

public class FindProviderById : IFindProviderById
{
    private readonly AppDbContext _dbContext;

    public FindProviderById(AppDbContext dbContext)
    {
        _dbContext = dbContext;
    }
    
    public async Task<Provider?> Find(int providerId, CancellationToken cancellationToken)
    {
        return await _dbContext.Set<Provider>().AsQueryable()
            .FirstOrDefaultAsync(p => p.Id == providerId, cancellationToken);
    }
}