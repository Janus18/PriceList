using PriceList.Server.Domain;

namespace PriceList.Server.DataAccess.Queries.FindProviderById;

public interface IFindProviderById
{
    Task<Provider?> Find(int providerId, CancellationToken cancellationToken);
}