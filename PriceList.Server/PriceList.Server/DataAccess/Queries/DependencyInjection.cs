using PriceList.Server.DataAccess.Queries.FindCategoryById;
using PriceList.Server.DataAccess.Queries.FindConfigurationByKey;
using PriceList.Server.DataAccess.Queries.FindProviderById;
using PriceList.Server.Domain.Interfaces;

namespace PriceList.Server.DataAccess.Queries;

public static class DependencyInjection
{
    public static IServiceCollection AddQueries(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddScoped<IFindCategoryById, FindCategoryById.FindCategoryById>();
        serviceCollection.AddScoped<IFindProviderById, FindProviderById.FindProviderById>();
        serviceCollection.AddScoped<IFindConfigurationByKey, FindConfigurationByKey.FindConfigurationByKey>();
        serviceCollection.AddScoped<IGetSynchronizationConfigurations, GetSynchronizationConfigurations.GetSynchronizationConfigurations>();
        return serviceCollection;
    }
}