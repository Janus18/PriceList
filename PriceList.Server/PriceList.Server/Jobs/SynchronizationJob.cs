using MediatR;
using Microsoft.EntityFrameworkCore;
using PriceList.Server.DataAccess;
using PriceList.Server.Domain.Synchronizations;
using PriceList.Server.Handlers.Sync;

namespace PriceList.Server.Jobs;

public class SynchronizationJob
{
    private readonly IMediator _mediator;
    private readonly AppDbContext _dbContext;

    public SynchronizationJob(IMediator mediator, AppDbContext dbContext)
    {
        _mediator = mediator;
        _dbContext = dbContext;
    }
    
    public async Task SynchronizeProducts(Guid token, CancellationToken cancellationToken)
    {
        var synchronization = await GetSynchronization(token, cancellationToken);
        await _mediator.Send(new SyncRequest(synchronization), cancellationToken);
    }

    private async Task<Synchronization> GetSynchronization(Guid token, CancellationToken cancellationToken)
    {
        var synchronization = await _dbContext.Set<Synchronization>()
            .FirstOrDefaultAsync(s => s.Token == token, cancellationToken);
        if (synchronization != null)
            return synchronization;
        synchronization = new Synchronization(token);
        _dbContext.Set<Synchronization>().Add(synchronization);
        await _dbContext.SaveChangesAsync(cancellationToken);
        return synchronization;
    }
}