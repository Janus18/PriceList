using Hangfire;
using PriceList.Server.Domain.Configurations;
using PriceList.Server.Domain.Interfaces;

namespace PriceList.Server.Jobs;

public class HangfireJobs(
    IBackgroundJobClient backgroundJobClient,
    IGetSynchronizationConfigurations getSynchronizationConfigurations)
{
    private const string SynchronizationJobKey = "Synchronization";
    
    public async Task AddOrUpdateSynchronization(SynchronizationConfiguration? config = null)
    {
        config ??= await getSynchronizationConfigurations.Get(CancellationToken.None);
        if (config.Enabled)
            RecurringJob.AddOrUpdate<HangfireJobs>(
                SynchronizationJobKey,
                x => x.AddSynchronizationJob(),
                config.Cron,
                new RecurringJobOptions { TimeZone = config.TimeZone });
        else
            RecurringJob.RemoveIfExists(SynchronizationJobKey);
    }

    // ReSharper disable once MemberCanBePrivate.Global
    // Method needs to be public so Hangfire can use it
    public void AddSynchronizationJob()
    {
        backgroundJobClient.Enqueue<SynchronizationJob>(
            j => j.SynchronizeProducts(Guid.NewGuid(), default));
    }
}