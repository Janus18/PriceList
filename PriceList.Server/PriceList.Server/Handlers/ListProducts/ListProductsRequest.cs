using MediatR;
using PriceList.Server.DTOs.GetProduct;

namespace PriceList.Server.Handlers.ListProducts;

public record ListProductsRequest(
    DateTime UpdatedAt,
    ushort Size,
    int? CategoryId,
    string? Description,
    string? Code,
    int? ProviderId,
    int? SynchronizationId,
    bool DisabledToo = false
) : IRequest<IList<GetProductResultDto>>;
