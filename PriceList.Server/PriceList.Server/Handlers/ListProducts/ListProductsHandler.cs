using MediatR;
using Microsoft.EntityFrameworkCore;
using PriceList.Server.DataAccess;
using PriceList.Server.Domain;
using PriceList.Server.Domain.Synchronizations;
using PriceList.Server.DTOs.GetProduct;
using PriceList.Server.Tools;

namespace PriceList.Server.Handlers.ListProducts;

public class ListProductsHandler : IRequestHandler<ListProductsRequest, IList<GetProductResultDto>>
{
    private readonly AppDbContext _dbContext;

    public ListProductsHandler(
        AppDbContext dbContext
    )
    {
        _dbContext = dbContext;
    }
    
    public async Task<IList<GetProductResultDto>> Handle(ListProductsRequest request, CancellationToken cancellationToken)
    {
        var query = _dbContext.Set<Product>().AsQueryable();

        if (!string.IsNullOrEmpty(request.Code))
        {
            query = query.Where(p => p.Code == request.Code);
        }
        else
        {
            var synchronization = await GetSynchronization(request.SynchronizationId);
            
            if (synchronization != null)
                query = query.Where(p => _dbContext.Set<ProductHistory>().Any(ph =>
                    ph.ProductId == p.Id &&
                    ph.UpdatedBySystem &&
                    synchronization.CreatedAt <= ph.CreatedAt &&
                    ph.CreatedAt <= synchronization.FinishedAt));
            
            if (!request.DisabledToo)
                query = query.Where(p => !p.Disabled);
            
            if (request.CategoryId.HasValue)
                query = query.Where(p => p.Category!.Id == request.CategoryId.Value);

            if (request.ProviderId.HasValue)
                query = query.Where(p => p.Provider!.Id == request.ProviderId.Value);

            if (!string.IsNullOrEmpty(request.Description))
            {
                query = query.OrderByDescending(p =>
                    EF.Functions
                        .ToTsVector(DbConfiguration.TextSearchConfiguration, p.Description)
                        .Rank(EF.Functions.PlainToTsQuery(DbConfiguration.TextSearchConfiguration,
                            request.Description)));   
            }
            else
            {
                query = query
                    .OrderByDescending(p => p.UpdatedAt)
                    .Where(p => p.UpdatedAt < request.UpdatedAt);
            }
        }
        
        var result = await query
            .Include(p => p.Category)
            .Include(p => p.Provider)
            .Take(request.Size)
            .ToListAsync(cancellationToken: cancellationToken);

        return result.Select(p => new GetProductResultDto(p)).ToList();
    }

    private async Task<Synchronization?> GetSynchronization(int? synchronizationId)
    {
        if (!synchronizationId.HasValue)
            return null;
        return await _dbContext.Set<Synchronization>().FirstOrDefaultAsync(s => s.Id == synchronizationId);   
    }
}