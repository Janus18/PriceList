using MediatR;
using Microsoft.EntityFrameworkCore;
using PriceList.Server.DataAccess;
using PriceList.Server.Domain;
using PriceList.Server.Tools;
using PriceList.Server.Tools.Exceptions;

namespace PriceList.Server.Handlers.UpdateProvider;

public class UpdateProviderHandler : IRequestHandler<UpdateProviderRequest>
{
    private readonly AppDbContext _dbContext;

    public UpdateProviderHandler(AppDbContext dbContext)
    {
        _dbContext = dbContext;
    }
    
    public async Task Handle(UpdateProviderRequest request, CancellationToken cancellationToken)
    {
        var provider = await _dbContext.Set<Provider>().AsQueryable()
            .FirstOrDefaultAsync(p => p.Id == request.Request.Id, cancellationToken);
        if (provider is null)
            throw new ValidationException(
                ErrorCodes.ProviderNotFound,
                $"No provider found with ID {request.Request.Id}");
        request.Request.UpdateProvider(provider);
        var result = await _dbContext.SaveChangesAsync(cancellationToken);
        Assert.GreaterThanOrEqualTo(result, 0);
    }
}