using MediatR;
using PriceList.Server.DTOs.UpdateProvider;

namespace PriceList.Server.Handlers.UpdateProvider;

public record UpdateProviderRequest(UpdateProviderRequestDto Request) : IRequest;