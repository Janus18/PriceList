using MediatR;
using PriceList.Server.DTOs.ListSynchronizations;

namespace PriceList.Server.Handlers.ListSynchronizations;

public record ListSynchronizationsRequest(
    DateTime Until,
    ushort MaxSize
) : IRequest<IList<ListSynchronizationsResultDto>>;
