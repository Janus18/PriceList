using MediatR;
using Microsoft.EntityFrameworkCore;
using PriceList.Server.DataAccess;
using PriceList.Server.Domain.Synchronizations;
using PriceList.Server.DTOs.ListSynchronizations;

namespace PriceList.Server.Handlers.ListSynchronizations;

public class ListSynchronizationsHandler : IRequestHandler<ListSynchronizationsRequest, IList<ListSynchronizationsResultDto>>
{
    private readonly AppDbContext _dbContext;

    public ListSynchronizationsHandler(AppDbContext dbContext)
    {
        _dbContext = dbContext;
    }
    
    public async Task<IList<ListSynchronizationsResultDto>> Handle(ListSynchronizationsRequest request, CancellationToken cancellationToken)
    {
        var result = await  _dbContext.Set<Synchronization>()
            .OrderByDescending(s => s.CreatedAt)
            .Where(s => s.CreatedAt <= request.Until)
            .Take(request.MaxSize)
            .ToListAsync(cancellationToken);
        return result.Select(s => new ListSynchronizationsResultDto(s)).ToList();
    }
}