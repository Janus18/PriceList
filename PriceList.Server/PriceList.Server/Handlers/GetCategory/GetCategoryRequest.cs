using MediatR;
using PriceList.Server.Domain;
using PriceList.Server.DTOs.GetCategory;

namespace PriceList.Server.Handlers.GetCategory;

public class GetCategoryRequest : IRequest<GetCategoryResultDto?>
{
    public int CategoryId { get; }

    public GetCategoryRequest(int categoryId)
    {
        CategoryId = categoryId;
    }
}