using MediatR;
using PriceList.Server.DataAccess;
using PriceList.Server.Domain;
using PriceList.Server.DTOs.GetCategory;

namespace PriceList.Server.Handlers.GetCategory;

public class GetCategoryHandler : IRequestHandler<GetCategoryRequest, GetCategoryResultDto?>
{
    private readonly AppDbContext _dbAppDbContext;

    public GetCategoryHandler(
        AppDbContext dbAppDbContext
    )
    {
        _dbAppDbContext = dbAppDbContext;
    }
    
    public async Task<GetCategoryResultDto?> Handle(GetCategoryRequest request, CancellationToken cancellationToken)
    {
        var result = await _dbAppDbContext.Set<Category>().FindAsync(
            new object[] { request.CategoryId },
            cancellationToken: cancellationToken);
        return result is null ? null : new GetCategoryResultDto(result);
    }
}