using MediatR;
using PriceList.Server.DTOs.AddCategory;

namespace PriceList.Server.Handlers.AddCategory;

public class AddCategoryRequest : IRequest<AddCategoryResultDto>
{
    public AddCategoryRequestDto Dto { get; }

    public AddCategoryRequest(AddCategoryRequestDto dto)
    {
        Dto = dto;
    }
}