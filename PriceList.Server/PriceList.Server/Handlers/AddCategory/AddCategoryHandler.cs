using MediatR;
using PriceList.Server.DataAccess;
using PriceList.Server.Domain;
using PriceList.Server.DTOs.AddCategory;
using PriceList.Server.Tools;

namespace PriceList.Server.Handlers.AddCategory;

public class AddCategoryHandler : IRequestHandler<AddCategoryRequest, AddCategoryResultDto>
{
    private readonly AppDbContext _dbAppDbContext;

    public AddCategoryHandler(AppDbContext dbAppDbContext)
    {
        _dbAppDbContext = dbAppDbContext;
    }
    
    public async Task<AddCategoryResultDto> Handle(AddCategoryRequest request, CancellationToken cancellationToken)
    {
        var category = request.Dto.ToCategory();
        _dbAppDbContext.Set<Category>().Add(category);
        var result = await _dbAppDbContext.SaveChangesAsync(cancellationToken);
        Assert.GreaterThanOrEqualTo(result, 1);
        return new AddCategoryResultDto(category.Id);
    }
}