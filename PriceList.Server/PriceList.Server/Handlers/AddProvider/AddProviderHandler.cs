using MediatR;
using PriceList.Server.DataAccess;
using PriceList.Server.Domain;
using PriceList.Server.DTOs.AddProvider;
using PriceList.Server.Tools;

namespace PriceList.Server.Handlers.AddProvider;

public class AddProviderHandler : IRequestHandler<AddProviderRequest, AddProviderResultDto>
{
    private readonly AppDbContext _dbContext;

    public AddProviderHandler(AppDbContext dbContext)
    {
        _dbContext = dbContext;
    }
    
    public async Task<AddProviderResultDto> Handle(AddProviderRequest request, CancellationToken cancellationToken)
    {
        var provider = request.Request.GetProvider();
        _dbContext.Set<Provider>().Add(provider);
        var result = await _dbContext.SaveChangesAsync(cancellationToken);
        Assert.GreaterThanOrEqualTo(result, 1);
        return new AddProviderResultDto(provider.Id);
    }
}