using MediatR;
using PriceList.Server.DTOs.AddProvider;

namespace PriceList.Server.Handlers.AddProvider;

public record AddProviderRequest(AddProviderRequestDto Request) : IRequest<AddProviderResultDto>;
