using MediatR;
using PriceList.Server.DTOs.GetCategory;

namespace PriceList.Server.Handlers.ListCategories;

public record ListCategoriesRequest : IRequest<IList<GetCategoryResultDto>>;
