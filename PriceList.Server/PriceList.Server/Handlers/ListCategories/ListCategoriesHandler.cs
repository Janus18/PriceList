using MediatR;
using Microsoft.EntityFrameworkCore;
using PriceList.Server.DataAccess;
using PriceList.Server.Domain;
using PriceList.Server.DTOs.GetCategory;

namespace PriceList.Server.Handlers.ListCategories;

public class ListCategoriesHandler : IRequestHandler<ListCategoriesRequest, IList<GetCategoryResultDto>>
{
    private readonly AppDbContext _dbContext;

    public ListCategoriesHandler(
        AppDbContext dbContext
    )
    {
        _dbContext = dbContext;
    }
    
    public async Task<IList<GetCategoryResultDto>> Handle(ListCategoriesRequest request, CancellationToken cancellationToken)
    {
        var categories = await _dbContext.Set<Category>()
            .AsQueryable()
            .OrderBy(c => c.Description)
            .ToListAsync(cancellationToken);
        return categories.Select(c => new GetCategoryResultDto(c)).ToList();
    }
}