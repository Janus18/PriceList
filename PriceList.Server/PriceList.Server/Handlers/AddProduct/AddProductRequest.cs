using MediatR;
using PriceList.Server.DTOs.AddProduct;

namespace PriceList.Server.Handlers.AddProduct;

public class AddProductRequest : IRequest<AddProductResultDto>
{
    public AddProductRequestDto Dto { get; }

    public AddProductRequest(AddProductRequestDto dto)
    {
        Dto = dto;
    }
}