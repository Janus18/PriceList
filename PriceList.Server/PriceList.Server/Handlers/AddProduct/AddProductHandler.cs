using MediatR;
using Microsoft.EntityFrameworkCore;
using PriceList.Server.DataAccess;
using PriceList.Server.DataAccess.Queries.FindCategoryById;
using PriceList.Server.DataAccess.Queries.FindProviderById;
using PriceList.Server.Domain;
using PriceList.Server.DTOs.AddProduct;
using PriceList.Server.Tools;
using PriceList.Server.Tools.Exceptions;

namespace PriceList.Server.Handlers.AddProduct;

public class AddProductHandler : IRequestHandler<AddProductRequest, AddProductResultDto>
{
    private readonly AppDbContext _dbContext;
    private readonly IFindCategoryById _findCategoryById;
    private readonly IFindProviderById _findProviderById;

    public AddProductHandler(
        AppDbContext dbContext,
        IFindCategoryById findCategoryById,
        IFindProviderById findProviderById
    )
    {
        _dbContext = dbContext;
        _findCategoryById = findCategoryById;
        _findProviderById = findProviderById;
    }
    
    public async Task<AddProductResultDto> Handle(AddProductRequest request, CancellationToken cancellationToken)
    {
        var product = await request.Dto.GetProduct(_findCategoryById, _findProviderById, cancellationToken);
        _dbContext.Set<Product>().Add(product);
        try
        {
            var result = await _dbContext.SaveChangesAsync(cancellationToken);
            Assert.GreaterThanOrEqualTo(result, 1);
            return new AddProductResultDto(product.Id);
        }
        catch (DbUpdateException dbUpdateException) when (dbUpdateException.IsUniqueConstraintViolation())
        {
            throw new ValidationException(
                ErrorCodes.ProductCodeExists,
                nameof(AddProductRequest.Dto.Code),
                $"Product code {request.Dto.Code} already exists");
        }
    }
}