using MediatR;
using PriceList.Server.DTOs.UpdateProductsBatch;

namespace PriceList.Server.Handlers.UpdateProductsBatch;

public class UpdateProductsBatchRequest : IRequest
{
    public UpdateProductsBatchDto Dto { get; }

    public UpdateProductsBatchRequest(UpdateProductsBatchDto dto)
    {
        Dto = dto;
    }
}