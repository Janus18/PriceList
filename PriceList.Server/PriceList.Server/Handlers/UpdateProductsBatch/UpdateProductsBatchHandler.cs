using System.Linq.Expressions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using PriceList.Server.DataAccess;
using PriceList.Server.Domain;
using PriceList.Server.Tools;

namespace PriceList.Server.Handlers.UpdateProductsBatch;

public class UpdateProductsBatchHandler : IRequestHandler<UpdateProductsBatchRequest>
{
    private readonly AppDbContext _dbContext;

    public UpdateProductsBatchHandler(
        AppDbContext dbContext
    )
    {
        _dbContext = dbContext;
    }
    
    public async Task Handle(UpdateProductsBatchRequest request, CancellationToken cancellationToken)
    {
        var queryable = _dbContext.Set<Product>().Where(p => request.Dto.ProductsIds.Contains(p.Id));

        Expression<Func<SetPropertyCalls<Product>, SetPropertyCalls<Product>>> setters = st => st
            .SetProperty(p => p.UpdatedAt, DateTime.UtcNow)
            .SetProperty(p => p.UpdatedBySystem, false);
        
        if (request.Dto.BasePriceIncrement.HasValue)
            setters = setters.AppendSetProperty(st => st.SetProperty(
                p => p.BasePrice!.Amount,
                p => p.BasePrice!.Amount * request.Dto.BasePriceIncrement.Value
            ));
        
        if (request.Dto.SellingPriceIncrement.HasValue)
            setters = setters.AppendSetProperty(st => st.SetProperty(
                p => p.SellingPrice!.Amount,
                p => p.SellingPrice!.Amount * request.Dto.SellingPriceIncrement
            ));

        if (request.Dto.TaxedPriceIncrement.HasValue)
            setters = setters.AppendSetProperty(st => st.SetProperty(
                p => p.TaxedPrice!.Amount,
                p => p.TaxedPrice!.Amount * request.Dto.TaxedPriceIncrement.Value
            ));
        
        if (request.Dto.Enabled.HasValue)
            setters = setters.AppendSetProperty(st => st.SetProperty(
                p => p.Disabled,
                !request.Dto.Enabled.Value
            ));

        if (request.Dto.CategoryId.HasValue)
            setters = setters.AppendSetProperty(st => st.SetProperty(
                p => p.CategoryId,
                request.Dto.CategoryId.Value
            ));
        
        if (request.Dto.ProviderId.HasValue)
            setters = setters.AppendSetProperty(st => st.SetProperty(
                p => p.ProviderId,
                request.Dto.ProviderId.Value
            ));
        
        await queryable.ExecuteUpdateAsync(setters, cancellationToken);
    }
}