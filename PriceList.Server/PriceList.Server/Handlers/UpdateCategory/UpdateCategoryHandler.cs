using MediatR;
using Microsoft.EntityFrameworkCore;
using PriceList.Server.DataAccess;
using PriceList.Server.Domain;
using PriceList.Server.Tools;
using PriceList.Server.Tools.Exceptions;

namespace PriceList.Server.Handlers.UpdateCategory;

public class UpdateCategoryHandler : IRequestHandler<UpdateCategoryRequest>
{
    private readonly AppDbContext _context;

    public UpdateCategoryHandler(
        AppDbContext context
    )
    {
        _context = context;
    }
    
    public async Task Handle(UpdateCategoryRequest request, CancellationToken cancellationToken)
    {
        var category = await _context.Set<Category>()
            .FirstOrDefaultAsync(c => c.Id == request.Dto.Id, cancellationToken);
        if (category == null)
            throw new ValidationException(
                ErrorCodes.CategoryNotFound,
                nameof(request.Dto.Id),
                $"No category with ID {request.Dto.Id} found");
        request.Dto.UpdateCategory(category);
        var result = await _context.SaveChangesAsync(cancellationToken);
        Assert.GreaterThanOrEqualTo(result, 0);
    }
}