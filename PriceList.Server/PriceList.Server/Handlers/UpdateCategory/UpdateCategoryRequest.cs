using MediatR;
using PriceList.Server.DTOs.UpdateCategory;

namespace PriceList.Server.Handlers.UpdateCategory;

public class UpdateCategoryRequest : IRequest
{
    public UpdateCategoryRequestDto Dto { get; }

    public UpdateCategoryRequest(UpdateCategoryRequestDto dto)
    {
        Dto = dto;
    }
}