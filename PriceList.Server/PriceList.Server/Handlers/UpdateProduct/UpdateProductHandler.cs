using MediatR;
using Microsoft.EntityFrameworkCore;
using PriceList.Server.DataAccess;
using PriceList.Server.DataAccess.Queries.FindCategoryById;
using PriceList.Server.DataAccess.Queries.FindProviderById;
using PriceList.Server.Domain;
using PriceList.Server.Tools;
using PriceList.Server.Tools.Exceptions;

namespace PriceList.Server.Handlers.UpdateProduct;

public class UpdateProductHandler : IRequestHandler<UpdateProductRequest>
{
    private readonly AppDbContext _context;
    private readonly IFindCategoryById _findCategoryById;
    private readonly IFindProviderById _findProviderById;

    public UpdateProductHandler(
        AppDbContext context,
        IFindCategoryById findCategoryById,
        IFindProviderById findProviderById
    )
    {
        _context = context;
        _findCategoryById = findCategoryById;
        _findProviderById = findProviderById;
    }
    
    public async Task Handle(UpdateProductRequest request, CancellationToken cancellationToken)
    {
        var product = await _context.Set<Product>()
            .AsQueryable()
            .Include(p => p.Category)
            .FirstOrDefaultAsync(p => p.Id == request.Dto.Id, cancellationToken);
        if (product == null)
            throw new ValidationException(
                ErrorCodes.ProductNotFound,
                nameof(UpdateProductRequest.Dto.Id),
                $"No product with ID {request.Dto.Id} found");
        await request.Dto.UpdateProduct(product, _findCategoryById, _findProviderById, cancellationToken);
        try
        {
            var result = await _context.SaveChangesAsync(cancellationToken);
            Assert.GreaterThanOrEqualTo(result, 0);
        }
        catch (DbUpdateException dbUpdateException) when (dbUpdateException.IsUniqueConstraintViolation())
        {
            throw new ValidationException(
                ErrorCodes.ProductCodeExists,
                nameof(UpdateProductRequest.Dto.Code),
                $"Product code {request.Dto.Code} already exists");
        }
    }
}