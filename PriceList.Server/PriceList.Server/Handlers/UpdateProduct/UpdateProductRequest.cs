using MediatR;
using PriceList.Server.DTOs.UpdateProduct;

namespace PriceList.Server.Handlers.UpdateProduct;

public class UpdateProductRequest : IRequest
{
    public UpdateProductRequestDto Dto { get; }

    public UpdateProductRequest(UpdateProductRequestDto dto)
    {
        Dto = dto;
    }
}