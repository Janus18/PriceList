using MediatR;
using Microsoft.EntityFrameworkCore;
using PriceList.Server.DataAccess;
using PriceList.Server.Domain;
using PriceList.Server.Tools.Exceptions;

namespace PriceList.Server.Handlers.UpdateProductEnabled;

public class UpdateProductEnabledHandler : IRequestHandler<UpdateProductEnabledRequest>
{
    private readonly AppDbContext _dbContext;

    public UpdateProductEnabledHandler(
        AppDbContext dbContext
    )
    {
        _dbContext = dbContext;
    }
    
    public async Task Handle(UpdateProductEnabledRequest request, CancellationToken cancellationToken)
    {
        var product = await _dbContext
            .Set<Product>()
            .AsQueryable()
            .FirstOrDefaultAsync(p => p.Id == request.ProductId, cancellationToken);
        if (product is null)
            throw new ValidationException(
                ErrorCodes.ProductNotFound,
                $"No product found with ID {request.ProductId}");
        if (request.Enabled)
            product.Enable();
        else
            product.Disable();
        await _dbContext.SaveChangesAsync(cancellationToken);
    }
}