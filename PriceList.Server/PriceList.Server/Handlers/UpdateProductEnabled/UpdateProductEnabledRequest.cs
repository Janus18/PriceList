using MediatR;

namespace PriceList.Server.Handlers.UpdateProductEnabled;

public class UpdateProductEnabledRequest : IRequest
{
    public int ProductId { get; }
    public bool Enabled { get; }

    public UpdateProductEnabledRequest(
        int productId,
        bool enabled
    )
    {
        ProductId = productId;
        Enabled = enabled;
    }
}