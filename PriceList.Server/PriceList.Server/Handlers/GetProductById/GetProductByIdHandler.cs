using MediatR;
using Microsoft.EntityFrameworkCore;
using PriceList.Server.DataAccess;
using PriceList.Server.Domain;
using PriceList.Server.DTOs.GetProduct;

namespace PriceList.Server.Handlers.GetProductById;

public class GetProductByIdHandler : IRequestHandler<GetProductByIdRequest, GetProductResultDto?>
{
    private readonly AppDbContext _dbContext;

    public GetProductByIdHandler(
        AppDbContext dbContext
    )
    {
        _dbContext = dbContext;
    }
    
    public async Task<GetProductResultDto?> Handle(GetProductByIdRequest byIdRequest, CancellationToken cancellationToken)
    {
        var product = await _dbContext
            .Set<Product>()
            .AsQueryable()
            .Where(p => p.Id == byIdRequest.Id)
            .Include(p => p.Category)
            .Include(p => p.Provider)
            .FirstOrDefaultAsync(cancellationToken: cancellationToken);
        return product is null ? null : new GetProductResultDto(product);
    }
}