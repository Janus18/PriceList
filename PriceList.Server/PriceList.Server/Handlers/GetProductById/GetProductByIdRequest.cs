using MediatR;
using PriceList.Server.DTOs.GetProduct;

namespace PriceList.Server.Handlers.GetProductById;

public class GetProductByIdRequest : IRequest<GetProductResultDto?>
{
    public int Id { get; }

    public GetProductByIdRequest(int id)
    {
        Id = id;
    }
}