using MediatR;
using PriceList.Server.DTOs.GetProvider;

namespace PriceList.Server.Handlers.ListProviders;

public record ListProvidersRequest() : IRequest<IList<GetProviderResultDto>>;
