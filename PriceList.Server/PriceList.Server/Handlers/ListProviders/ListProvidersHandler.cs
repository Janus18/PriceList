using MediatR;
using Microsoft.EntityFrameworkCore;
using PriceList.Server.DataAccess;
using PriceList.Server.Domain;
using PriceList.Server.DTOs.GetProvider;

namespace PriceList.Server.Handlers.ListProviders;

public class ListProvidersHandler : IRequestHandler<ListProvidersRequest, IList<GetProviderResultDto>>
{
    private readonly AppDbContext _dbContext;

    public ListProvidersHandler(AppDbContext dbContext)
    {
        _dbContext = dbContext;
    }
    
    public async Task<IList<GetProviderResultDto>> Handle(ListProvidersRequest request, CancellationToken cancellationToken)
    {
        var providers = await _dbContext.Set<Provider>()
            .AsQueryable()
            .OrderBy(p => p.Name)
            .ToListAsync(cancellationToken);
        return providers.Select(p => new GetProviderResultDto(p)).ToList();
    }
}