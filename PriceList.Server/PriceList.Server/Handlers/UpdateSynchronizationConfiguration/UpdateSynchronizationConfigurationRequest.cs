using MediatR;
using PriceList.Server.DTOs.UpdateSynchronizationConfiguration;

namespace PriceList.Server.Handlers.UpdateSynchronizationConfiguration;

public record UpdateSynchronizationConfigurationRequest(
    bool Enabled,
    ushort[] Days,
    ushort[] Hours,
    string Timezone
) : IRequest
{
    public UpdateSynchronizationConfigurationRequest(UpdateSynchronizationConfigurationRequestDto request)
        : this(request.Enabled, request.Days, request.Hours, request.Timezone)
    {
    }
}
