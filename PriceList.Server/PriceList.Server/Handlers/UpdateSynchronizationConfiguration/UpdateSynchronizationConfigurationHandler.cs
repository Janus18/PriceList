using MediatR;
using PriceList.Server.DataAccess;
using PriceList.Server.Domain;
using PriceList.Server.Domain.Configurations;
using PriceList.Server.Domain.Interfaces;
using PriceList.Server.Jobs;

namespace PriceList.Server.Handlers.UpdateSynchronizationConfiguration;

public class UpdateSynchronizationConfigurationHandler(
    AppDbContext dbContext,
    IGetSynchronizationConfigurations getSynchronizationConfigurations,
    HangfireJobs hangfireJobs
) : IRequestHandler<UpdateSynchronizationConfigurationRequest>
{
    public async Task Handle(UpdateSynchronizationConfigurationRequest request, CancellationToken cancellationToken)
    {
        var configuration = await UpdateDatabase(request, cancellationToken);
        await hangfireJobs.AddOrUpdateSynchronization(configuration);
    }

    private async Task<SynchronizationConfiguration> UpdateDatabase(
        UpdateSynchronizationConfigurationRequest request,
        CancellationToken cancellationToken
    )
    {
        var configuration = await getSynchronizationConfigurations.Get(cancellationToken);
        if (configuration.CronConfiguration.IsNew)
            dbContext.Set<Configuration>().Add(configuration.CronConfiguration);
        if (configuration.EnabledConfiguration.IsNew)
            dbContext.Set<Configuration>().Add(configuration.EnabledConfiguration);
        if (configuration.TimeZoneConfiguration.IsNew)
            dbContext.Set<Configuration>().Add(configuration.TimeZoneConfiguration);
        configuration.UpdateCron(request.Days, request.Hours);
        configuration.SetStatus(request.Enabled);
        configuration.SetTimeZone(request.Timezone);
        await dbContext.SaveChangesAsync(cancellationToken);
        return configuration;
    }
}