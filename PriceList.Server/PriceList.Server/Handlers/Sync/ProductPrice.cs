using System.Text.Json.Serialization;
using PriceList.Server.Domain;

namespace PriceList.Server.Handlers.Sync;

public record ProductPrice(
    string Reference,
    [property: JsonConverter(typeof(JsonStringEnumConverter))] Currency Currency,
    decimal Price);
