using MediatR;
using PriceList.Server.Domain.Synchronizations;

namespace PriceList.Server.Handlers.Sync;

public record SyncRequest(Synchronization Synchronization) : IRequest;