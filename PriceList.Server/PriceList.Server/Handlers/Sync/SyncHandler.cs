using MediatR;
using Microsoft.EntityFrameworkCore;
using PriceList.Server.DataAccess;
using PriceList.Server.Domain;
using PriceList.Server.Domain.Synchronizations;

namespace PriceList.Server.Handlers.Sync;

public class SyncHandler(
    AppDbContext dbContext,
    IHttpClientFactory httpClientFactory,
    ILogger<SyncHandler> logger)
    : IRequestHandler<SyncRequest>
{
    private const int BatchSize = 50;

    public async Task Handle(SyncRequest request, CancellationToken cancellationToken)
    {
        List<Product> products;
        var client = httpClientFactory.CreateClient();
        int? lastProductId = null;
        do
        {
            IQueryable<Product> query = dbContext.Set<Product>()
                .Include(p => p.Provider)
                .OrderBy(p => p.Id);
            if (lastProductId.HasValue)
                query = query.Where(p => p.Id > lastProductId.Value);
            products = await query.Take(BatchSize).ToListAsync(cancellationToken);
            await SyncProducts(request.Synchronization, client, products, cancellationToken);
            lastProductId = products.Last().Id;
        } while (products.Count == BatchSize);
        
        request.Synchronization.Finish();
        await dbContext.SaveChangesAsync(cancellationToken);
    }

    private async Task SyncProducts(Synchronization synchronization, HttpClient client, IEnumerable<Product> products,
        CancellationToken cancellationToken)
    {
        foreach (var product in products)
        {
            if (!product.HasPricesUri)
                continue;
            try
            {
                var updated = await SyncProduct(client, product, cancellationToken);
                synchronization.Add(updated ? SyncStatus.Updated : SyncStatus.NotUpdated, product.Id);
            }
            catch (Exception e)
            {
                logger.LogError(e, "Product sync error. Product ID: {ProductId}.", product.Id);
                synchronization.Add(SyncStatus.Failed, product.Id, e.Message);
            }
        }

        await dbContext.SaveChangesAsync(cancellationToken);
    }

    private static async Task<bool> SyncProduct(HttpClient client, Product product, CancellationToken cancellationToken)
    {
        if (product.PricesUri == null)
            throw new ApplicationException($"Product {product.Id} does not have a valid URI");
        var response = await client.GetAsync(product.PricesUri!.AbsoluteUri, cancellationToken);
        response.EnsureSuccessStatusCode();
        var price = await response.Content.ReadFromJsonAsync<ProductPrice>(cancellationToken: cancellationToken);
        return product.SyncPrices(new Money(price!.Currency, price.Price));
    }
}