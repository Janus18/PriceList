using MediatR;
using Microsoft.EntityFrameworkCore;
using PriceList.Server.DataAccess;
using PriceList.Server.Domain;
using PriceList.Server.DTOs.GetProduct;

namespace PriceList.Server.Handlers.ListProductHistory;

public class ListProductHistoryHandler : IRequestHandler<ListProductHistoryRequest, IList<GetProductResultDto>>
{
    private readonly AppDbContext _dbContext;

    public ListProductHistoryHandler(AppDbContext dbContext)
    {
        _dbContext = dbContext;
    }
    
    public async Task<IList<GetProductResultDto>> Handle(ListProductHistoryRequest request, CancellationToken cancellationToken)
    {
        var query = _dbContext.Set<ProductHistory>().Where(p => p.ProductId == request.ProductId);
        if (request.MaxDate.HasValue)
            query = query.Where(p => p.CreatedAt < request.MaxDate.Value);
        var versions = await query
            .OrderByDescending(p => p.CreatedAt)
            .Take(request.MaxSize)
            .Include(p => p.Provider)
            .Include(p => p.Category)
            .ToListAsync(cancellationToken);
        return versions.Select(v => new GetProductResultDto(v)).ToList();
    }
}
