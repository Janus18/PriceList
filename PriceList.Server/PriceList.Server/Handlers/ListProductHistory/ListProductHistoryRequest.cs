using MediatR;
using PriceList.Server.DTOs.GetProduct;

namespace PriceList.Server.Handlers.ListProductHistory;

public record ListProductHistoryRequest(
    int ProductId,
    ushort MaxSize,
    DateTime? MaxDate = null
) : IRequest<IList<GetProductResultDto>>;
