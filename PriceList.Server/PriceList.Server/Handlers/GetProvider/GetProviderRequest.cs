using MediatR;
using PriceList.Server.DTOs.GetProvider;

namespace PriceList.Server.Handlers.GetProvider;

public record GetProviderRequest(int Id) : IRequest<GetProviderResultDto?>;