using MediatR;
using Microsoft.EntityFrameworkCore;
using PriceList.Server.DataAccess;
using PriceList.Server.Domain;
using PriceList.Server.DTOs.GetProvider;

namespace PriceList.Server.Handlers.GetProvider;

public class GetProviderHandler : IRequestHandler<GetProviderRequest, GetProviderResultDto?>
{
    private readonly AppDbContext _dbContext;

    public GetProviderHandler(AppDbContext dbContext)
    {
        _dbContext = dbContext;
    }
    
    public async Task<GetProviderResultDto?> Handle(GetProviderRequest request, CancellationToken cancellationToken)
    {
        var provider = await _dbContext.Set<Provider>().AsQueryable()
            .FirstOrDefaultAsync(p => p.Id == request.Id, cancellationToken);
        return provider is null ? null : new GetProviderResultDto(provider);
    }
}