using MediatR;
using Microsoft.EntityFrameworkCore;
using PriceList.Server.DataAccess;
using PriceList.Server.Domain;
using PriceList.Server.DTOs.GetProduct;

namespace PriceList.Server.Handlers.GetProductByCode;

public class GetProductByCodeHandler : IRequestHandler<GetProductByCodeRequest, GetProductResultDto?>
{
    private readonly AppDbContext _dbContext;

    public GetProductByCodeHandler(
        AppDbContext dbContext
    )
    {
        _dbContext = dbContext;
    }
    
    public async Task<GetProductResultDto?> Handle(GetProductByCodeRequest request, CancellationToken cancellationToken)
    {
        var product = await _dbContext.Set<Product>()
            .AsQueryable()
            .Where(p => p.Code == request.Code)
            .Include(p => p.Category)
            .Include(p => p.Provider)
            .FirstOrDefaultAsync(cancellationToken: cancellationToken);
        return product is not null ? new GetProductResultDto(product) : null;
    }
}