using MediatR;
using PriceList.Server.DTOs.GetProduct;

namespace PriceList.Server.Handlers.GetProductByCode;

public class GetProductByCodeRequest : IRequest<GetProductResultDto?>
{
    public string Code { get; }

    public GetProductByCodeRequest(string code)
    {
        Code = code;
    }
}