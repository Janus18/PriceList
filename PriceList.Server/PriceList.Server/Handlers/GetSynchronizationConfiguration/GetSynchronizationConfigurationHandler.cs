using MediatR;
using PriceList.Server.Domain.Interfaces;
using PriceList.Server.DTOs.GetSynchronizationConfiguration;

namespace PriceList.Server.Handlers.GetSynchronizationConfiguration;

public class GetSynchronizationConfigurationHandler(IGetSynchronizationConfigurations getSynchronizationConfigurations)
    : IRequestHandler<GetSynchronizationConfigurationRequest, GetSynchronizationConfigurationResponseDto>
{
    public async Task<GetSynchronizationConfigurationResponseDto> Handle(
        GetSynchronizationConfigurationRequest request,
        CancellationToken cancellationToken
    )
    {
        var synchronizationConfiguration = await getSynchronizationConfigurations.Get(cancellationToken);
        return new GetSynchronizationConfigurationResponseDto(synchronizationConfiguration);
    }
}