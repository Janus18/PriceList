import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BatchUpdateDto } from 'src/app/models/batchUpdate';
import { ProductService } from '../../services/product.service';
import { MessageDialogComponent, MessageDialogData } from '../../shared/message-dialog/message-dialog.component';
import { GenericFormControlDescriptor } from '../../shared/form-control-descriptor';
import { GenericFormDescriptor } from '../../shared/form-descriptor/form-descriptor';
import { NumberValidator } from '../../shared/functions/number-validator';
import { FromSpanishNumber } from '../../shared/functions/spanish-numbers';
import { ProviderService } from '../../services/provider.service';
import { CategoryService } from '../../services/category.service';
import { Provider } from '../../models/provider';
import { Category } from '../../models/category';

export class BatchUpdateData {
  constructor(
    public products: Array<number>
  ) { }
}

class FormValue {
  basePriceIncrement?: string;
  sellingPriceIncrement?: string;
  taxedPriceIncrement?: string;
  categoryId?: number;
  providerId?: number;

  public getDto(productsIds: Array<number>) {
    return new BatchUpdateDto(
      productsIds,
      this.basePriceIncrement ? FromSpanishNumber(this.basePriceIncrement) : undefined,
      this.taxedPriceIncrement ? FromSpanishNumber(this.taxedPriceIncrement) : undefined,
      this.sellingPriceIncrement ? FromSpanishNumber(this.sellingPriceIncrement) : undefined,
      undefined,
      this.categoryId ? this.categoryId : undefined,
      this.providerId ? this.providerId : undefined
    );
  }
}

@Component({
  selector: 'app-batch-update',
  templateUrl: './batch-update.component.html',
  styleUrls: ['./batch-update.component.scss']
})
export class BatchUpdateComponent implements OnInit {
  private controlDescriptors: Array<GenericFormControlDescriptor> = [
    {
      name: 'basePriceIncrement',
      label: 'Precio de lista (Incremento)',
      defaultValue: '',
      validations: [NumberValidator],
      validationMessage: new Map<string, string>([['pattern', 'No es un número válido']]),
    },
    {
      name: 'sellingPriceIncrement',
      label: 'Precio de venta (Incremento)',
      defaultValue: '',
      validations: [NumberValidator],
      validationMessage: new Map<string, string>([['pattern', 'No es un número válido']]),
    },
    {
      name: 'taxedPriceIncrement',
      label: 'Precio + IVA (Incremento)',
      defaultValue: '',
      validations: [NumberValidator],
      validationMessage: new Map<string, string>([['pattern', 'No es un número válido']]),
    },
    {
      name: 'categoryId',
      label: 'Categoría',
      defaultValue: undefined,
      validations: [],
      validationMessage: new Map()
    },
    {
      name: 'providerId',
      label: 'Proveedor',
      defaultValue: undefined,
      validations: [],
      validationMessage: new Map()
    },
  ];
  
  public formDescriptor: GenericFormDescriptor<FormValue> =
    new GenericFormDescriptor(this.controlDescriptors, () => new FormValue());
  
  public providers: Array<Provider> = [];
  public categories: Array<Category> = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) private data: BatchUpdateData,
    private dialogRef: MatDialogRef<BatchUpdateComponent>,
    private snackbar: MatSnackBar,
    private dialog: MatDialog,
    private productsService: ProductService,
    private providersService: ProviderService,
    private categoriesService: CategoryService
  ) { }

  ngOnInit(): void {
    this.providersService.list().subscribe(ps => this.providers = ps);
    this.categoriesService.list().subscribe(cs => this.categories = cs);
  }

  cancel(): void {
    this.dialogRef.close([]);
  }

  save(): void {
    if (!this.formDescriptor.valid) {
      const errors = this.formDescriptor.formErrors();
      this.dialog.open(MessageDialogComponent, {
        width: "480px",
        data: new MessageDialogData("Error", errors)
      });
      return;
    }

    this.productsService.batchUpdate(this.formDescriptor.value.getDto(this.data.products)).subscribe(
      result => {
        this.snackbar.open("Productos actualizados", "Cerrar", { duration: 3000 });
        this.dialogRef.close(result.body);
      },
      () => {
        this.dialog.open(
          MessageDialogComponent,
          {
            width: "300px",
            data: new MessageDialogData(
              "Error",
              "Los productos no pudieron ser actualizados, intente nuevamente en unos momentos."
            )
          }
        )
      }
    );
  }
}
