import { Component, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Product } from '../../models/product';
import { IsInt } from '../../shared/functions/is-int';
import { NumberValidator } from '../../shared/functions/number-validator';
import { MessageDialogComponent, MessageDialogData } from '../../shared/message-dialog/message-dialog.component';
import { ProductOutputDto, ProductOutputMoneyDto } from '../../models/dto/productOutputDto';
import { environment } from 'src/environments/environment';
import { Category } from '../../models/category';
import { FormControlDescriptor } from '../../shared/form-control-descriptor';
import { FormDescriptor } from '../../shared/form-descriptor/form-descriptor';
import { ScannerComponent } from '../../scanner/scanner.component';
import { Provider } from '../../models/provider';
import { CategoryService } from '../../services/category.service';
import { ProviderService } from '../../services/provider.service';
import { ProductService } from '../../services/product.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpErrorResponse } from '@angular/common/http';
import { Location } from '@angular/common';

class FormValue {
  code: string = '';
  description: string = '';
  price: string = '';
  taxedPrice: string = '';
  sellingPrice: string = '';
  frequent: boolean = false;
  category?: Category;
  provider?: Provider;
  providerReference?: string;
  units?: number;

  public getDto(productId?: number): ProductOutputDto {
    return {
      id: productId,
      code: this.code,
      description: this.description,
      basePrice: this.price
        ? ProductOutputMoneyDto.From(environment.currency, this.price)
        : undefined,
      taxedPrice: this.taxedPrice
        ? ProductOutputMoneyDto.From(environment.currency, this.taxedPrice)
        : undefined,
      sellingPrice: this.sellingPrice
        ? ProductOutputMoneyDto.From(environment.currency, this.sellingPrice)
        : undefined,
      frequent: this.frequent,
      categoryId: this.category?.id,
      providerId: this.provider?.id,
      providerReference: this.providerReference,
      units: this.units ? this.units : undefined,
    };
  }
};

@Component({
  selector: 'app-form-detail',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss']
})
export class ProductFormComponent implements OnInit {
  formDescriptor: FormDescriptor<Product, FormValue>;
  public categories: Array<Category> = [];
  public providers: Array<Provider> = [];

  public productId: number | undefined;
  public get isNew(): boolean { return !this.productId }

  controlDescriptors: Array<FormControlDescriptor<Product>> =
    [
      new FormControlDescriptor<Product>()
        .WithName('category')
        .WithLabel('Categoría')
        .WithDefaultValue(undefined)
        .WithGetter((p) => p.category ? this.categories.find(c => c.id === p.category!.id) : undefined),
      new FormControlDescriptor<Product>()
        .WithName('provider')
        .WithLabel('Proveedor')
        .WithDefaultValue(undefined)
        .WithGetter((p) => p.provider ? this.providers.find(c => c.id === p.provider!.id) : undefined),
      new FormControlDescriptor<Product>()
        .WithName('code')
        .WithLabel('Código')
        .WithDefaultValue("")
        .WithGetter((p) => p.code),
      new FormControlDescriptor<Product>()
        .WithName('description')
        .WithLabel('Descripción')
        .WithDefaultValue("")
        .WithGetter((p) => p.description)
        .WithValidators([Validators.required], [["required", "Completar"]]),
      new FormControlDescriptor<Product>()
        .WithName('frequent')
        .WithLabel('Frecuente')
        .WithDefaultValue(false)
        .WithGetter((p) => p.frequent),
      new FormControlDescriptor<Product>()
        .WithName('price')
        .WithLabel('Precio de lista')
        .WithDefaultValue("")
        .WithGetter((p) => p.basePrice.displayNoSymbol())
        .WithValidators([NumberValidator], [["pattern", "No es un número válido"]]),
      new FormControlDescriptor<Product>()
        .WithName('taxedPrice')
        .WithLabel('Precio + IVA')
        .WithDefaultValue("")
        .WithGetter((p) => p.taxedPrice.displayNoSymbol())
        .WithValidators([NumberValidator], [["pattern", "No es un número válido"]]),
      new FormControlDescriptor<Product>()
        .WithName('sellingPrice')
        .WithLabel('Precio de venta')
        .WithDefaultValue("")
        .WithGetter((p) => p.sellingPrice.displayNoSymbol())
        .WithValidators(
          [NumberValidator],
          [["pattern", "No es un número válido"]]),
      new FormControlDescriptor<Product>()
        .WithName('providerReference')
        .WithLabel('Referencia de proveedor')
        .WithDefaultValue("")
        .WithGetter((p) => p.providerReference),
      new FormControlDescriptor<Product>()
        .WithName('units')
        .WithLabel('Unidades')
        .WithDefaultValue('')
        .WithGetter((p) => p.units)
        .WithValidators(
          [Validators.pattern(/^\d*$/)],
          [["pattern", "No es un número válido"]]
        ),
    ];

  constructor(
    private productService: ProductService,
    private categoryService: CategoryService,
    private providerService: ProviderService,
    private dialog: MatDialog,
    private snackbar: MatSnackBar,
    private route: ActivatedRoute,
    private location: Location
  ) {
    this.formDescriptor = this.getFormDescriptor(undefined);
    const productId = this.route.snapshot.paramMap.get('id');
    this.productId = productId && IsInt(productId) && productId !== '0'
      ? parseInt(productId, 10)
      : undefined;
  }

  ngOnInit(): void {
    this.categoryService.list().subscribe(cs => this.categories = cs);
    this.providerService.list().subscribe(ps => this.providers = ps);
    
    if (this.isNew)
      return;

    this.productService.get(this.productId!).subscribe(
      (p) => {
        this.formDescriptor = this.getFormDescriptor(p);
      },
      () => this.dialog.open(MessageDialogComponent, {
        width: "480px",
        data: new MessageDialogData("Error", "No se pudo cargar el producto")
      })
    );
  }

  private getFormDescriptor(product?: Product): FormDescriptor<Product, FormValue> {
    return new FormDescriptor(
      product,
      this.controlDescriptors as Array<FormControlDescriptor<Product | undefined>>,
      () => new FormValue());
  }

  public save(): void {
    if (!this.formDescriptor.valid) {
      const errors = this.formDescriptor.formErrors();
      this.dialog.open(MessageDialogComponent, {
        width: "480px",
        data: new MessageDialogData("Error", errors)
      });
      return;
    }
    const value = this.formDescriptor.value;
    const product = value.getDto(this.isNew ? undefined : this.productId);
    if (this.isNew)
      this.create(product);
    else
      this.update(product);
  }

  public cancel(): void {
    this.navigateToList();
  }

  public scan(): void {
    this.dialog.open(ScannerComponent)
        .afterClosed()
        .subscribe((code: string) => {
            if (!code)
                return;
            this.formDescriptor.form.get('code')?.setValue(code);
        });
  }

  private create(product: ProductOutputDto): void {
    this.productService.addProduct(product).subscribe(
      (response) => {
        this.snackbar.open(
          `Producto creado. ID ${response.body?.id}`,
          'Cerrar',
          { duration: 3000 });
        this.navigateToList();
      },
      (error: HttpErrorResponse) => {
          const message = error.error?.code === 'ProductCodeExists'
              ? 'Ya existe un producto con el mismo código'
              : 'Se produjo un error inesperado, intente nuevamente';
          this.dialog.open(MessageDialogComponent, {
              width: "480px",
              data: new MessageDialogData("Error", message)
          });
      });
  }

  private update(product: ProductOutputDto): void {
    this.productService.update(product).subscribe(
      () => {
        this.snackbar.open('Producto actualizado', 'Cerrar', { duration: 3000 });
        this.navigateToList();
      },
      (error: HttpErrorResponse) => {
        this.dialog.open(MessageDialogComponent, {
          width: "480px",
          data: new MessageDialogData(
            'Error',
            'Se produjo un error inesperado, intente nuevamente'
          )
        });
    });
  }

  private navigateToList(): void {
    this.location.back();
  }
}
