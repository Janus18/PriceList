import { Component, Inject, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';
import { Product } from 'src/app/models/product';
import { DateTime } from 'luxon';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormControl, FormGroup } from '@angular/forms';

enum ProductPropertyId {
  Timestamp,
  Code,
  Description,
  Units,
  BasePrice,
  TaxedPrice,
  sellingPrice,
  RecommendedSellingPrice,
  ProviderReference,
  Category,
  Provider
}

function AllPropertiesIds(): Array<ProductPropertyId> {
  return Array.from(Object.keys(ProductPropertyId))
    .filter(x => !/^\d+$/.test(x))
    .map(x => ProductPropertyId[x as any] as unknown as ProductPropertyId);
}

class ProductProperty {
  constructor(
    public id: ProductPropertyId,
    public tag: string,
    public v1: string,
    public v2?: string,
    public twoVersions: boolean = false,
  ) { }

  public get changed(): boolean {
    return this.twoVersions && this.v1 !== this.v2;
  }
};

export type ProductDetailData = {
  product: Product;
};

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {
  public productId: number = 0;
  public productVersions: Array<Product> = [];
  public properties: Array<ProductProperty> = AllPropertiesIds()
    .map((k) => (new ProductProperty(k, this.tag(k), '', '')));

  public form: FormGroup = new FormGroup({
    versionA: new FormControl(0),
    versionB: new FormControl(undefined),
  });
  
  constructor(
    private productService: ProductService,
    @Inject(MAT_DIALOG_DATA) private data: ProductDetailData,
    private dialogRef: MatDialogRef<ProductDetailComponent>,
  ) {
    this.productVersions.push(this.data.product);
    this.productId = this.data.product.id;

    this.form.get('versionA')?.valueChanges.subscribe(
      (index: number) => this.updateProperties(
        this.productVersions[index],
        this.productVersions[this.form.get('versionB')?.value]
      ));

    this.form.get('versionB')?.valueChanges.subscribe(
      (index: number) => this.updateProperties(
        this.productVersions[this.form.get('versionA')?.value],
        index >= 0 ? this.productVersions[index] : undefined
      ));

    this.form.get('versionA')?.setValue(0);
  }

  ngOnInit(): void {
    this.productService.getHistory(this.data.product.id, 50).subscribe(
      (response) => {
        this.productVersions.push(...response);
        this.form.get('versionA')?.setValue(0);
      }
    );
  }

  public versionTag(product: Product): string {
    const index = this.productVersions.indexOf(product);
    return this.displayDate(index === this.productVersions.length - 1
      ? this.productVersions[0].createdAt
      : this.productVersions[index + 1].createdAt);
  }

  public goBack(): void {
    this.dialogRef.close();
  }

  private updateProperties(versionA: Product, versionB?: Product): void {
    const versionBProperties = !!versionB
      ? this.productProperties(versionB)
      : undefined;
    const versionAProperties = this.productProperties(versionA);
    for (const k of AllPropertiesIds()) {
      const property = this.properties.find(p => p.id === k)!;
      property.v1 = versionAProperties.get(k) ?? '';
      property.v2 = versionBProperties?.get(k);
      property.twoVersions = !!property.v2;
    }
  }

  private productProperties(product: Product): Map<ProductPropertyId, string> {
    return new Map([
      [ProductPropertyId.BasePrice, product.basePrice.displayNotEmpty()],
      [ProductPropertyId.Category, product.category?.description ?? 'N/D'],
      [ProductPropertyId.Code, product.code],
      [ProductPropertyId.Description, product.description],
      [ProductPropertyId.Provider, product.provider?.name ?? 'N/D'],
      [ProductPropertyId.ProviderReference, product.providerReference ?? 'N/D'],
      [ProductPropertyId.RecommendedSellingPrice, product.recommendedSellingPrice.displayNotEmpty()],
      [ProductPropertyId.TaxedPrice, product.taxedPrice.displayNotEmpty()],
      [ProductPropertyId.Timestamp, this.versionTag(product)],
      [ProductPropertyId.Units, product.units?.toString(10) ?? '1'],
      [ProductPropertyId.sellingPrice, product.sellingPrice.displayNotEmpty()]
    ]);
  }

  private displayDate(d: DateTime): string {
    return d.toFormat('dd/MM/yyyy HH:mm:ss');
  }

  private tag(id: ProductPropertyId): string {
    switch (id) {
      case ProductPropertyId.BasePrice:
        return 'Precio base';
      case ProductPropertyId.Category:
        return 'Categoría';
      case ProductPropertyId.Code:
        return 'Código';
      case ProductPropertyId.Description:
        return 'Descripción';
      case ProductPropertyId.Provider:
        return 'Proveedor';
      case ProductPropertyId.ProviderReference:
        return 'Referencia';
      case ProductPropertyId.RecommendedSellingPrice:
        return 'Precio de venta recomendado';
      case ProductPropertyId.TaxedPrice:
        return 'Precio + impuestos';
      case ProductPropertyId.Timestamp:
        return 'Fecha';
      case ProductPropertyId.Units:
        return 'Unidades';
      case ProductPropertyId.sellingPrice:
        return 'Precio de venta';
    }
  }
}
