import { Component, EventEmitter, HostListener, OnInit } from '@angular/core';
import { Product } from 'src/app/models/product';
import { BaseDataSource } from 'src/app/shared/base-data-source';
import { Column } from 'src/app/shared/generic-table/column-definition';
import { MenuAction } from 'src/app/shared/generic-table/menu-action';
import { ProductsFilter } from 'src/app/models/products-filter';
import { FormControl, FormGroup } from '@angular/forms';
import { catchError, debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { BatchUpdateComponent, BatchUpdateData } from 'src/app/products/batch-update/batch-update.component';
import { ProductService } from 'src/app/services/product.service';
import { Category } from 'src/app/models/category';
import { CategoryService } from 'src/app/services/category.service';
import { ScannerComponent } from 'src/app/scanner/scanner.component';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BarcodegenService } from 'src/app/services/barcodegen.service';
import { ProviderService } from 'src/app/services/provider.service';
import { Provider } from 'src/app/models/provider';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable, forkJoin, of } from 'rxjs';
import { ProductDetailComponent } from '../product-detail/product-detail.component';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
    public products: BaseDataSource<Product, ProductsFilter> = new BaseDataSource(
        f => !!f.id ? this.getById(f.id) : this.productService.filter(f),
        new ProductsFilter('updatedAt', false, 50)
    );
    public categories: Array<Category> = [];
    public providers: Array<Provider> = [];

    public displayFilter = false;

    public columns: Array<Column<Product>> = [
        new Column('description', 'Descripción', p => p.description),
        new Column('sellingPrice', 'Precio de Venta', p => p.unitSellingPrice.display()),
        new Column('recSellingPrice', 'Precio de Venta Rec.', p => p.unitRecommendedSellingPrice.display()),
        new Column('updatedAt', 'Última actualización', p => p.lastUpdate.toFormat("dd/MM/yyyy HH:mm"))
    ];

    public get displayedColumns(): Array<string> {
        return ['selection'].concat(this.columns.map(c => c.columnDef).concat(['actions']));
    }

    public actions: Array<MenuAction<Product>> = [
        new MenuAction(
            "Detalle",
            (p) => this.detail(p),
            "zoom_in"
        ),
        new MenuAction(
            "Editar",
            (p) => this.update(p),
            "edit"
        )
    ];

    public groupActions: Array<MenuAction<Array<number>>> = [
        new MenuAction(
            'Editar',
            (ids) => this.batchUpdate(ids),
            'edit_note'),
        new MenuAction(
            'Generar códigos',
            (ids) => this.generateBarcodes(ids),
            'qr_code'),
        new MenuAction(
            'Limpiar selección',
            () => this.clearSelection(),
            'clear_all'),
    ];

    public searchForm: FormGroup = new FormGroup({
        frequent: new FormControl(this.products.filter.frequents),
        description: new FormControl(''),
    });

    public selectionClearEmitter: EventEmitter<unknown> = new EventEmitter(true);

    public filters: Array<{ id: string, description: string, value: string }> = [];

    public get hiddenDrawer(): boolean {
        return this.windowWidth < 992;
    }

    private windowWidth: number = 0;

    @HostListener("window:resize", []) setWindowWidth() {
        this.windowWidth = window.innerWidth;
    }

    constructor(
        private productService: ProductService,
        private categoryService: CategoryService,
        private providerService: ProviderService,
        private dialog: MatDialog,
        private snackbar: MatSnackBar,
        private barcodeGenService: BarcodegenService,
        private router: Router,
        private activatedRoute: ActivatedRoute
    ) {
        this.setWindowWidth();

        this.searchForm.get('description')?.valueChanges.pipe(
            debounceTime(500),
            distinctUntilChanged()
        ).subscribe((value: string) => this.addQuery({ description: value ? value : null }));

        this.searchForm.get('frequent')?.valueChanges.subscribe(
            (value: boolean) => this.addQuery({ frequents: value ? value : null })
        );
    }

    ngOnInit(): void {
        forkJoin([
            this.categoryService.list(),
            this.providerService.list()
        ]).subscribe(([cs, ps]) => {
            this.categories = cs;
            this.providers = ps;
            this.initFormWithQuery(this.activatedRoute.snapshot.queryParamMap);
            this.initFiltersWithQuery(this.activatedRoute.snapshot.queryParamMap);
        });
        this.activatedRoute.queryParamMap.subscribe(map => {
            this.products.reset(f => {
                f.id = this.getNumberFromQuery(map, 'id');
                f.code = map.get('code') ?? undefined;
                f.description = map.get('description') ?? undefined;
                f.categoryId = this.getNumberFromQuery(map, 'categoryId');
                f.providerId = this.getNumberFromQuery(map, 'providerId');
                f.frequents = map.get('frequents') === 'true';
                f.synchronizationId = this.getNumberFromQuery(map, 'synchronizationId');
            });
            const hasDescription = map.has('description') && map.get('description')!.length > 0;
            this.products.canGetMore = !hasDescription;
            this.products.filter.column = hasDescription ? 'description' : 'updatedAt';
        });
    }

    public rowIdentifier(product: Product): number {
        return product.id;
    }

    public scan(): void {
        this.dialog.open(ScannerComponent, { data: { hasInput: true } })
            .afterClosed()
            .subscribe((code: string) => {
                if (!code)
                    return;
                this.addQuery({ code });
                this.pushCodeFilter(code);
            });
    }

    public clearSearch(): void {
        this.searchForm.get('description')?.setValue('');
    }

    public clearUpdatedAt(): void {
        this.searchForm.get('updatedAt')?.setValue('');
    }

    public clearSelection(): void {
        this.selectionClearEmitter.emit({});
    }

    public create(): void {
        this.router.navigate(['products', '0']);
    }

    public batchUpdate(productIds: Array<number>): void {
        if (!productIds || productIds.length === 0) {
            this.noProductSelected();
            return;
        }
        this.dialog.open(BatchUpdateComponent,
        {
            width: "480px",
            data: new BatchUpdateData(productIds)
        }).afterClosed().subscribe(
            () => {
                this.products.reset();
            }
        );
    }

    public generateBarcodes(productIds: Array<number>): void {
        if (!productIds || productIds.length < 1) {
            this.noProductSelected();
            return;
        }
        const codes = this.products.data.filter(p => productIds.includes(p.id)).map(p => p.code);
        this.barcodeGenService.generate(codes).save();
    }

    public update(product: Product): void {
        this.router.navigate(['products', product.id]);
    }

    public detail(product: Product): void {
        this.dialog.open(ProductDetailComponent, { data: { product }, width: '600px', height: '90%' });
    }

    public filterRemoved(id: string): void {
        this.filters = this.filters.filter(f => f.id !== id);
        const query: { [x: string]: string | null } = {};
        query[id] = null;
        this.addQuery(query);
    }

    public categorySelected(category: Category): void {
        if (this.categoryIsSelected)
            return;
        this.pushCategoryFilter(category);
        this.addQuery({ categoryId: category?.id });
    }

    public providerSelected(provider: Provider): void {
        if (this.providerIsSelected)
            return;
        this.pushProviderFilter(provider);
        this.addQuery({ providerId: provider?.id });
    }

    public get categoryIsSelected(): boolean {
        return this.filters.some(f => f.id === 'categoryId');
    }

    public get providerIsSelected(): boolean {
        return this.filters.some(f => f.id === 'providerId');
    }

    public get filtersCount(): number {
        return this.filters.length;
    }

    private noProductSelected(): void {
        this.snackbar.open("Seleccionar al menos un producto para continuar", "Cerrar", { duration: 3000 });
    }

    private addQuery(query: {}): void {
        this.router.navigate(
            [],
            {
                relativeTo: this.activatedRoute,
                queryParamsHandling: 'merge',
                queryParams: query
            });
    }

    private getNumberFromQuery(map: ParamMap, key: string): number | undefined {
        if (map.has(key) && /^\d*$/.test(map.get(key)!))
            return parseInt(map.get(key)!);
        return undefined;
    }

    private initFormWithQuery(map: ParamMap): void {
        if (map.has('description'))
            this.searchForm.get('description')?.setValue(map.get('description'), { emitEvent: false });
        if (map.has('frequents'))
            this.searchForm.get('frequents')?.setValue(map.get('frequents') === 'true', { emitEvent: false });
    }

    private initFiltersWithQuery(map: ParamMap): void {
        if (map.has('id'))
            this.pushIdFilter(this.getNumberFromQuery(map, 'id'));
        if (map.has('code'))
            this.pushCodeFilter(map.get('code') ?? undefined);
        if (map.has('synchronizationId'))
            this.filters.push({ id: 'synchronizationId', description: 'Sincronización', value: map.get('synchronizationId')! });
        if (map.has('categoryId') && this.categories.some(c => c.id === this.getNumberFromQuery(map, 'categoryId')))
            this.pushCategoryFilter(this.categories.find(c => c.id === this.getNumberFromQuery(map, 'categoryId')));
        if (map.has('providerId') && this.providers.some(c => c.id === this.getNumberFromQuery(map, 'providerId')))
            this.pushProviderFilter(this.providers.find(c => c.id === this.getNumberFromQuery(map, 'providerId')));
    }

    private pushIdFilter(id?: number) {
        if (!id)
            return;
        this.filters.push({ id: 'id', description: 'ID', value: id.toString(10) });
    }

    private pushCategoryFilter(category?: Category) {
        if (!category)
            return;
        this.filters.push({ id: 'categoryId', description: 'Categoría', value: category.description });
    }

    private pushProviderFilter(provider?: Provider) {
        if (!provider)
            return;
        this.filters.push({ id: 'providerId', description: 'Proveedor', value: provider.name });
    }

    private pushCodeFilter(code?: string) {
        if (!code)
            return;
        this.filters.push({ id: 'code', description: 'Código', value: code });
    }

    private getById(id: number): Observable<Array<Product>> {
        return this.productService.get(id).pipe(
            map(p => !!p ? [p] : []),
            catchError(_ => of([]))
        );
    }
}
