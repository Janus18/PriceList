import { Validators } from "@angular/forms";

export const NumberValidator = Validators.pattern(/^(\d{1,3})?((\.\d{3})+|\d+)(\,\d+)?$/);
