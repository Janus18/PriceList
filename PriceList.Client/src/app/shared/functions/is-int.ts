export const IsInt = (x: string) => /^\d+$/.test(x);
