import * as currency from "currency.js";

export function FromSpanishNumber(n: string): string {
    return new currency(n, { decimal: ',', separator: '.' })
        .format({ symbol: '', decimal: '.', separator: '' });
}

export function ToSpanishNumber(n: string): string {
    return new currency(n, { decimal: '.', separator: '' })
        .format({ symbol: '', decimal: ',', separator: '.' });
}
