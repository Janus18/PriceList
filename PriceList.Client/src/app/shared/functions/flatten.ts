export function flatten<T>(xs: Array<Array<T>>): Array<T> {
    return xs.reduce((prev, current) => prev.concat(current), [])
}
