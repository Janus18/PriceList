export function Insert<T>(xs: Array<T>, x: T, compare: (a: T, b: T) => number): Array<T> {
    const next = xs.findIndex(y => compare(y, x) > 0);
    if (next < 0)
        xs.push(x);
    else
        xs.splice(next, 0, x);
    return xs;
}
