import { AbstractControl, FormControl, ValidationErrors } from "@angular/forms";

export function ArrayValidator<T>(xs: Array<T>): (control: AbstractControl) => ValidationErrors | null {
    return (control: AbstractControl) => {
        if (!xs.includes(control.value))
            return { array: true };
        return null;
    }
}