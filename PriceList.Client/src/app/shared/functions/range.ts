export function Range(from: number = 0, to: number): Array<number> {
    if (to <= from)
        return [];
    const xs = new Array<number>(to - from + 1);
    for (let i = 0; i < to - from + 1; i++)
        xs[i] = i + from;
    return xs;
}