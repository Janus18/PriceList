import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';


export class ConfirmationDialogData {
  constructor(
    public title: string,
    public message: string
  ) { }
}


@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.scss']
})
export class ConfirmationDialogComponent {

  title: string = "";
  message: string = "";

  constructor(@Inject(MAT_DIALOG_DATA) private data: ConfirmationDialogData) {
    if (data && data.title) {
      this.title = data.title;
    }
    if (data && data.message) {
      this.message = data.message;
    }
  }
}
