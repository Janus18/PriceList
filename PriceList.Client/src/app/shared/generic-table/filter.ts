export abstract class Filter<T> {
    private _pageSize: number;

    constructor(
        public column: string,
        public ascending: boolean,
        pageSize: number
    ) {
        this._pageSize = pageSize;
    }
    
    set pageSize(x: number) {
        this._pageSize = Math.round(Math.abs(x));
    }

    get pageSize(): number {
        return this._pageSize;
    }

    public abstract firstPage(): void;

    public abstract nextPage(values: Array<T>): void;
}

