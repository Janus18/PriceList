import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GenericTableComponent } from './generic-table.component';
import { Filter } from './filter';

describe('GenericTableComponent', () => {
  let component: GenericTableComponent<unknown, unknown, Filter<unknown>>;
  let fixture: ComponentFixture<GenericTableComponent<unknown, unknown, Filter<unknown>>>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GenericTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GenericTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
