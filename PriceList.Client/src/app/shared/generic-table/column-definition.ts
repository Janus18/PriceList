import { TemplateRef } from '@angular/core';

export class Column<T> {
    constructor(
        public columnDef: string,
        public header: string,
        public value?: (row: T) => string,
        public reference?: TemplateRef<unknown>,
        public sorts: boolean = false
    ) { }
}
