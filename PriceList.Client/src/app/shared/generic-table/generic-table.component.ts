import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BaseDataSource } from 'src/app/shared/base-data-source';
import { Column } from 'src/app/shared/generic-table/column-definition';
import { MenuAction } from 'src/app/shared/generic-table/menu-action';
import { Filter } from 'src/app/shared/generic-table/filter';
import { RowProperties } from './row-properties';


@Component({
  selector: 'app-generic-table',
  templateUrl: './generic-table.component.html',
  styleUrls: ['./generic-table.component.scss']
})
export class GenericTableComponent<T, TId, T2 extends Filter<T>> implements OnInit {
  @Input() dataSource?: BaseDataSource<T, T2>;
  @Input() columns?: Array<Column<T>>;
  @Input() displayedColumns?: Array<string>;
  @Input() actions: Array<MenuAction<T>> = [];
  @Input() groupActions: Array<MenuAction<Array<TId>>> = [];
  @Input() rowsProperties?: (t: T) => RowProperties;
  @Input() rowIdentifier?: (t: T) => TId;
  @Input() selectionClear?: EventEmitter<unknown>;
  
  @Output() rowClick: EventEmitter<T>;

  private selection: Set<TId> = new Set();

  constructor() {
    this.rowClick = new EventEmitter<T>();
  }

  ngOnInit(): void {
    if (!this.selectionClear)
      return;
    this.selectionClear.asObservable().subscribe(
      () => this.selection.clear()
    );
  }

  public get mobileColumns(): Array<Column<T>> {
    return this.columns?.filter(c => this.displayed(c)) ?? [];
  }

  public displayed(column: Column<T>): boolean {
    return this.displayedColumns?.includes(column.columnDef) ?? false;
  }

  public isTopColumn(column: Column<T>): boolean {
    return !!this.displayedColumns && column.columnDef === this.displayedColumns[0];
  }

  public get showSelection(): boolean {
    return this.displayedColumns?.some(c => c === 'selection') === true;
  }

  public get showActions(): boolean {
    return this.displayedColumns?.some(c => c === "actions") === true;
  }

  public onRowClick(row: T): void {
    this.rowClick.emit(row);
  }

  public rowIsSelected(t: T): boolean {
    return !!(this.rowIdentifier && this.selection.has(this.rowIdentifier(t)));
  }

  public onRowSelected(t: T): void {
    if (!this.rowIdentifier)
      return;
    const identifier = this.rowIdentifier(t);
    if (this.selection.has(identifier))
      this.selection.delete(identifier);
    else
      this.selection.add(identifier);
  }

  public hasGroupActions(t: T): boolean {
    return !!(this.groupActions &&
      this.groupActions.length > 0 &&
      this.rowIdentifier &&
      this.selection.has(this.rowIdentifier(t)));
  }

  public groupActionEnabled(action: MenuAction<Array<TId>>): boolean {
    return action.active(Array.from(this.selection));
  }

  public groupActionTrigger(action: MenuAction<Array<TId>>): void {
    action.action(Array.from(this.selection));
  }

  public hasEnabledActions(row: T): boolean {
    return this.actions?.some(x => x.active(row)) === true;
  }
}
