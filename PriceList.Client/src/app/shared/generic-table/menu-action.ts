export class MenuAction<T> {
    public name: string;
    public active: (p: T) => boolean;
    public action: (p: T) => void;
    public icon?: string;
    public color: string;

    constructor(
        name: string,
        action: (p: T) => void,
        icon?: string,
        active?: (p: T) => boolean,
        color?: string
    ) {
        this.name = name;
        this.action = action;
        this.active = active ? active : (p: T) => true;
        this.icon = icon;
        this.color = color ? color : 'primary';
    }
}
