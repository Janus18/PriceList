import { DataSource, CollectionViewer } from '@angular/cdk/collections';
import { BehaviorSubject, Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { Filter } from './generic-table/filter';

export class BaseDataSource<T, T2 extends Filter<T>> implements DataSource<T> {
    private formsSubject = new BehaviorSubject<T[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    private _hasMore = true;

    public canGetMore = true;

    constructor(
        private reload: (f: T2) => Observable<Array<T>>,
        public filter: T2
    ) { }

    public get hasMore(): boolean {
        return this.canGetMore && this._hasMore;
    }

    public loading$ = this.loadingSubject.asObservable();

    set loadFunction(r: (f: T2) => Observable<Array<T>>) {
        this.reload = r;
    }

    public get data(): Array<T> {
        return this.formsSubject.value;
    }

    public connect(collectionViewer: CollectionViewer): Observable<T[]> {
        return this.formsSubject.asObservable();
    }

    public disconnect(collectionViewer: CollectionViewer): void {
        this.formsSubject.complete();
        this.loadingSubject.complete();
    }

    public clear(): void {
        this.formsSubject.next([]);
    }

    public next(): void {
        if (!this._hasMore) {
            return;
        }
        this.load();
    }

    public reset(updateFilterFn?: (x: T2) => void): void {
        if (updateFilterFn) {
            updateFilterFn(this.filter);
        }
        this.filter.firstPage();
        this.clear();
        this.load();
    }

    private load(): void {
        this.loadingSubject.next(true);
        this.reload(this.filter)
        .pipe(finalize(() => this.loadingSubject.next(false)))
        .subscribe(result => {
            if (result) {
                this.formsSubject.next(this.formsSubject.value.concat(result));
                this.filter.nextPage(result);
            }
            this._hasMore = result && result.length >= this.filter.pageSize;
        });
    }
}

