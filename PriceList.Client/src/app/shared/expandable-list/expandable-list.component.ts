import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IDisplayable } from '../displayable';

@Component({
  selector: 'app-expandable-list',
  templateUrl: './expandable-list.component.html',
  styleUrls: ['./expandable-list.component.scss']
})
export class ExpandableListComponent<T extends IDisplayable> {
  @Input() title: string = '';
  @Input() values: Array<T> = [];
  @Output() selected: EventEmitter<T> = new EventEmitter(true);
  
  public expanded: boolean = false;

  public get displayedValues(): Array<T> {
    return this.expanded ? this.values : this.values.slice(0, 5);
  }

  public get expandable(): boolean {
    return this.values.length > 5;
  }

  public onValueSelected(value: T): void {
    this.selected.emit(value);
  }

  public expand(): void {
    this.expanded = !this.expanded;
  }
}
