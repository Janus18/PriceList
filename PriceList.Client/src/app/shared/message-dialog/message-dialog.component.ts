import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';


export class MessageDialogData {
  constructor(
    public title: string,
    public message: string | Array<string>
  ) { }
}


@Component({
  selector: 'app-message-dialog',
  templateUrl: './message-dialog.component.html',
  styleUrls: ['./message-dialog.component.scss']
})
export class MessageDialogComponent implements OnInit {

  get title(): string {
    return this.data && this.data.title ? this.data.title : "";
  }

  get message(): string | undefined {
    return this.data && this.data.message && typeof(this.data.message) === "string" ? this.data.message : undefined;
  }

  get messages(): Array<string> | undefined {
    return this.data && this.data.message && typeof(this.data.message) !== "string" ? this.data.message : undefined;
  }

  constructor(@Inject(MAT_DIALOG_DATA) private data: MessageDialogData) { }

  ngOnInit(): void {
  }

}
