import { ValidatorFn } from "@angular/forms"

export type GenericFormControlDescriptor = {
    name: string;
    label: string;
    defaultValue: unknown;
    validations: ValidatorFn[];
    validationMessage: Map<string, string>;
};

export class FormControlDescriptor<T> {
    _name: string = '';
    _label: string = '';
    _defaultValue: unknown = undefined;
    _getter: (p: T) => unknown = () => undefined;
    _validations: ValidatorFn[] = [];
    _validationMessage: Map<string, string> = new Map<string, string>();

    public get name(): string { return this._name; }
    public get label(): string { return this._label; }
    public get defaultValue(): unknown { return this._defaultValue; }
    public get getter(): (p: T) => unknown { return this._getter; }
    public get validations(): ValidatorFn[] { return this._validations; }
    public get validationMessage(): Map<string, string> { return this._validationMessage; }

    public WithName(name: string): FormControlDescriptor<T> {
        this._name = name;
        return this;
    }

    public WithLabel(label: string): FormControlDescriptor<T> {
        this._label = label;
        return this;
    }

    public WithDefaultValue(value: unknown): FormControlDescriptor<T> {
        this._defaultValue = value;
        return this;
    }

    public WithGetter(getter: (p: T) => unknown): FormControlDescriptor<T> {
        this._getter = getter;
        return this;
    }

    public WithValidators(validators: ValidatorFn[], messages: [string, string][]): FormControlDescriptor<T> {
        this._validations = validators;
        this._validationMessage = new Map<string, string>(messages);
        return this;
    }
};
