import { FormControl, FormGroup } from "@angular/forms";
import { FormControlDescriptor, GenericFormControlDescriptor } from "../form-control-descriptor";
import { flatten } from "../functions/flatten";

export class GenericFormDescriptor<TValue> {
    public form: FormGroup;

    constructor(
        private controls: Array<GenericFormControlDescriptor>,
        private valueConstructor: () => TValue
    ) {
        this.form = new FormGroup({});
        for (const control of controls) {
            this.form.addControl(
                control.name,
                new FormControl(control.defaultValue, control.validations)
            );
        }
    }

    public get valid() {
        return this.form.valid;
    }

    public get value(): TValue {
        const value = this.valueConstructor();
        return Object.assign(value as {}, this.form.value);
    }

    public getLabel(controlName: string): string {
        return this.controls.find(c => c.name === controlName)?.label ?? "";
    }
    
    public formErrors(): Array<string> {
        const errors = this.controls.map(descriptor => {
            const control = this.form.get(descriptor.name);
            if (control === undefined || control === null || control.valid)
                return [];
            return Array.from(descriptor.validationMessage.entries())
                .filter(([k, v]) => control.hasError(k))
                .map(([k, v]) => `${descriptor.label}: ${v}`);
        });
        return flatten(errors);
    }
}

export class FormDescriptor<TLoaded, TValue> extends GenericFormDescriptor<TValue> {
    public form: FormGroup;

    constructor(
        obj: TLoaded,
        controls: Array<FormControlDescriptor<TLoaded>>,
        valueConstructor: () => TValue
    ) {
        super(controls, valueConstructor);
        this.form = new FormGroup({});
        for (const control of controls) {
            this.form.addControl(control.name, new FormControl(
                obj === null || obj === undefined ? control.defaultValue : control.getter(obj),
                control.validations
            ));
        }
    }
}
