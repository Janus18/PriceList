export interface IDisplayable {
    display(): string;
}
