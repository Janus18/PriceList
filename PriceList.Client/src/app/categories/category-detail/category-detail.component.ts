import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, Inject } from '@angular/core';
import { Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Category } from 'src/app/models/category';
import { CategoryService } from 'src/app/services/category.service';
import { MessageDialogComponent, MessageDialogData } from 'src/app/shared/message-dialog/message-dialog.component';
import { FormControlDescriptor } from 'src/app/shared/form-control-descriptor';
import { FormDescriptor } from 'src/app/shared/form-descriptor/form-descriptor';
import { CategoryOutputDto } from 'src/app/models/dto/categoryOutputDto';


export class CategoryData {
  constructor(
    public category?: Category
  ) { }
}

class FormValue {
  public name: string = '';

  public getDto(categoryId?: number): CategoryOutputDto {
    return {
      id: categoryId,
      description: this.name,
    };
  }
}


@Component({
  selector: 'app-category-detail',
  templateUrl: './category-detail.component.html',
  styleUrls: ['./category-detail.component.scss']
})
export class CategoryDetailComponent {
  isNew: boolean = true;
  formDescriptor: FormDescriptor<Category, FormValue>;
  private controlDescriptors: Array<FormControlDescriptor<Category>> = [
    new FormControlDescriptor<Category>()
      .WithName('name')
      .WithLabel('Nombre')
      .WithDefaultValue('')
      .WithGetter((c) => c.description)
      .WithValidators([Validators.required], [["required", "Completar"]])
  ];

  get title(): string {
    return `${this.isNew ? "Crear" : "Editar"} Categoría`;
  }

  constructor(
    @Inject(MAT_DIALOG_DATA) private data: CategoryData,
    private dialogRef: MatDialogRef<CategoryDetailComponent>,
    private dialog: MatDialog,
    private categoryService: CategoryService,
    private snackbar: MatSnackBar
  ) {
    const category = data?.category;
    this.isNew = category === null || category === undefined;
    this.formDescriptor = new FormDescriptor(
      category,
      this.controlDescriptors as Array<FormControlDescriptor<Category | undefined>>,
      () => new FormValue());
  }

  save(): void {
    if (this.formDescriptor.valid) {
      this.saveCategory(
        this.formDescriptor.value.getDto(this.isNew ? undefined : this.data.category!.id)
      );
    }
    else {
      const data = new MessageDialogData("Error", this.formDescriptor.formErrors());
      this.dialog.open(MessageDialogComponent, { width: "380px", data: data });
    }
  }

  cancel(): void {
    this.dialogRef.close(this.isNew ? undefined : false);
  }

  private saveCategory(category: CategoryOutputDto): void {
    if (this.isNew) {
      this.categoryService.add(category).subscribe(
        result => this.onSaveFinished(result),
        (_: HttpErrorResponse) => this.snackbar.open("Error", "Cerrar", { duration: 3000 })
      );
    } else {
      this.categoryService.update(category).subscribe(
        result => this.onSaveFinished(result),
        (_: HttpErrorResponse) => this.snackbar.open("Error", "Cerrar", { duration: 3000 })
      );
    }
  }

  private onSaveFinished(result: HttpResponse<unknown>): void {
    if (this.isNew && result.status === 201) {
      this.snackbar.open("Categoría creada", "Cerrar", { duration: 3000 });
      this.dialogRef.close(result.body);
    }
    else if (! this.isNew && result.status === 204) {
      this.snackbar.open("Categoría actualizada", "Cerrar", { duration: 3000 });
      this.dialogRef.close(true);
    }
    else {
      this.snackbar.open("Error", "Cerrar", { duration: 3000 });
    }
  }
}
