import { Component, OnInit } from '@angular/core';
import { Column } from 'src/app/shared/generic-table/column-definition';
import { Category } from 'src/app/models/category';
import { BaseDataSource } from 'src/app/shared/base-data-source';
import { CategoryService } from 'src/app/services/category.service';
import { MenuAction } from 'src/app/shared/generic-table/menu-action';
import { MatDialog } from '@angular/material/dialog';
import { CategoryDetailComponent, CategoryData } from 'src/app/categories/category-detail/category-detail.component';
import { ConfirmationDialogComponent, ConfirmationDialogData } from 'src/app/shared/confirmation-dialog/confirmation-dialog.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpErrorResponse } from '@angular/common/http';
import { MessageDialogComponent, MessageDialogData } from 'src/app/shared/message-dialog/message-dialog.component';
import { CategoriesFilter } from 'src/app/models/categories-filter';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.scss']
})
export class CategoryListComponent implements OnInit {
  categories: BaseDataSource<Category, CategoriesFilter> = new BaseDataSource(
    f => this.categoryService.list(),
    new CategoriesFilter()
  );
  columns: Array<Column<Category>> = [
    new Column('description', 'Descripción', c => c.description),
    new Column('lastUpdate', 'Última actualización', c => c.lastUpdate.toFormat("dd/MM/yyyy HH:mm")),
  ];
  displayedColumns: Array<string> = this.columns.map(c => c.columnDef).concat(['actions']);
  actions: Array<MenuAction<Category>> = [
    new MenuAction('Actualizar', c => this.update(c), 'edit', () => true, 'primary'),
    new MenuAction('Borrar', c => this.delete(c), 'clear', () => false, 'warn')
  ];

  constructor(
    private categoryService: CategoryService,
    private dialog: MatDialog,
    private snackbar: MatSnackBar,
  ) { }

  ngOnInit(): void {
    this.categories.next();
  }

  create(): void {
    this.dialog.open(
      CategoryDetailComponent,
      { width: "380px", data: new CategoryData(undefined) }
    ).afterClosed().subscribe((result: boolean) => {
      if (result) {
        this.categories.reset();
      }
    });
  }

  private update(c: Category): void {
    this.dialog.open(
      CategoryDetailComponent,
      { width: "380px", data: new CategoryData(c) }
    ).afterClosed().subscribe((result: boolean) => {
      if (result) {
        this.categories.reset();
      }
    });
  }

  private delete(c: Category): void {
    this.dialog.open(
      ConfirmationDialogComponent,
      {
        width: "380px",
        data: new ConfirmationDialogData("Confirmar", `Se eliminará la categoría ${c.description}`)
      }
    ).afterClosed().subscribe((answer: boolean) => {
      if (answer) {
        this.deleteCategory(c.id);
      }
    });
  }

  private deleteCategory(categoryId: number): void {
    this.categoryService.delete(categoryId).subscribe(
      result => {
        if (result.status === 204) {
          this.snackbar.open("Categoría eliminada", "Cerrar", { duration: 3000 });
        }
        this.categories.reset();
      },
      (error: HttpErrorResponse) => {
        let message: string;
        if (error.status === 404) {
          message = "La categoría que se intenta eliminar no fue encontrada";
        }
        else if (error.status === 409) {
          message = "La categoría no puede ser eliminada";
        }
        else {
          message = "Ocurrió un error inesperado al eliminar la categoría";
        }
        const data = new MessageDialogData("Error", message);
        this.dialog.open(MessageDialogComponent, { width: "380px", data: data });
      }
    );
  }
}
