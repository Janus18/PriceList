import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SynchronizationListComponent } from './synchronization-list.component';

describe('SynchronizationListComponent', () => {
  let component: SynchronizationListComponent;
  let fixture: ComponentFixture<SynchronizationListComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SynchronizationListComponent]
    });
    fixture = TestBed.createComponent(SynchronizationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
