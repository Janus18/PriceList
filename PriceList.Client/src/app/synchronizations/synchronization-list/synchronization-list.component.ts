import { AfterViewInit, Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { BaseDataSource } from 'src/app/shared/base-data-source';
import { Synchronization } from 'src/app/models/synchronization';
import { SynchronizationsFilter } from 'src/app/models/synchronizations-filter';
import { Column } from 'src/app/shared/generic-table/column-definition';
import { MenuAction } from 'src/app/shared/generic-table/menu-action';
import { SynchronizationService } from 'src/app/services/synchronization.service';
import { DateTime } from 'luxon';
import { MatDialog } from '@angular/material/dialog';
import { MessageDialogComponent, MessageDialogData } from 'src/app/shared/message-dialog/message-dialog.component';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-synchronization-list',
  templateUrl: './synchronization-list.component.html',
  styleUrls: ['./synchronization-list.component.scss']
})
export class SynchronizationListComponent implements OnInit, AfterViewInit {
  public synchronizations: BaseDataSource<Synchronization, SynchronizationsFilter> =
    new BaseDataSource(f => this.synchronizationService.list(f), new SynchronizationsFilter());
  
  @ViewChild('ErrorsCell') errorsCell: TemplateRef<unknown> | null = null;
  
  public columns: Array<Column<Synchronization>> = [
    new Column('id', 'ID', s => s.id.toString(10)),
    new Column('createdAt', 'Creada', s => this.formatDate(s.createdAt)),
    new Column('finishedAt', 'Finalizada', s => s.finishedAt ? this.formatDate(s.finishedAt) : ''),
    new Column('total', 'Total', s => s.total.toString(10)),
    new Column('updated', 'Actualizados', s => s.updated.toString(10)),
  ];
  
  public get displayedColumns(): Array<string> {
    return this.columns.map(c => c.columnDef).concat(['actions']);
  } 
  
  public actions: Array<MenuAction<Synchronization>> = [
    new MenuAction('Actualizados', (s) => this.updatedProducts(s), 'check_circle', (s) => s.updated > 0, 'primary'),
    new MenuAction('Errores', (s) => this.displayErrors(s), 'info', (s) => s.errors?.length > 0, 'warn'),
  ];

  constructor(
    private synchronizationService: SynchronizationService,
    private dialog: MatDialog,
    private router: Router,
    private snackbar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.synchronizations.next();
  }

  ngAfterViewInit(): void {
    this.columns.push(new Column('failed', 'Errores', undefined, this.errorsCell!));
  }

  sync(): void {
    this.synchronizationService.trigger().subscribe(
      () => {
        this.snackbar.open('Sincronización iniciada', 'Cerrar');
        setTimeout(() => this.refresh(), 2000);
      },
      () => this.snackbar.open('Error: La sincronización no pudo ser iniciada', 'Cerrar')
    );
  }

  refresh(): void {
    this.synchronizations.reset();
  }

  private formatDate(d: DateTime): string {
    return d.toLocal().setLocale('es-AR').toFormat('ff');
  }

  private displayErrors(s: Synchronization): void {
    const data = new MessageDialogData('Detalle - Errores', s.errors.map(x => `<a href="/?id=${x.productId}">Producto ${x.productId}</a>: ${x.message}`));
    this.dialog.open(MessageDialogComponent, { width: '480px', data });
  }

  private updatedProducts(s: Synchronization): void {
    this.router.navigate([''], { queryParams: { synchronizationId: s.id.toString(10) } });
  }
}
