import { Component, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { MatOptionSelectionChange } from '@angular/material/core';
import { Location } from '@angular/common';
import { SynchronizationService } from 'src/app/services/synchronization.service';
import { SyncConfigOption, SynchronizationConfiguration } from 'src/app/models/synchronization-configuration';
import { FormDescriptor } from 'src/app/shared/form-descriptor/form-descriptor';
import { FormControlDescriptor } from 'src/app/shared/form-control-descriptor';
import { MatDialog } from '@angular/material/dialog';
import { MessageDialogComponent, MessageDialogData } from 'src/app/shared/message-dialog/message-dialog.component';
import { TimeZone } from 'src/app/models/time-zone';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ArrayValidator } from 'src/app/shared/functions/array-validator';

class SynchronizationConfigurationFormResult {
  public enabled: boolean = true;
  public days: Array<number> = [];
  public hours: Array<number> = [];
  public timezone: string = '';

  public ToSyncConfig(): SynchronizationConfiguration {
    return new SynchronizationConfiguration(
      this.days,
      this.enabled,
      this.hours,
      new TimeZone(this.timezone)
    );
  }
}

@Component({
  selector: 'app-synchronization-configuration',
  templateUrl: './synchronization-configuration.component.html',
  styleUrls: ['./synchronization-configuration.component.scss']
})
export class SynchronizationConfigurationComponent implements OnInit {
  public days: Array<SyncConfigOption> = SynchronizationConfiguration.AllDays();
  public hours: Array<SyncConfigOption> = SynchronizationConfiguration.AllHours();
  private timezones: Array<string> = Intl.supportedValuesOf('timeZone');
  public filteredTimezones: Array<string> = this.timezones;

  public formDescriptor: FormDescriptor<SynchronizationConfiguration, SynchronizationConfigurationFormResult> =
    new FormDescriptor(
      undefined,
      [
        new FormControlDescriptor<SynchronizationConfiguration | undefined>()
          .WithDefaultValue([])
          .WithGetter((c) => c?.days ?? [])
          .WithLabel('Días')
          .WithName('days')
          .WithValidators([Validators.required], [['required', 'Seleccionar al menos 1 día']]),
        new FormControlDescriptor<SynchronizationConfiguration | undefined>()
          .WithDefaultValue(true)
          .WithGetter((c) => c?.enabled ?? true)
          .WithLabel('Habilitada')
          .WithName('enabled'),
        new FormControlDescriptor<SynchronizationConfiguration | undefined>()
          .WithDefaultValue([])
          .WithGetter((c) => c?.hours ?? [])
          .WithLabel('Horas')
          .WithName('hours')
          .WithValidators([Validators.required], [['required', 'Seleccionar al menos 1 hora']]),
        new FormControlDescriptor<SynchronizationConfiguration | undefined>()
          .WithDefaultValue(TimeZone.User().name)
          .WithGetter((c) => c?.timezone?.name ?? TimeZone.User().name)
          .WithLabel('Zona')
          .WithName('timezone')
          .WithValidators(
            [Validators.required, ArrayValidator(this.timezones)],
            [['required', 'Completar'], ['array', 'Debe seleccionar un valor']]),
      ], () => new SynchronizationConfigurationFormResult()
    );

  constructor(
    private synchronizationService: SynchronizationService,
    private location: Location,
    private dialog: MatDialog,
    private snackbar: MatSnackBar
  ) {
    this.setTimeZoneFiltering();
  }

  ngOnInit(): void {
    this.synchronizationService.getConfiguration().subscribe(sc => {
      this.formDescriptor.form.patchValue({
        enabled: sc.enabled,
        days: sc.days,
        hours: sc.hours,
        timezone: sc.timezone.name
      });
    });
  }

  public allHours(event: MatOptionSelectionChange<any>) {
    this.formDescriptor.form.get('hours')?.setValue(event.source.selected ? this.hours.map((h) => h.id) : []);
  }

  public allDays(event: MatOptionSelectionChange<any>): void {
    this.formDescriptor.form.get('days')?.setValue(event.source.selected ? this.days.map((h) => h.id) : []);
  }

  public save(): void {
    if (!this.formDescriptor.valid) {
      const data = new MessageDialogData('Errores', this.formDescriptor.formErrors());
      this.dialog.open(MessageDialogComponent, { data });
      return;
    }
    const config = this.formDescriptor.value.ToSyncConfig();
    this.synchronizationService.saveConfiguration(config).subscribe(
      (r) => {
        if (r.ok) {
          this.snackbar.open('Configuración guardada', 'Cerrar', { duration: 3000 });
          this.goBack();
        } else
          this.OnError()
      }, (err) => this.OnError()
    )
  }

  public goBack(): void {
    this.location.back();
  }

  private OnError(): void  {
    const data = new MessageDialogData('Error', 'Ocurrió un error inesperado al actualizar la configuración.');
    this.dialog.open(MessageDialogComponent, { data });
  }

  private setTimeZoneFiltering(): void {
    this.formDescriptor.form.controls['timezone'].valueChanges.subscribe(
      (value: string) => {
        const lowerValue = value.toLowerCase();
        this.filteredTimezones = this.timezones.filter(t => t.toLocaleLowerCase().includes(lowerValue));
      }
    );
  }
}
