import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SynchronizationConfigurationComponent } from './synchronization-configuration.component';

describe('SynchronizationConfigurationComponent', () => {
  let component: SynchronizationConfigurationComponent;
  let fixture: ComponentFixture<SynchronizationConfigurationComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SynchronizationConfigurationComponent]
    });
    fixture = TestBed.createComponent(SynchronizationConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
