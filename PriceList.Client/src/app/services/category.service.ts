import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Category } from '../models/category';
import { CategoryInputDto } from '../models/dto/categoryInputDto';
import { map, tap } from 'rxjs/operators';
import { CategoryOutputDto } from '../models/dto/categoryOutputDto';
import { Insert } from '../shared/functions/insert';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  private cache: Array<Category> | undefined = undefined;

  constructor(private client: HttpClient) { }

  list(): Observable<Array<Category>> {
    if (this.cache !== undefined)
      return of(this.cache);
    const url = `${environment.apiurl}/categories`;
    return this.client.get<Array<CategoryInputDto>>(url, { observe: 'body', responseType: 'json' })
      .pipe(
        map(cs => cs.map(c => Category.FromInput(c))),
        tap(cs => this.cache = cs)
      );
  }

  add(category: CategoryOutputDto): Observable<HttpResponse<{ id: number }>> {
    const url = `${environment.apiurl}/categories`;
    return this.client.post<{ id: number }>(
      url, category,
      { observe: 'response', responseType: 'json' }
    ).pipe(
      tap(resp => resp.status === 201
        ? this.insertInOrder(Category.FromOutput(resp.body!.id, category))
        : undefined)
    );
  }

  update(category: CategoryOutputDto): Observable<HttpResponse<undefined>> {
    const url = `${environment.apiurl}/categories`;
    return this.client.put<undefined>(
      url, category,
      { observe: 'response', responseType: 'json' }
    ).pipe(
      tap(resp => {
        if (resp.status !== 204)
          return;
        const catgy = this.cache?.find(c => c.id === category.id)?.update(category)
        if (catgy)
          this.moveInOrder(catgy);
      })
    );
  }

  delete(categoryId: number): Observable<HttpResponse<undefined>> {
    const url = `${environment.apiurl}/categories/${categoryId}`;
    return this.client.delete<undefined>(
      url, { observe: 'response', responseType: 'json' }
    ).pipe(
      tap(resp => resp.status === 204
        ? this.cache = this.cache?.filter(c => c.id !== categoryId)
        : undefined)
    );
  }

  private insertInOrder(category: Category): void {
    if (!this.cache)
      return;
    Insert(this.cache, category, (a, b) => a.description.localeCompare(b.description));
  }

  private moveInOrder(category: Category): void {
    if (!this.cache)
      return;
    const toDelete = this.cache.findIndex(c => c.id === category.id);
    if (toDelete >= 0)
      this.cache.splice(toDelete, 1);
    this.insertInOrder(category);
  }
}
