import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { BatchUpdateDto } from '../models/batchUpdate';
import { Product } from '../models/product';
import { ProductsFilter } from '../models/products-filter';
import { ProductInputDto } from '../models/dto/productInputDto';
import { map } from 'rxjs/operators';
import { ProductOutputDto } from '../models/dto/productOutputDto';
import { DateTime } from 'luxon';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  constructor(
    private client: HttpClient
  ) {}
  
  get(id: number): Observable<Product> {
    return this.client.get<ProductInputDto>(
      `${environment.apiurl}/products/${id}`,
      { observe: 'body', responseType: 'json' }
    ).pipe(
      map(p => Product.FromInput(p))
    );
  }

  filter(filter: ProductsFilter): Observable<Array<Product>> {
    const queries = [
      `updatedAt=${filter.updatedAt.toISO()}`,
      `size=${filter.pageSize}`
    ];
    if (filter.code)
      queries.push(`code=${filter.code}`);
    if (filter.description)
      queries.push(`description=${filter.description}`);
    if (filter.categoryId)
      queries.push(`categoryId=${filter.categoryId}`);
    if (filter.providerId)
      queries.push(`providerId=${filter.providerId}`);
    if (filter.synchronizationId)
      queries.push(`synchronizationId=${filter.synchronizationId}`);
    queries.push(`frequent=${filter.frequents}`);
    return this.client.get<Array<ProductInputDto>>(
      `${environment.apiurl}/products/all?${queries.join('&')}`,
      { observe: 'body', responseType: 'json' }
    ).pipe(
      map(ps => ps.map(p => Product.FromInput(p)))
    );
  }

  setFrequent(
    productId: number,
    frequent: boolean
  ): Observable<HttpResponse<undefined>> {
    return this.client.put<undefined>(
      `${environment.apiurl}/products/${productId}/frequent?value=${frequent}`,
      undefined,
      { responseType: "json", observe: "response" }
    );
  }

  update(producto: ProductOutputDto): Observable<HttpResponse<undefined>> {
    return this.client.put<undefined>(
      `${environment.apiurl}/products`,
      producto,
      { responseType: "json", observe: "response" }
    );
  }

  batchUpdate(dto: BatchUpdateDto): Observable<HttpResponse<undefined>> {
    return this.client.put<undefined>(
      `${environment.apiurl}/products/all`,
      dto,
      { responseType: "json", observe: "response" }
    );
  }

  addProduct(product: ProductOutputDto): Observable<HttpResponse<{ id: number }>> {
    return this.client.post<{ id: number }>(
      `${environment.apiurl}/products`, product,
      { responseType: 'json', observe: 'response' }
    );
  }

  getHistory(productId: number, maxSize: number, maxDate?: DateTime): Observable<Array<Product>> {
    const queries = [
      ['maxSize', maxSize.toString(10)]
    ];
    if (maxDate) {
      queries.push(['maxDate', maxDate.toISOTime() ?? '']);
    }
    const query = queries.map(q => q.join('=')).join('&');
    return this.client.get<Array<ProductInputDto>>(
      `${environment.apiurl}/products/${productId}/history?${query}`,
      { observe: 'body', responseType: 'json' }
    ).pipe(
      map((ps) => ps.map(p => Product.FromInput(p)))
    );
  }
}
