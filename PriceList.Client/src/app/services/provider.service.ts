import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Provider } from '../models/provider';
import { environment } from 'src/environments/environment';
import { ProviderInputDto } from '../models/dto/providerInputDto';
import { map, tap } from 'rxjs/operators';
import { ProviderOutputDto } from '../models/dto/providerOutputDto';
import { Insert } from '../shared/functions/insert';

@Injectable({
  providedIn: 'root'
})
export class ProviderService {
  private cache: Array<Provider> | undefined = undefined;

  constructor(private client: HttpClient) { }

  list(): Observable<Array<Provider>> {
    if (this.cache !== undefined)
      return of(this.cache);
    const url = `${environment.apiurl}/providers`;
    return this.client.get<Array<ProviderInputDto>>(url, { observe: 'body', responseType: 'json' })
      .pipe(
        map(cs => cs.map(c => Provider.FromInput(c))),
        tap(cs => this.cache = cs)
      );
  }

  add(category: ProviderOutputDto): Observable<HttpResponse<{ id: number }>> {
    const url = `${environment.apiurl}/providers`;
    return this.client.post<{ id: number }>(
      url, category,
      { observe: 'response', responseType: 'json' }
    ).pipe(
      tap(x => x.status === 201
        ? this.insertInOrder(Provider.FromOutput(x.body!.id, category))
        : undefined)
    );
  }

  update(provider: ProviderOutputDto): Observable<HttpResponse<undefined>> {
    const url = `${environment.apiurl}/providers`;
    return this.client.put<undefined>(
      url, provider,
      { observe: 'response', responseType: 'json' }
    ).pipe(
      tap(x => {
        if (x.status !== 204)
          return;
        const cachedProvider = this.cache?.find(p => p.id === provider.id);
        if (!cachedProvider)
          return;
        this.moveInOrder(cachedProvider.update(provider));
      })
    );
  }

  private insertInOrder(provider: Provider): void {
    if (!this.cache)
      return;
    Insert(this.cache, provider, (x, y) => x.name.localeCompare(y.name));
  }

  private moveInOrder(provider: Provider): void {
    if (!this.cache)
      return;
    const toDelete = this.cache.findIndex(c => c.id === provider.id);
    if (toDelete >= 0)
      this.cache.splice(toDelete, 1);
    this.insertInOrder(provider);
  }
}
