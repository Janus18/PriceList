import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Synchronization } from '../models/synchronization';
import { environment } from 'src/environments/environment';
import { SynchronizationInputDto } from '../models/dto/synchronizationInputDto';
import { map } from 'rxjs/operators';
import { SynchronizationsFilter } from '../models/synchronizations-filter';
import { SynchronizationConfiguration } from '../models/synchronization-configuration';
import { SynchronizationConfigurationInputDto } from '../models/dto/synchronizationConfigurationInputDto';

@Injectable({
  providedIn: 'root'
})
export class SynchronizationService {
  constructor(private client: HttpClient) { }

  list(filter: SynchronizationsFilter): Observable<Array<Synchronization>> {
    const queries = [
      ['until', filter.until.toISO()],
      ['maxSize', filter.pageSize]
    ];
    const query = queries.map(q => q.join('=')).join('&');
    return this.client.get<Array<SynchronizationInputDto>>(
      `${environment.apiurl}/synchronizations?${query}`,
      { observe: 'body', responseType: 'json' }
    ).pipe(
      map(cs => cs.map(c => Synchronization.FromInput(c)))
    );
  }

  trigger(): Observable<HttpResponse<unknown>> {
    return this.client.post<unknown>(
      `${environment.apiurl}/synchronizations`, {},
      { observe: 'response' }
    );
  }

  getConfiguration(): Observable<SynchronizationConfiguration> {
    return this.client.get<SynchronizationConfigurationInputDto>(
      `${environment.apiurl}/synchronizations/configuration`, { observe: 'body' }
    ).pipe(
      map(c => SynchronizationConfiguration.FromInput(c))
    );
  }

  saveConfiguration(config: SynchronizationConfiguration): Observable<HttpResponse<unknown>> {
    return this.client.put(
      `${environment.apiurl}/synchronizations/configuration`,
      config.ToOutput(),
      { observe: 'response' }
    );
  }
}
