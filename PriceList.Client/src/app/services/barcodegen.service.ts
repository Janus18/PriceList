import { Injectable } from '@angular/core';
import * as kjua from 'kjua-svg';
import * as jspdf from 'jspdf';

@Injectable({
  providedIn: 'root'
})
export class BarcodegenService {
  static pageWidth = 210;
  static pageHeight = 297;
  static columnsPerPage = 7;
  static rowsPerPage = 7;
  static cellWidth = 24;
  static cellHeight = 24;
  static borderTopBottom = (BarcodegenService.pageHeight - (BarcodegenService.rowsPerPage * BarcodegenService.cellHeight)) / 2;

  constructor() { }

  public generate(values: Array<string>): jspdf.jsPDF {
    const document = new jspdf.jsPDF();
    for (let i = 0; i < values.length; i++) {
      const position = this.elementCoordinates(i);
      const x = ((BarcodegenService.pageWidth / BarcodegenService.columnsPerPage) / 2) - (BarcodegenService.cellWidth / 2) + (position.x * (BarcodegenService.pageWidth / BarcodegenService.columnsPerPage));
      const y = BarcodegenService.borderTopBottom + (position.y * BarcodegenService.cellHeight) + 1;
      document.addImage(this.getBarcodeData(values[i]), "JPG", x, y, BarcodegenService.cellWidth - 2, BarcodegenService.cellHeight - 2);
      if (position.y === BarcodegenService.rowsPerPage - 1 && position.x === BarcodegenService.columnsPerPage - 1) {
        document.addPage();
      }
    }
    return document;
  }

  private elementCoordinates(i: number): { x: number, y: number } {
    return {
      x: i % BarcodegenService.columnsPerPage,
      y: Math.floor(i / BarcodegenService.rowsPerPage)
    };
  }

  private getBarcodeData(text: string, size = 900) {
    return kjua({
      render: "canvas",
      crisp: true,
      minVersion: 1,
      ecLevel: "Q",
      size: size,
      ratio: undefined,
      fill: "#333",
      back: "#fff",
      text,
      rounded: 10,
      quiet: 2,
      mode: "label",
      mSize: 5,
      mPosX: 50,
      mPosY: 100,
      label: text,
      fontname: "sans-serif",
      fontcolor: "#3F51B5",
      image: undefined
    });
  }
}
