import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductListComponent } from 'src/app/products/product-list/product-list.component';
import { CategoryListComponent } from 'src/app/categories/category-list/category-list.component';
import { ProviderListComponent } from 'src/app/providers/provider-list/provider-list.component';
import { ProductFormComponent } from 'src/app/products/product-form/product-form.component';
import { SynchronizationListComponent } from 'src/app/synchronizations/synchronization-list/synchronization-list.component';
import { SynchronizationConfigurationComponent } from './synchronizations/synchronization-configuration/synchronization-configuration.component';

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        component: ProductListComponent
    },
    {
        path: 'products/:id',
        pathMatch: 'full',
        component: ProductFormComponent
    },
    {
        path: 'categories',
        pathMatch: 'full',
        component: CategoryListComponent
    },
    {
        path: 'providers',
        pathMatch: 'full',
        component: ProviderListComponent
    },
    {
        path: 'synchronizations',
        pathMatch: 'full',
        component: SynchronizationListComponent
    },
    {
        path: 'synchronizations/configure',
        pathMatch: 'full',
        component: SynchronizationConfigurationComponent
    },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
