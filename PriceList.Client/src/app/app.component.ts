import { Component, OnInit } from '@angular/core';
import { SynchronizationService } from './services/synchronization.service';
import { SynchronizationsFilter } from './models/synchronizations-filter';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public synchronizationsAlarm: boolean = false;
  
  constructor(
    private synchronizationService: SynchronizationService
  ) { }

  ngOnInit(): void {
    const filter = new SynchronizationsFilter();
    filter.pageSize = 1;
    this.synchronizationService.list(filter).subscribe(
      (xs) => this.synchronizationsAlarm = xs.length > 0 && xs[0].hasErrors
    );
  }
}
