import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { registerLocaleData } from '@angular/common';

import localeEsAr from '@angular/common/locales/es';
registerLocaleData(localeEsAr);

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { MaterialModule } from 'src/app/material/material.module';
import { ProductListComponent } from './products/product-list/product-list.component';
import { GenericTableComponent } from './shared/generic-table/generic-table.component';
import { MessageDialogComponent } from './shared/message-dialog/message-dialog.component';
import { ProductFormComponent } from './products/product-form/product-form.component';
import { BatchUpdateComponent } from './products/batch-update/batch-update.component';
import { CategoryDetailComponent } from './categories/category-detail/category-detail.component';
import { ConfirmationDialogComponent } from './shared/confirmation-dialog/confirmation-dialog.component';
import { CategoryListComponent } from './categories/category-list/category-list.component';
import { ScannerComponent } from './scanner/scanner.component';
import { NgxScannerQrcodeModule, LOAD_WASM } from 'ngx-scanner-qrcode';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { RouterModule } from '@angular/router';
import { ProviderDetailComponent } from './providers/provider-detail/provider-detail.component';
import { ProviderListComponent } from './providers/provider-list/provider-list.component';
import { SynchronizationListComponent } from './synchronizations/synchronization-list/synchronization-list.component';
import { ProductDetailComponent } from './products/product-detail/product-detail.component';
import { ExpandableListComponent } from './shared/expandable-list/expandable-list.component';
import { SynchronizationConfigurationComponent } from './synchronizations/synchronization-configuration/synchronization-configuration.component';

LOAD_WASM().subscribe();

@NgModule({
  declarations: [
    AppComponent,
    ProductListComponent,
    GenericTableComponent,
    MessageDialogComponent,
    ProductFormComponent,
    BatchUpdateComponent,
    CategoryDetailComponent,
    ConfirmationDialogComponent,
    CategoryListComponent,
    ScannerComponent,
    ProviderDetailComponent,
    ProviderListComponent,
    SynchronizationListComponent,
    ProductDetailComponent,
    ExpandableListComponent,
    SynchronizationConfigurationComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NoopAnimationsModule,
    MaterialModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxScannerQrcodeModule,
    RouterModule,
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'es-Ar' },
    { provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'outline', subscriptSizing: 'dynamic' } }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
