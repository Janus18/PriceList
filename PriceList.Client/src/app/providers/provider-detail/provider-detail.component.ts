import { HttpResponse } from '@angular/common/http';
import { Component, Inject } from '@angular/core';
import { Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ProviderOutputDto } from 'src/app/models/dto/providerOutputDto';
import { Provider } from 'src/app/models/provider';
import { ProviderService } from 'src/app/services/provider.service';
import { FormControlDescriptor } from 'src/app/shared/form-control-descriptor';
import { FormDescriptor } from 'src/app/shared/form-descriptor/form-descriptor';
import { NumberValidator } from 'src/app/shared/functions/number-validator';
import { MessageDialogComponent, MessageDialogData } from 'src/app/shared/message-dialog/message-dialog.component';

export type ProviderDetailData = {
  provider: Provider;
};

class FormValue {
  public name: string = '';
  public pricesHost: string = '';
  public profitMargin: string = '';
  public taxes: string = '';

  public getDto(id?: number): ProviderOutputDto {
    return new ProviderOutputDto(
      id,
      this.name,
      this.pricesHost,
      this.profitMargin,
      this.taxes
    );
  }
}

@Component({
  selector: 'app-provider-detail',
  templateUrl: './provider-detail.component.html',
  styleUrls: ['./provider-detail.component.scss']
})
export class ProviderDetailComponent {
  isNew: boolean;
  formDescriptor: FormDescriptor<Provider, FormValue>;
  controlDescriptors: Array<FormControlDescriptor<Provider>> = [
    new FormControlDescriptor<Provider>()
      .WithName('name')
      .WithLabel('Nombre')
      .WithDefaultValue('')
      .WithGetter((p) => p.name)
      .WithValidators([Validators.required], [['required', 'Completar']]),
    new FormControlDescriptor<Provider>()
      .WithName('pricesHost')
      .WithLabel('Host precios')
      .WithDefaultValue('')
      .WithGetter((p) => p.pricesHost),
    new FormControlDescriptor<Provider>()
      .WithName('profitMargin')
      .WithLabel('Margen')
      .WithDefaultValue('')
      .WithGetter((p) => p.profitMargin ?? '')
      .WithValidators([NumberValidator], [['pattern', 'No es un número válido']]),
    new FormControlDescriptor<Provider>()
      .WithName('taxes')
      .WithLabel('Impuestos')
      .WithDefaultValue('')
      .WithGetter((p) => p.taxes ?? '')
      .WithValidators([NumberValidator], [['pattern', 'No es un número válido']]),
  ];

  get title(): string {
    return `${this.isNew ? "Crear" : "Editar"} Categoría`;
  }
    
  constructor(
    @Inject(MAT_DIALOG_DATA) private data: ProviderDetailData,
    private dialogRef: MatDialogRef<ProviderDetailComponent>,
    private providerService: ProviderService,
    private snackbar: MatSnackBar,
    private dialog: MatDialog
  ) {
    this.isNew = !this.data.provider;
    this.formDescriptor = new FormDescriptor(
      data.provider,
      this.controlDescriptors,
      () => new FormValue());
  }

  save(): void {
    if (this.formDescriptor.valid) {
      this.saveProvider(
        this.formDescriptor.value.getDto(this.isNew ? undefined : this.data.provider.id)
      );
    }
    else {
      const data = new MessageDialogData("Error", this.formDescriptor.formErrors());
      this.dialog.open(MessageDialogComponent, { width: "380px", data: data });
    }
  }

  cancel(): void {
    this.dialogRef.close(false);
  }

  private saveProvider(provider: ProviderOutputDto): void {
    if (this.isNew) {
      this.providerService.add(provider).subscribe(
        (result: HttpResponse<unknown>) => this.onSaveFinished(result),
        () => this.snackbar.open("Error", "Cerrar", { duration: 3000 })
      );
    } else {
      this.providerService.update(provider).subscribe(
        (result: HttpResponse<unknown>) => this.onSaveFinished(result),
        () => this.snackbar.open("Error", "Cerrar", { duration: 3000 })
      );
    }
  }

  private onSaveFinished(result: HttpResponse<unknown>): void {
    if (result.status !== 201 && result.status !== 204) {
      this.snackbar.open("Error", "Cerrar", { duration: 3000 });
      return;
    }
    this.snackbar.open(
      result.status === 201 ? "Proveedor creado" : "Proveedor actualizado",
      "Cerrar",
      { duration: 3000 });
    this.dialogRef.close(true);
  }
}
