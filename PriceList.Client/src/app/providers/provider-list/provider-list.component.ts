import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Provider } from 'src/app/models/provider';
import { ProviderService } from 'src/app/services/provider.service';
import { BaseDataSource } from 'src/app/shared/base-data-source';
import { Column } from 'src/app/shared/generic-table/column-definition';
import { MenuAction } from 'src/app/shared/generic-table/menu-action';
import { ProviderDetailComponent, ProviderDetailData } from '../provider-detail/provider-detail.component';
import { ProvidersFilter } from 'src/app/models/providers-filter';

@Component({
  selector: 'app-provider-list',
  templateUrl: './provider-list.component.html',
  styleUrls: ['./provider-list.component.scss']
})
export class ProviderListComponent implements OnInit {
  providers: BaseDataSource<Provider, ProvidersFilter> = new BaseDataSource(
    f => this.providerService.list(),
    new ProvidersFilter()
  );
  columns: Array<Column<Provider>> = [
    new Column('name', 'Nombre', c => c.name),
    new Column('taxes', 'Impuestos', c => c.taxes ?? ''),
    new Column('profitMargin', 'Margen', c => c.profitMargin ?? ''),
    new Column('lastUpdate', 'Última actualización', c => c.lastUpdate.toFormat("dd/MM/yyyy HH:mm")),
  ];
  displayedColumns: Array<string> = this.columns.map(c => c.columnDef).concat(['actions']);
  actions: Array<MenuAction<Provider>> = [
    new MenuAction('Actualizar', c => this.update(c), 'edit', () => true, 'primary')
  ];

  constructor(
    private dialog: MatDialog,
    private providerService: ProviderService
  ) { }

  ngOnInit(): void {
    this.providers.next();
  }

  create(): void {
    this.dialog.open(
      ProviderDetailComponent,
      { width: "380px", data: {} }
    ).afterClosed().subscribe((result: boolean) => {
      if (result) {
        this.providers.reset();
      }
    });
  }

  private update(provider: Provider): void {
    this.dialog.open(
      ProviderDetailComponent,
      { width: "380px", data: { provider } as ProviderDetailData }
    ).afterClosed().subscribe((result: boolean) => {
      if (result) {
        this.providers.reset();
      }
    });
  }
}
