import { DateTime } from "luxon";
import { Filter } from "../shared/generic-table/filter";
import { Synchronization } from "./synchronization";

export class SynchronizationsFilter extends Filter<Synchronization> {
    constructor(
        public until: DateTime = DateTime.utc(),
        pageSize: number = 20
    ) {
        super('createdAt', false, pageSize);
        this.pageSize = pageSize;
    }

    public firstPage(): void {
        this.until = DateTime.utc();
    }

    public nextPage(values: Synchronization[]): void {
        this.until = values[values.length - 1].createdAt;
    }
}
