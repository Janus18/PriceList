import { DateTime } from "luxon";
import { CategoryInputDto } from "./dto/categoryInputDto";
import { CategoryOutputDto } from "./dto/categoryOutputDto";
import { IDisplayable } from "../shared/displayable";

export class Category implements IDisplayable {
    constructor(
        public id: number,
        public description: string,
        public createdAt: DateTime,
        public updatedAt?: DateTime
    ) { }

    display(): string {
        return this.description;
    }
    
    public static FromInput(category: CategoryInputDto): Category {
        return new Category(
            category.id,
            category.description,
            DateTime.fromISO(category.createdAt),
            category.updatedAt ? DateTime.fromISO(category.updatedAt) : undefined
        );
    }

    public static FromOutput(id: number, category: CategoryOutputDto): Category {
        return new Category(id, category.description, DateTime.local(), undefined);
    }

    public update(category: CategoryOutputDto): Category {
        this.description = category.description;
        this.updatedAt = DateTime.local();
        return this;
    }

    public get lastUpdate(): DateTime {
        return this.updatedAt ? this.updatedAt : this.createdAt;
    }
}

