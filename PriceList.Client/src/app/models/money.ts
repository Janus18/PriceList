import * as currencyjs from "currency.js";
import { MoneyInputDto } from "./dto/moneyInputDto";

export abstract class Money {
    public abstract currency: string;
    
    constructor(protected amount: currencyjs) {}

    public static Empty(): Money {
        return new EmptyMoney();
    }

    public static FromInput(money: MoneyInputDto): Money {
        return Money.Parse(money.currency, money.amount);
    }

    public static FromInputUnit(money: MoneyInputDto): Money {
        return Money.Parse(money.currency, money.unitAmount);
    }

    public static Parse(currency: string, amount: string): Money {
        if (currency === "ARS") {
            return new Pesos(amount);
        }
        return Money.Empty();
    }

    public display(): string {
        return this.amount.format();
    }

    public abstract displayNoSymbol(): string;

    public displayNotEmpty(): string {
        return this.display();
    }

    public get cable(): string {
        return this.amount.format({ symbol: '', decimal: '.', separator: '' });
    }
}

export class Pesos extends Money {
    public currency: string = "ARS";
    static Parser = (value: string | number | currencyjs) =>
        currencyjs(value, { symbol: '$', decimal: ',', separator: '.' });
    
    constructor(value: string | number | currencyjs) {
        super(Pesos.Parser(value));
    }

    override displayNoSymbol(): string {
        return this.amount.format({ symbol: '', decimal: ',', separator: '.' });
    }
}

export class EmptyMoney extends Money {
    public currency: string = "";

    constructor() {
        super(currencyjs(0));
    }

    public override display(): string {
        return "";
    }

    public override displayNoSymbol(): string {
        return "";
    }

    override displayNotEmpty(): string {
        return "N/D";
    }
}
