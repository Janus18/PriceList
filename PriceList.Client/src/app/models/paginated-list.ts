
export class PaginatedList<T> {
    constructor(
        public totalCount: number,
        public items: Array<T>
    ) {}
}

