import { DateTime } from "luxon";

export class TimeZone {
    constructor(
        public name: string,
    ) { }
    
    public static User(): TimeZone {
        return new TimeZone(DateTime.local().zone.name);
    }
}
