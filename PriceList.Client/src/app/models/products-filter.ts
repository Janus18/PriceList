import { DateTime } from 'luxon';
import { Filter } from 'src/app/shared/generic-table/filter';
import { Product } from './product';

export class ProductsFilter extends Filter<Product> {
    constructor(
        column: string,
        ascending: boolean,
        pageSize: number
    ) {
        super(column, ascending, pageSize);
    }

    public id?: number;
    public categoryId?: number;
    public providerId?: number;
    public frequents: boolean = false;
    public code?: string;
    public updatedAt: DateTime = DateTime.utc();
    public description?: string;
    public synchronizationId?: number;

    public firstPage(): void {
        this.updatedAt = DateTime.utc();
    }

    public nextPage(values: Product[]): void {
        this.updatedAt = values.length > 0 ? values[values.length - 1].lastUpdate : DateTime.utc();
    }
}

