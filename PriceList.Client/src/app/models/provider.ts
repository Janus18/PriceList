import { DateTime } from "luxon";
import { ProviderInputDto } from "./dto/providerInputDto";
import { ToSpanishNumber } from "../shared/functions/spanish-numbers";
import { ProviderOutputDto } from "./dto/providerOutputDto";
import { IDisplayable } from "../shared/displayable";

export class Provider implements IDisplayable {
    constructor(
        public createdAt: DateTime,
        public id: number,
        public name: string,
        public pricesHost?: string,
        public profitMargin?: string,
        public taxes?: string,
        public updatedAt?: DateTime | undefined
    ) { }

    display(): string {
        return this.name;
    }

    public static FromInput(provider: ProviderInputDto): Provider {
        return new Provider(
            DateTime.fromISO(provider.createdAt),
            provider.id,
            provider.name,
            provider.pricesHost,
            provider.profitMargin ? ToSpanishNumber(provider.profitMargin) : undefined,
            provider.taxes ? ToSpanishNumber(provider.taxes) : undefined,
            provider.updatedAt ? DateTime.fromISO(provider.updatedAt) : undefined
        );
    }

    public static FromOutput(id: number, provider: ProviderOutputDto): Provider {
        return new Provider(
            DateTime.local(),
            id,
            provider.name,
            provider.pricesHost,
            provider.profitMargin ? ToSpanishNumber(provider.profitMargin) : undefined,
            provider.taxes ? ToSpanishNumber(provider.taxes) : undefined,
            DateTime.local()
        );
    }

    public update(provider: ProviderOutputDto): Provider {
        this.name = provider.name;
        this.pricesHost = provider.pricesHost;
        this.profitMargin = provider.profitMargin ? ToSpanishNumber(provider.profitMargin) : undefined;
        this.taxes = provider.taxes ? ToSpanishNumber(provider.taxes) : undefined;
        this.updatedAt = DateTime.local();
        return this;
    }

    public get lastUpdate(): DateTime {
        return this.updatedAt ? this.updatedAt : this.createdAt;
    }
}