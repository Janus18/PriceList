import { DateTime } from "luxon";
import { SynchronizationInputDto } from "./dto/synchronizationInputDto";

export class Synchronization {
    constructor(
        public id: number,
        public createdAt: DateTime,
        public finishedAt: DateTime | undefined,
        public total: number,
        public updated: number,
        public failed: number,
        public errors: SynchronizationError[]
    ) { }
    
    public static FromInput(input: SynchronizationInputDto): Synchronization {
        return new Synchronization(
            input.id,
            DateTime.fromISO(input.createdAt),
            input.finishedAt ? DateTime.fromISO(input.finishedAt) : undefined,
            input.total,
            input.updated,
            input.failed,
            input.errors.map(x => SynchronizationError.FromInput(x))
        );
    }

    public get hasErrors(): boolean {
        return this.failed > 0;
    }
}

export class SynchronizationError {
    constructor(
        public productId: number,
        public message: string
    ) { }

    public static FromInput(input: { productId: number; message: string; }): SynchronizationError {
        return new SynchronizationError(input.productId, input.message);
    }
}
