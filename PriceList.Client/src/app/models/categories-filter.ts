import { Filter } from "../shared/generic-table/filter";
import { Category } from "./category";

export class CategoriesFilter extends Filter<Category> {
    constructor() {
        super('description', true, 50);
    }

    public firstPage(): void {}
    public nextPage(values: Category[]): void {}

}