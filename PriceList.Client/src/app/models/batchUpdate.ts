
export class BatchUpdateDto {
    constructor(
        public productsIds: Array<number>,
        public basePriceIncrement?: string,
        public taxedPriceIncrement?: string,
        public sellingPriceIncrement?: string,
        public frequent?: boolean,
        public categoryId?: number,
        public providerId?: number
    ) { }
}
