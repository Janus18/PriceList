import { Filter } from "../shared/generic-table/filter";
import { Provider } from "./provider";

export class ProvidersFilter extends Filter<Provider> {
    constructor() {
        super('name', true, 50);
    }

    public firstPage(): void {}
    public nextPage(values: Provider[]): void {}
}