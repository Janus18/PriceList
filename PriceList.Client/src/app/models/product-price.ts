import { Observable, of } from "rxjs";
import { ProductPriceInputDto } from "./dto/productPriceInputDto";
import { catchError, first, map, share, tap } from "rxjs/operators";

export class ProductPrice {
    private getter: Observable<string>;

    constructor(
        public reference: string,
        getter: Observable<ProductPriceInputDto>
    ) {
        this.getter = getter.pipe(
            map((p) => p.price),
            catchError(() => "Error"),
            share()
        );
    }

    public get value(): Observable<string> {
        return this.getter;
    }
}

export class EmptyProductPrice extends ProductPrice {
    constructor() {
        super("", of({ reference: "", price: "" }));
    }

    public override get value(): Observable<string> {
        return of("-");
    }
}
