import { DateTime } from "luxon";
import { Category } from "./category";
import { ProductInputDto } from "./dto/productInputDto";
import { Money } from "./money";
import { Provider } from "./provider";

export class Product {
    constructor(
        public id: number,
        public code: string,
        public description: string,
        public basePrice: Money,
        public taxedPrice: Money,
        public sellingPrice: Money,
        public recommendedSellingPrice: Money,
        public unitBasePrice: Money,
        public unitTaxedPrice: Money,
        public unitSellingPrice: Money,
        public unitRecommendedSellingPrice: Money,
        public frequent: boolean,
        public createdAt: DateTime,
        public updatedAt?: DateTime,
        public category?: Category,
        public provider?: Provider,
        public providerReference?: string,
        public units?: number
    ) { }

    public static FromInput(p: ProductInputDto): Product {
        return new Product(
            p.id,
            p.code,
            p.description,
            p.basePrice ? Money.FromInput(p.basePrice) : Money.Empty(),
            p.taxedPrice ? Money.FromInput(p.taxedPrice) : Money.Empty(),
            p.sellingPrice ? Money.FromInput(p.sellingPrice) : Money.Empty(),
            p.recommendedSellingPrice ? Money.FromInput(p.recommendedSellingPrice) : Money.Empty(),
            p.basePrice ? Money.FromInputUnit(p.basePrice) : Money.Empty(),
            p.taxedPrice ? Money.FromInputUnit(p.taxedPrice) : Money.Empty(),
            p.sellingPrice ? Money.FromInputUnit(p.sellingPrice) : Money.Empty(),
            p.recommendedSellingPrice ? Money.FromInputUnit(p.recommendedSellingPrice) : Money.Empty(),
            p.frequent,
            DateTime.fromISO(p.createdAt),
            p.updatedAt ? DateTime.fromISO(p.updatedAt) : undefined,
            p.category ? Category.FromInput(p.category) : undefined,
            p.provider ? Provider.FromInput(p.provider) : undefined,
            p.providerReference,
            p.units
        );
    }

    public get lastUpdate(): DateTime {
        return this.updatedAt ? this.updatedAt : this.createdAt;
    }
}

