import { DateTime } from "luxon";
import { SynchronizationConfigurationInputDto } from "./dto/synchronizationConfigurationInputDto";
import { SynchronizationConfigurationOutputDto } from "./dto/synchronizationConfigurationOutputDto";
import { GetLocale } from "../shared/functions/locale";
import { Range } from '../shared/functions/range';
import { TimeZone } from "./time-zone";

export type SyncConfigOption = {
    id: number;
    description: string;
};

export class SynchronizationConfiguration {
    constructor(
        public days: Array<number>,
        public enabled: boolean,
        public hours: Array<number>,
        public timezone: TimeZone
    ) { }

    public static FromInput(input: SynchronizationConfigurationInputDto): SynchronizationConfiguration {
        return new SynchronizationConfiguration(
            input.days,
            input.enabled,
            input.hours,
            new TimeZone(input.timezone)
        );
    }

    public static AllHours(): Array<SyncConfigOption> {
        return Range(0, 23)
            .map((x) => ({ id: x, description: SynchronizationConfiguration.FormatHour(x) }));
    }

    public static AllDays(): Array<SyncConfigOption> {
        return Range(0, 6)
            .map((x) => ({ id: x, description: SynchronizationConfiguration.WeekDayName(x) }));
    }

    public ToOutput(): SynchronizationConfigurationOutputDto {
        return {
            days: this.days,
            enabled: this.enabled,
            hours: this.hours,
            timezone: this.timezone.name
        };
    }

    private static FormatHour(hour: number): string {
        return `${hour >= 10 ? '' : '0'}${hour.toString(10)}:00`;
    }

    private static WeekDayName(day: number): string {
        return DateTime.utc(2024, 1, 1).plus({ days: day })
          .setLocale(GetLocale())
          .toFormat('cccc');
    }
}
