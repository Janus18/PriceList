export type ProductPriceInputDto = {
    reference: string;
    price: string;
};
