import { CategoryInputDto } from "./categoryInputDto";
import { MoneyInputDto } from "./moneyInputDto";
import { ProviderInputDto } from "./providerInputDto";

export type ProductInputDto = {
    id: number;
    code: string;
    description: string;
    frequent: boolean;
    basePrice?: MoneyInputDto;
    sellingPrice: MoneyInputDto;
    taxedPrice?: MoneyInputDto;
    recommendedSellingPrice?: MoneyInputDto;
    category?: CategoryInputDto;
    provider?: ProviderInputDto;
    createdAt: string;
    updatedAt: string;
    providerReference?: string;
    units?: number;
}
