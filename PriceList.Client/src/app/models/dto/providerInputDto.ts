export type ProviderInputDto = {
    createdAt: string,
    id: number,
    name: string,
    pricesHost: string,
    profitMargin: string,
    taxes: string,
    updatedAt: string
};
