export type MoneyInputDto = {
    currency: string;
    amount: string;
    unitAmount: string;
}
