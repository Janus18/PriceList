export type SynchronizationConfigurationOutputDto = {
    days: Array<number>;
    enabled: boolean;
    hours: Array<number>;
    timezone: string;
};
