export type CategoryOutputDto = {
    id?: number;
    description: string;
};
