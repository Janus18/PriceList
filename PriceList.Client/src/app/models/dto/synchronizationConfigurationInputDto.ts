export type SynchronizationConfigurationInputDto = {
    days: Array<number>;
    enabled: boolean;
    hours: Array<number>;
    timezone: string;
};
