import * as currency from "currency.js";
import { Money } from "../money";

export type ProductOutputDto = {
    id?: number;
    code: string;
    description: string;
    basePrice?: ProductOutputMoneyDto;
    sellingPrice?: ProductOutputMoneyDto;
    taxedPrice?: ProductOutputMoneyDto;
    frequent: boolean;
    categoryId?: number;
    providerId?: number;
    providerReference?: string;
    units?: number
};

export class ProductOutputMoneyDto {
    constructor(
        public currency: string,
        public amount: string | currency,
    ) { }

    static FromMoney(m: Money): ProductOutputMoneyDto {
        return new ProductOutputMoneyDto(m.currency, m.cable);
    }

    static From(currency: string, amount: string): ProductOutputMoneyDto {
        return ProductOutputMoneyDto.FromMoney(Money.Parse(currency, amount));
    }
}
