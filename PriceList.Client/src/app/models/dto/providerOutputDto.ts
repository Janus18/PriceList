import { FromSpanishNumber } from "src/app/shared/functions/spanish-numbers";

export class ProviderOutputDto {
    public id: number | undefined;
    public name: string;
    public pricesHost: string | undefined;
    public profitMargin: string | undefined;
    public taxes: string | undefined;

    constructor(
        id: number | undefined,
        name: string,
        pricesHost: string | undefined,
        profitMargin: string | undefined,
        taxes: string | undefined
    ) {
        this.id = id;
        this.name = name;
        this.pricesHost = pricesHost;
        this.profitMargin = profitMargin ? FromSpanishNumber(profitMargin) : undefined;
        this.taxes = taxes ? FromSpanishNumber(taxes) : undefined;
    }
};
