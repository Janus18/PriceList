export type CategoryInputDto = {
    id: number;
    description: string;
    createdAt: string;
    updatedAt: string;
}
