export type SynchronizationInputDto = {
    id: number;
    createdAt: string;
    finishedAt?: string;
    total: number;
    updated: number;
    failed: number;
    errors: { productId: number; message: string; }[];
};
