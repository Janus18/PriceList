import { AfterViewInit, Component, Inject, OnDestroy, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSelectChange } from '@angular/material/select';
import { NgxScannerQrcodeComponent, ScannerQRCodeDevice } from 'ngx-scanner-qrcode';

export type ScannerData = {
  hasInput: boolean
};

@Component({
  selector: 'app-scanner',
  templateUrl: './scanner.component.html',
  styleUrls: ['./scanner.component.scss']
})
export class ScannerComponent implements AfterViewInit, OnDestroy {
  static deviceIdKey = "DeviceID";

  @ViewChild('action') scanner?: NgxScannerQrcodeComponent;
  public message: { value: string, type: 'normal' | 'error' } | undefined = undefined;
  public displayScanner: boolean = true;
  public hasInput: boolean;
  public inputControl: FormControl = new FormControl('');
  private deviceId: string | null = null;

  constructor(
    private dialogRef: MatDialogRef<ScannerComponent>,
    @Inject(MAT_DIALOG_DATA) private data: ScannerData,
  ) {
    this.hasInput = data?.hasInput === true;
    this.deviceId = localStorage.getItem(ScannerComponent.deviceIdKey);
  }

  public get devices(): Array<ScannerQRCodeDevice> {
    return this.scanner?.isStart ? (this.scanner.devices?.value ?? []) : [];
  }

  public get currentDevice(): ScannerQRCodeDevice | null {
    return this.scanner?.isStart ? this.devices[this.scanner.deviceIndexActive] : null;
  }

  ngOnDestroy(): void {
    this.stop();
  }

  ngAfterViewInit(): void {
    try {
      this.scanner?.start((devices: Array<ScannerQRCodeDevice>) =>
        this.playDevice(this.findPreferredDevice(devices))
      ).subscribe(
        () => this.readOne(),
        err => this.onDeviceError()
      );
    } catch (err) {
      this.onDeviceError();
    }
  }

  /** Closes the dialog with the input value. Should only be used when the input is present. */
  public close(): void {
    if (!this.hasInput)
      return;
    this.dialogRef.close(this.inputControl.value);
  }

  private readOne(): void {
    this.scanner?.data.subscribe((res) => {
      if (!res || !res[0]?.value)
        return;
      if (this.hasInput)
        this.inputControl.setValue(res[0].value);
      else
        this.dialogRef.close(res[0].value);
    });
  }

  public deviceChanged(event: MatSelectChange): void {
    if (!this.scanner?.isStart)
      return;
    const device = event.value as ScannerQRCodeDevice;
    this.playDevice(device);
  }

  private findPreferredDevice(allDevices: Array<ScannerQRCodeDevice>): ScannerQRCodeDevice | undefined {
    if (this.deviceId && allDevices.some(d => d.deviceId === this.deviceId)) {
      return allDevices.find(d => d.deviceId === this.deviceId);
    }
    return allDevices.length > 0 ? allDevices[0] : undefined;
  }

  private playDevice(device: ScannerQRCodeDevice | undefined): void {
    if (!device)
      return;
    this.scanner?.playDevice(device.deviceId).subscribe(
      () => localStorage.setItem(ScannerComponent.deviceIdKey, device.deviceId),
      err => this.onDeviceError());
  }

  private onDeviceError(): void {
    this.displayError("No se pudo iniciar la cámara");
    this.stop();
    this.displayScanner = false;
  }

  private stop(): void {
    if (this.scanner?.isStart) {
      this.scanner.stop().subscribe();
    }
  }

  private displayError(err: string): void {
    this.message = { value: err, type: 'error' };
  }
}
